jun.login = new Ext.extend(Ext.Window, {
    width: 390,
    height: 200,
    layout: "form",
    modal: !0,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Login",
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                style: 'margin:5px',
                html: "Please, make sure that your computer's date and time are correct."
            },
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4; padding: 10px",
                id: "form-Login",
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                plain: !0,
                items: [
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date & Time',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y H:i:s',
                        readOnly: true,
                        anchor: "100%"
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Login",
                    hidden: !1,
                    ref: "../btnLogin"
                }
            ]
        };
        jun.login.superclass.initComponent.call(this);
        this.btnLogin.on("click", this.onbtnLoginClick, this);
        this.setDateTime();
    },
    setDateTime: function(){
        Ext.Ajax.request({
            url: 'GetDateTime',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnLoginClick: function () {
        var username = this.username.getValue();
        var password = this.password.getValue();
        if (username.trim() == "" || password.trim() == "") {
            Ext.Msg.alert("Warning!", "Login Failed");
            return;
        }
        var a = Ext.getCmp("passwordid").getValue();
        a = jun.EncryptPass(a);
        Ext.getCmp("passwordid").setValue(a);
        Ext.getCmp("form-Login").getForm().submit({
            scope: this,
            url: "login",
            waitTitle: "Connecting",
            waitMsg: "Sending data...",
            success: function () {
                var a = BASE_URL;
                window.location = a;
            },
            failure: function (f, a) {
                Ext.getCmp("form-Login").getForm().reset();
                this.setDateTime();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.errors.reason);
                }
            }
        });
    }
}), jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                region: "north",
                applyTo: "header",
                height: 30
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "west",
                height: 20
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "east",
                height: 20
            },
            {
                xtype: "container",
                autoEl: "div",
                region: "south",
                height: 20
            }
        ], jun.ViewportUi.superclass.initComponent.call(this);
    }
}), jun.win = new Ext.extend(Ext.Window, {
    layout: "fit",
    width: 300,
    height: 150,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    initComponent: function () {
        this.items = [ jun.login ], jun.win.superclass.initComponent.call(this);
    }
}), Ext.onReady(function () {
    var a = function () {
        Ext.get("loading").remove(), Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.QuickTips.init();
    var b = new jun.login({});
    b.show();
});