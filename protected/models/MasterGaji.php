<?php
Yii::import('application.models._base.BaseMasterGaji');
class MasterGaji extends BaseMasterGaji
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->master_gaji_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->master_gaji_id = $uuid;
        }
        return parent::beforeValidate();
    }
}