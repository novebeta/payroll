<?php
Yii::import('application.models._base.BaseSchemaGajiView');
class SchemaGajiView extends BaseSchemaGajiView
{

    public function primaryKey()
    {
        return 'schema_gaji_id';
    }
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}