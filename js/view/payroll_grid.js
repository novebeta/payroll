jun.PayrollGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Payroll",
    id: 'docs-jun.PayrollGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Periode',
            sortable: true,
            resizable: true,
            dataIndex: 'periode_id',
            width: 100,
            renderer: jun.renderPeriode,
            filter: {xtype: "textfield", filterName: "periode"}
        },
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Level',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_level',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Golongan',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_gol',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Jabatan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_jabatan',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        // if (jun.rztSchemaCmp.getTotalCount() === 0)
        jun.rztSchemaCmp.load();
        // if (jun.rztPeriodeCmp.getTotalCount() === 0)
        jun.rztPeriodeCmp.load();
        jun.rztPeriodeLib.load();
        // if (jun.rztLevelingCmp.getTotalCount() === 0)
        jun.rztLevelingCmp.load();
        // if (jun.rztGolonganCmp.getTotalCount() === 0)
        jun.rztGolonganCmp.load();
        // if (jun.rztAreaCmp.getTotalCount() === 0)
        jun.rztAreaCmp.load();
        this.store = jun.rztPayroll;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Generate All per Periode',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                // {
                //     xtype: 'button',
                //     text: 'Edit Payroll per Pegawai',
                //     ref: '../btnEdit'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'button',
                    text: 'Recalculated per Pegawai',
                    ref: '../btnRecalculated'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Buat Slip Gaji',
                    ref: '../btnCreateSlipGaji'
                },
                {
                    xtype: "form",
                    frame: !1,
                    id: "form-ReportPayroll",
                    border: !1,
                    items: [
                        {
                            xtype: "hidden",
                            name: "periode_id",
                            ref: "../../periode_id"
                        },
                        {
                            xtype: "hidden",
                            name: "payroll_id",
                            ref: "../../payroll_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }
                //,
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PayrollGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnRecalculated.on('Click', this.recalculatedRec, this);
        this.btnCreateSlipGaji.on('Click', this.createSlipGaji, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PayrollGenerateWin({modez: 0});
        form.show();
    },
    createSlipGaji: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih payroll");
            return;
        }
        // if (jun.rztLock.findExact('periode_id', selectedz.data.periode_id) == -1) {
        //     Ext.MessageBox.alert("Warning", "Data Payroll belum di lock");
        //     return;
        // }
        // if (selectedz.data.lock == 0) {
        //
        // }
        Ext.getCmp("form-ReportPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPayroll").getForm().url = "Report/CetakSlip";
        this.payroll_id.setValue(selectedz.json.payroll_id);
        var form = Ext.getCmp('form-ReportPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.payroll_id;
        var form = new jun.PayrollWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPayrollDetails.load({
            params: {
                payroll_id: idz
            }
        });
    },
    recalculatedRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin kalkulasi data ini?', this.recalculatedRecRecYes, this);
    },
    recalculatedRecRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Payroll/PayrollRecalculated',
            method: 'POST',
            params: {
                periode_id: record.data.periode_id,
                pegawai_id: record.data.pegawai_id
            },
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Payroll/delete/id/' + record.json.payroll_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
