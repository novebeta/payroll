jun.PegawaiBankstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PegawaiBankstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PegawaiBankStoreId',
            url: 'PegawaiBank',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pegawai_bank_id'},
                {name: 'bank_id'},
                {name: 'tipe_bank'},
                {name: 'no_rekening'},
                {name: 'fotocopy'},
                {name: 'visible'},
                {name: 'pegawai_id'},
            ]
        }, cfg));
    }
});
jun.rztPegawaiBank = new jun.PegawaiBankstore();
//jun.rztPegawaiBank.load();
