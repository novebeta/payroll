jun.EmailTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.EmailTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmailTransStoreId',
            url: 'EmailTrans',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'email_trans_id'},
                {name: 'pegawai_id'},
                {name: 'email'},
                {name: 'nama_'},
                {name: 'grup_'}
            ]
        }, cfg));
    }
});
jun.rztEmailTrans = new jun.EmailTransstore();
//jun.rztEmailTrans.load();
jun.EmailGroupingstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.EmailGroupingstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmailGroupingstoreId',
            url: 'EmailTrans/preview',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'email_trans_id'},
                {name: 'email'},
                {name: 'nama_'},
            ]
        }, cfg));
    }
});
jun.rztEmailGroupPreview = new jun.EmailGroupingstore();