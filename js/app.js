jun.ajaxCounter = 0;
jun.bu_id = '';
jun.cabang_id = '';
/*
    Menu Tree
 */
jun.TreeUi = Ext.extend(Ext.tree.TreePanel, {
    title: "Menu",
    useArrows: !0,
    autoScroll: !0,
    rootVisible: false,
    floatable: !1,
    animate: true,
    // border: false,
    autoLoad: false,
    containerScroll: false,
    dataUrl: "site/tree",
    initComponent: function () {
        this.root = {
            text: "Menu"
        };
        this.tbar = {
            xtype: 'toolbar',
            id: "treepanel-toolbar-menu",
            ref: 'treeToolbar',
            enableOverflow: true,
            items: [
                {
                    xtype: 'splitbutton',
                    text: 'Bisnis Unit',
                    class: '',
                    ref: '../btnbu',
                    menu: new Ext.menu.Menu()
                },
                // new Ext.CycleButton({
                //     showText: true,
                //     class: '',
                //     ref: '../btnbu',
                //     // items:[]
                //     prependText: 'View as ',
                //     iconCls: 'x-toolbar-more-icon',
                //     items: [
                //         {
                //             text: 'text only',
                //             iconCls: 'view-text',
                //             checked: true
                //         },
                //         {
                //             text: 'HTML',
                //             iconCls: 'view-html'
                //         }
                //     ],
                //     changeHandler: function (btn, item) {
                //         Ext.Msg.alert('Change View', item.text);
                //     }
                // }),
                {
                    text: '>>',
                    ref: '../separator',
                    hidden: true,
                    disabled: true
                },
                {
                    xtype: 'splitbutton',
                    text: 'Cabang',
                    ref: '../btnCabang',
                    class: '',
                    hidden: true,
                    menu: new Ext.menu.Menu()
                }
            ]
        };
        jun.TreeUi.superclass.initComponent.call(this);
        this.on('beforeload', function () {
            if (jun.bu_id == '') {
                return false;
            }
        }, this);
        this.on("click", function (a, b) {
            a.isLeaf() && (b.stopEvent(), a.id == "logout" ? Ext.MessageBox.confirm("Pertanyaan", "Apakah anda yakin ingin logout?", function (a) {
                if (a === "no") return;
                window.location.href = "site/logout";
            }, this) : jun.mainPanel.loadClass(a.id));
        });
        this.btnbu.on('click', this.btnbuOnclick, this);
        this.btnCabang.on('click', this.btnCabangOnclick, this);
        Ext.Ajax.request({
            url: 'Bu/IndexSrBu',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                // var menu = new Ext.menu.Menu();
                this.btnbu.menu.removeAll();
                Ext.each(response.results, function (it, id, all) {
                    var item = new Ext.menu.Item({
                        text: it.bu_name,
                        id: it.bu_id,
                        kode: it.bu_nama_alias
                    });
                    item.on('click', this.btnbuItemOnclick, this);
                    this.btnbu.menu.addItem(item);
                }, this);
                // this.btnbu.menu = menu;
                this.btnbu.menu.doLayout();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnbuOnclick: function (t, e) {
        if (jun.bu_id !== '') {
            jun.mainTreeUi.getRootNode().reload();
            jun.mainPanel.removeAll(true);
            this.resetBtnCabang();
            Ext.Ajax.request({
                url: 'Cabang/IndexSrCabangCmp',
                method: 'POST',
                scope: this,
                params: {
                    bu_id: jun.bu_id
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    // var menu = new Ext.menu.Menu();
                    this.btnCabang.menu.removeAll();
                    Ext.each(response.results, function (it, id, all) {
                        var item = new Ext.menu.Item({
                            text: it.nama_cabang,
                            id: it.cabang_id,
                            kode: it.kode_cabang
                        });
                        item.on('click', this.btnCabangItemOnclick, this);
                        this.btnCabang.menu.addItem(item);
                    }, this);
                    // this.btnCabang.menu = menu;
                    this.btnCabang.menu.doLayout();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    resetBtnCabang: function () {
        jun.cabang_id = '';
        this.btnCabang.setText('Cabang');
    },
    btnbuItemOnclick: function (t, e) {
        // console.log(t.id + ' ' + t.text);
        this.btnbu.setText(t.kode);
        this.btnbu.class = t.id;
        jun.bu_id = t.id;
        this.resetBtnCabang();
        this.separator.setVisible(true);
        this.btnCabang.setVisible(true);
        this.btnbuOnclick();
    },
    btnCabangItemOnclick: function (t, e) {
        // console.log(t.id + ' ' + t.text);
        this.btnCabang.setText(t.kode);
        this.btnCabang.class = t.id;
        jun.cabang_id = t.id;
    },
    btnCabangOnclick: function (t, e) {
        // console.log(t.id + ' ' + t.text);
        // this.btnCabang.setText(t.kode);
        // this.btnCabang.class = t.id;
        jun.cabang_id = this.btnCabang.class;
        jun.mainTreeUi.getRootNode().reload();
        jun.mainPanel.removeAll(true);
    }
});
/*
    Menu BU
 */
jun.BuDataView = Ext.extend(Ext.DataView, {
    id: 'docs-jun.BuDataView',
    tpl: new Ext.XTemplate(
        '<ul>',
        '<tpl for=".">',
        '<tpl if="item_type == 1">',
        '<li class="BuDataViewItem">',
        '<div class="product-img">',
        '<img src="{[this.getImageSrc(values)]}"  alt="{bu_name}">',
        '</div>',
        '</li>',
        '</tpl>',
        '<tpl if="item_type == 2">',
        '<li class="BuDataViewItem category">',
        '<div class="product-img">',
        '<img src="{[this.getImageSrc(values)]}"  alt="{bu_name}">',
        '</div>',
        '</li>',
        '</tpl>',
        '</tpl>',
        '</ul>',
        {
            getImageSrc: function (p) {
                return 'images/' + p.bu_id + '.png';
            }
        }
    ),
    cls: 'BuDataView',
    itemSelector: 'li.BuDataViewItem',
    overClass: 'BuDataViewItem-hover',
    singleSelect: true,
    autoScroll: true,
    initComponent: function () {
        this.store = new Ext.data.JsonStore({
            url: 'bu/pick',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'node_id'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'item_type'}
            ]
        });
        jun.BuDataView.superclass.initComponent.call(this);
        this.on('selectionchange', this.onSelectionchange, this);
        this.store.load({
            params: {'view': 2}
        });
        this.store.baseParams = {};
    },
    onSelectionchange: function (c, selections) {
        var r = c.getSelectedRecords();
        if (!r.length) return;
        // this.record = r[0];
        var rec = r[0];
        switch (rec.get('item_type')) {
            case '1' :
                //if(rec.get('max_qty')==0) break;
                // this.addProduct(rec);
                Ext.MessageBox.show({
                    title: 'Business Unit',
                    msg: rec.get('kode') + rec.get('nama'),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                break;
            case '2' :
                // this.loadProductByCategory(rec.get('product_category_id'), rec.get('product_category_name'));
                // this.store.setBaseParam('view', 2);
                this.store.setBaseParam('node_id', rec.get('node_id'));
                this.store.load();
                break;
        }
        this.clearSelections();
    }
});
/*
    Main panel
 */
jun.mainPanel = new jun.TabsUi();
jun.mainTreeUi = new jun.TreeUi({
    region: "west",
    split: !0,
    width: 240
});
jun.ViewportUi = Ext.extend(Ext.Viewport, {
    layout: "border",
    initComponent: function () {
        this.items = [
            {
                xtype: "box",
                region: "north",
                id: "app-header",
                html: SYSTEM_TITLE + "<br />" + '<span class="subtitle">' + SYSTEM_SUBTITLE + "</span>",
                height: 60
            },
            jun.mainTreeUi,
            // {
            //     xtype: 'panel',
            //     // layout: 'accordion',
            //     region: "west",
            //     split: !0,
            //     width: 240,
            //     items: [
            //         // {
            //         //     title: "Business units",
            //         //     layout: 'fit',
            //         //     border: false,
            //         //     items: new jun.BuDataView()
            //         // },
            //         new jun.TreeUi()
            //     ]
            // },
            jun.mainPanel
        ];
        jun.ViewportUi.superclass.initComponent.call(this);
    }
});
jun.myMask = new Ext.LoadMask(Ext.getBody(), {msg: "Processing... please wait"});
Ext.onReady(function () {
    var a = function () {
        Ext.get("loading").remove(), Ext.fly("loading-mask").fadeOut({
            remove: !0
        });
    };
    Ext.Ajax.timeout = 1800000;
    Ext.QuickTips.init();
    //    loadText = "Sedang proses... silahkan tunggu";
    Ext.Ajax.on("beforerequest", function (conn, opts) {
        jun.myMask.show();
        jun.ajaxCounter++;
    });
    Ext.Ajax.on("requestcomplete", function (conn, response, opts) {
        if (jun.ajaxCounter > 1) {
            jun.ajaxCounter--;
        } else {
            jun.ajaxCounter = 0;
            jun.myMask.hide();
        }
    });
    Ext.Ajax.on("requestexception", function (conn, response, opts) {
        jun.ajaxCounter = 0;
        jun.myMask.hide();
        switch (response.status) {
            case 403:
                window.location.href = 'site/logout';
                break;
            case 500:
                Ext.Msg.alert('Internal Server Error', response.responseText);
                break;
            default :
                Ext.Msg.alert(response.status + " " + response.statusText, response.responseText);
                break;
        }
    });
    var b = new jun.ViewportUi({});
});