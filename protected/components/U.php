<?php
class U {
	static function generate_uuid() {
		$command = Yii::app()->db->createCommand( "SELECT UUID();" );
		$uuid    = $command->queryScalar();
		return $uuid;
	}
	static function report_absen( $periode_id, $cabang_id ) {
		$periode = Periode::model()->findByPk( $periode_id );
		$pstart  = $periode->periode_start;
		$pend    = $periode->periode_end;
		$comm    = Yii::app()->db->createCommand( "
            SELECT 
		t2.*
		, (SUM(kode_ket = 0) + SUM(kode_ket = 3)) AS HK
		, SUM(kode_ket = 1) AS LK
		, SUM(kode_ket = 2) AS S
		, SUM(kode_ket = 4) AS OFF
		, SUM(kode_ket = 5) AS CT
		, SUM(kode_ket = 6) AS CM
		, SUM(kode_ket = 7) AS CB
		, SUM(kode_ket = 8) AS CI
		, SUM(kode_ket = 9) AS CNA
		FROM
		(
                    SELECT
                    p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode, j.kode
                    , ifnull(SUM(min_early_time), 0) AS real_lembur_awal
                    , ifnull(SUM(min_over_time_awal), 0) AS real_lembur_pertama
                    , ifnull(SUM(min_over_time), 0) AS real_lembur_akhir
                    FROM	
                    pbu_pegawai p
                    LEFT JOIN pbu_jabatan j ON j.jabatan_id = p.jabatan_id
                    LEFT JOIN pbu_cabang c ON c.cabang_id = p.cabang_id
                    LEFT JOIN pbu_bu b ON b.bu_id = c.bu_id
                    LEFT JOIN pbu_validasi_view v ON v.PIN = p.nik 
                        AND v.approval_lembur = 1 
                        AND v.status_int = 1 
                        AND v.in_time >= :pstart AND v.out_time <= :pend
                    GROUP BY p.nik
		) t2
		LEFT JOIN pbu_validasi_view v ON v.PIN = t2.nik
		LEFT JOIN pbu_pegawai p ON p.nik = v.PIN
		LEFT JOIN pbu_keterangan k ON k.keterangan_id = v.kode_ket
            WHERE 
            t2.cabang_id = :cabang_id
            AND v.in_time >= :pstart
            AND v.out_time <= :pend
            GROUP BY t2.pegawai_id                
            ORDER BY t2.nik
        " );
		$a       = $comm->queryAll( true, [
			':pstart'    => $pstart,
			':pend'      => $pend,
			':cabang_id' => $cabang_id
		] );
		return $a;
	}
	static function report_fp( $periode_id, $pegawai, $cabang_id ) {
		if ( $pegawai ) {
			$where = "t2.pegawai_id = '$pegawai' AND";
		}
//        $comm = Yii::app()->db->createCommand("
//        select 
//        p.nama_lengkap, b.bu_name, b.bu_kode, p.nik, DATE_FORMAT(v.in_time,'%Y-%m-%d') tgl,
//        DATE_FORMAT(v.in_time,'%H:%i:%s') masuk, DATE_FORMAT(v.out_time,'%H:%i:%s') keluar, nama_ket as keterangan, '-' alasan
//        from pbu_validasi_view v
//        LEFT JOIN pbu_pegawai p on p.nik = v.PIN
//        LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
//        LEFT JOIN pbu_bu b on b.bu_id = c.bu_id 
//        LEFT JOIN pbu_result r on r.result_id = v.result_id
//        LEFT JOIN pbu_periode pr on pr.periode_id = r.periode_id 
//        LEFT JOIN pbu_keterangan k on k.keterangan_id = v.kode_ket
//        WHERE pr.periode_id = :periode_id $where AND c.cabang_id = '$cabang'
//        ORDER BY p.nik, tgl");
		$periode = Periode::model()->findByPk( $periode_id );
		$pstart  = $periode->periode_start;
		$pend    = $periode->periode_end;
		$comm    = Yii::app()->db->createCommand(
			"select 
                        t2.*
                        , IF(kode_ket = 3 OR kode_ket = 0, DATE_FORMAT(v.in_time,'%H:%i:%s'), '-') masuk
                        , IF(kode_ket = 3 OR kode_ket = 0, DATE_FORMAT(v.out_time,'%H:%i:%s'), '-') keluar
                        , nama_ket as keterangan, '-' alasan, v.approval_lembur
                from
                (
                    SELECT
                    t1.*, p.pegawai_id, p.nama_lengkap, p.nik, p.cabang_id, b.bu_name, b.bu_kode
                    FROM
                    (
                        select * from 
                                         (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
                                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) s
                        where selected_date >= DATE_FORMAT(:pstart, '%Y-%m-%d') and selected_date <= DATE_FORMAT(:pend, '%Y-%m-%d')
                    ) t1,
                    pbu_pegawai p
                    LEFT JOIN pbu_cabang c on c.cabang_id = p.cabang_id
                    LEFT JOIN pbu_bu b on b.bu_id = c.bu_id
                ) t2
                LEFT JOIN pbu_validasi_view v on v.PIN = t2.nik and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date
                LEFT JOIN pbu_pegawai p on p.nik = v.PIN
                LEFT JOIN pbu_result r on r.result_id = v.result_id
                LEFT JOIN pbu_periode pr on pr.periode_id = r.periode_id 
                LEFT JOIN pbu_keterangan k on k.keterangan_id = v.kode_ket
                where $where t2.cabang_id = :cabang_id
                order by t2.nik, t2.selected_date"
		);
		$a       = $comm->queryAll( true, [
			':pstart'    => $pstart,
			':pend'      => $pend,
			':cabang_id' => $cabang_id
		] );
		return $a;
	}
	static function report_rekap_pegawai() {
		$comm = Yii::app()->db->createCommand( "SELECT pp.pegawai_id,pp.nik,pl.kode AS kode_leveling,pg.kode AS kode_golongan,pp.nama_lengkap,pp.email,
        to_char(pp.tgl_masuk,'DD Mon YYYY') AS tgl_masuk,pp.npwp,pp.bank_nama,pp.bank_kota,pp.rekening,
        pj.nama_jabatan,psp.kode AS kode_status,
        MAX(CASE WHEN pm.kode = 'GP' AND pmg.leveling_id = pp.leveling_id THEN pmg.amount ELSE 0 END) AS gp,
        MAX(CASE WHEN pm.kode = 'TJ' AND pmg.leveling_id = pp.leveling_id THEN pmg.amount ELSE 0 END) AS tj,
        MAX(CASE WHEN pm.kode = 'UM' AND pmg.leveling_id = pp.leveling_id THEN pmg.amount ELSE 0 END) AS um,
        MAX(CASE WHEN pm.kode = 'UT' AND pmg.leveling_id = pp.leveling_id THEN pmg.amount ELSE 0 END) AS ut
        FROM pbu_pegawai AS pp
        LEFT JOIN pbu_jabatan AS pj ON pp.jabatan_id = pj.jabatan_id
        LEFT JOIN pbu_status_pegawai AS psp ON pp.status_pegawai_id = psp.status_pegawai_id
        LEFT JOIN pbu_golongan AS pg ON pp.golongan_id = pg.golongan_id
        LEFT JOIN pbu_leveling AS pl ON pp.leveling_id = pl.leveling_id
        LEFT JOIN pbu_master_gaji AS pmg ON pmg.golongan_id = pg.golongan_id
        LEFT JOIN pbu_master AS pm ON pmg.master_id = pm.master_id
        GROUP BY pp.pegawai_id,pp.nik,pl.kode,pg.kode,pp.nama_lengkap,pp.email,pp.tgl_masuk,
				pp.bank_nama,pp.bank_kota,pp.rekening,pj.nama_jabatan,psp.kode
        ORDER BY pg.kode DESC " );
		return $comm->queryAll( true );
	}
	static function report_master_gaji() {
		/** @var CDbCommand $comm */
		$sql1 = Yii::app()->db->createCommand( 'SELECT GROUP_CONCAT(CONCAT(\'MAX(IF(pm.kode = "\',pm.kode,\'", pmg.amount, NULL)) AS \',pm.kode))
          FROM pbu_master AS pm;' )->queryScalar();
		$sql2 = 'SELECT pl.nama AS lnama,pg.nama AS gnama,pa.nama AS anama, ' . $sql1 . ',
        SUM(CASE pm.periode_ WHEN "daily" THEN pmg.amount*20 WHEN "weekly" THEN pmg.amount*4 ELSE pmg.amount END) AS asumsi
        FROM pbu_master_gaji AS pmg
        INNER JOIN pbu_master AS pm ON pmg.master_id = pm.master_id
        INNER JOIN pbu_leveling AS pl ON pmg.leveling_id = pl.leveling_id
        INNER JOIN pbu_golongan AS pg ON pmg.golongan_id = pg.golongan_id
        INNER JOIN pbu_area AS pa ON pmg.area_id = pa.area_id
        GROUP BY pl.leveling_id,pg.golongan_id,pa.area_id
        ORDER BY pl.kode DESC, pg.kode DESC';
		return Yii::app()->db->createCommand( $sql2 )->queryAll( true );
	}
	static function report_rekap_payroll_header( $periode_id ) {
		$comm = Yii::app()->db->createCommand( '
        SELECT p1.nama_skema FROM pbu_payroll_details p1 
        INNER JOIN pbu_payroll p2 ON p1.payroll_id=p2.payroll_id 
        WHERE p2.periode_id = :periode_id GROUP BY p1.nama_skema,p1.seq ORDER BY p1.seq' );
		return $comm->queryAll( true, [ ':periode_id' => $periode_id ] );
	}
	static function report_rekap_payroll( $periode_id, $hide_hk_null = false ) {
		$where = '';
		if ( $hide_hk_null ) {
			$where = "AND pp.total_hari_kerja > 0";
		}
//		Yii::app()->db->createCommand( 'SET SESSION group_concat_max_len = 1000000;' )->execute();
		$sql1 = Yii::app()->db->createCommand( "SELECT
	string_agg (
	'SUM(CASE WHEN ppd.nama_skema = '''||ppd.nama_skema||''' THEN ppd.amount ELSE 0 END) AS '||	'\"'||ppd.nama_skema|| '\"' ,',') 
FROM
	( SELECT p1.nama_skema FROM pbu_payroll_details p1 INNER JOIN pbu_payroll p2 ON p1.payroll_id = p2.payroll_id 
	WHERE p2.periode_id = :periode_id
	GROUP BY p1.nama_skema,p1.seq ORDER BY p1.seq ) ppd;" )
		                      ->queryScalar( [ ':periode_id' => $periode_id ] );
		if ( $sql1 != null ) {
			$sql1 = ',' . $sql1;
		}
		$comm = Yii::app()->db->createCommand( "SELECT pp.periode_id,pp.kode_cab,pp.total_hari_kerja,pp.total_lk,
            pp.total_sakit,pp.total_cuti_tahunan,pp.total_off,pp.jatah_off,(pp.jatah_off - pp.total_off) as ganti_off,pp.pegawai_id,
            pp.nik,pp.nama_lengkap,pp.nama_jabatan,to_char(pp.tgl_masuk,'DD-MM-YYYY') AS tgl_masuk,pp.email,pp.total_income,
            pp.total_deduction,pp.take_home_pay,pp.transfer,pp.kode_level||' '||pp.kode_gol AS kode_level,
            pp.golongan_id,pp.leveling_id,pp.area_id,pp.nama_area,pp.total_cuti_bersalin " . $sql1 . "
            FROM pbu_payroll AS pp
            INNER JOIN pbu_payroll_details AS ppd ON ppd.payroll_id = pp.payroll_id
            WHERE pp.periode_id = :periode_id " . $where . "
        GROUP BY pp.pegawai_id,pp.kode_cab,pp.nama_area,pp.periode_id,pp.total_hari_kerja,pp.total_lk,
            pp.total_sakit,pp.total_cuti_tahunan,pp.total_off,pp.jatah_off,ganti_off,pp.pegawai_id,
            pp.nik,pp.nama_lengkap,pp.nama_jabatan,pp.tgl_masuk,pp.email,pp.total_income,
            pp.total_deduction,pp.take_home_pay,pp.transfer,pp.kode_level,pp.kode_gol,pp.total_cuti_bersalin,
            pp.golongan_id,pp.leveling_id,pp.area_id
        ORDER BY pp.kode_level DESC, pp.kode_gol DESC" );
		return $comm->queryAll( true, [ ':periode_id' => $periode_id ] );
	}
	static function report_laporan_bank_payroll( $periode_id, $bank_id, $tipe_bank, $h ) {
//		Yii::app()->db->createCommand( 'SET SESSION group_concat_max_len = 1000000;' )->execute();
		$comm = Yii::app()->db->createCommand( "SELECT pp.periode_id,pp.kode_cab,pp.total_hari_kerja,pp.total_lk,
            pp.total_sakit,pp.total_cuti_tahunan,pp.total_off,pp.jatah_off,(pp.jatah_off - pp.total_off) as ganti_off,pp.pegawai_id,
            pp.nik,pp.nama_lengkap,pp.nama_jabatan,to_char(pp.tgl_masuk,'DD-MM-YYYY') AS tgl_masuk,pp.email,pp.total_income,
            pp.total_deduction,pp.take_home_pay,pp.transfer,pp.kode_level||' '||pp.kode_gol AS kode_level,
            pp.golongan_id,pp.leveling_id,pp.area_id,pp.nama_area,pp.total_lembur_1,pp.total_lembur_next,
			''''||ppb.no_rekening AS no_rekening " . $h . "
            FROM pbu_payroll AS pp
            INNER JOIN pbu_payroll_details AS ppd ON ppd.payroll_id = pp.payroll_id            
			INNER JOIN pbu_pegawai_bank AS ppb ON pp.pegawai_id = ppb.pegawai_id
            WHERE pp.periode_id = :periode_id AND ppb.bank_id = :bank_id AND ppb.tipe_bank = :tipe_bank
        GROUP BY pp.pegawai_id,pp.kode_cab,pp.nama_area,pp.periode_id,pp.total_hari_kerja,pp.total_lk,
            pp.total_sakit,pp.total_cuti_tahunan,pp.total_off,pp.jatah_off,ganti_off,pp.pegawai_id,
            pp.nik,pp.nama_lengkap,pp.nama_jabatan,pp.tgl_masuk,pp.email,pp.total_income,
            pp.total_deduction,pp.take_home_pay,pp.transfer,pp.kode_level,pp.kode_gol,
            pp.golongan_id,pp.leveling_id,pp.area_id,pp.total_lembur_1,pp.total_lembur_next,
			ppb.no_rekening 
        ORDER BY pp.kode_level DESC, pp.kode_gol DESC" );
		return $comm->queryAll( true, [
			':periode_id' => $periode_id,
			':bank_id'    => $bank_id,
			':tipe_bank'  => $tipe_bank,
		] );
	}
	static function absensiPayrollIndex( $periode_id ) {
		/** @var Periode $periode */
		$periode = Periode::model()->findByPk( $periode_id );
		$id      = Yii::app()->user->getId();
		$sri     = Users::model()
		                ->findByAttributes( array( 'id' => $id ) )
			->security_roles_id;
//		$comm    = Yii::app()->db->createCommand( '\SET periode_id = :periode_id;' )
//		                         ->execute( [ ':periode_id' => $periode_id ] );
		$comm = Yii::app()->db->createCommand( "DROP TABLE IF EXISTS absensi;" )
		                      ->execute();
		$comm = Yii::app()->db->createCommand( '
CREATE TEMPORARY TABLE IF NOT EXISTS absensi AS (
        SELECT
            CAST(:periode_id AS TEXT) AS periode_id,
            ppa.payroll_absensi_id,
            COALESCE(ppa.total_hari_kerja,0) AS total_hari_kerja,
            COALESCE(ppa.locked,0) AS locked,
            COALESCE(ppa.total_lk,0) AS total_lk,
            COALESCE(ppa.total_sakit,0) AS total_sakit,
            COALESCE(ppa.total_cuti_tahunan,0) AS total_cuti_tahunan,
            COALESCE(ppa.total_off,0) AS total_off,            
            COALESCE(ppa.total_cuti_menikah,0) AS total_cuti_menikah,
            COALESCE(ppa.total_cuti_bersalin,0) AS total_cuti_bersalin,
            COALESCE(ppa.total_cuti_istimewa,0) AS total_cuti_istimewa,
            COALESCE(ppa.total_cuti_non_aktif,0) AS total_cuti_non_aktif,
            COALESCE(ppa.total_lembur_1 ,0) AS total_lembur_1,
            COALESCE(ppa.total_lembur_next,0) AS total_lembur_next,
            COALESCE(ppa.total_presentasi,0) AS total_presentasi,
            COALESCE(ppa.jatah_off,0) AS jatah_off,
            COALESCE(ppa.less_time,0) AS less_time,
            pg.nik,pg.pegawai_id,pg.nama_lengkap,pg.tgl_masuk
        FROM
            pbu_pegawai AS pg 
            INNER JOIN pbu_cabang AS pc ON pg.cabang_id = pc.cabang_id AND pc.bu_id = :bu_id
        LEFT JOIN pbu_payroll_absensi AS ppa ON ppa.pegawai_id = pg.pegawai_id AND ppa.periode_id = :periode_id
        INNER JOIN pbu_sr_cabang AS sr ON pg.cabang_id = sr.cabang_id AND sr.security_roles_id = :security_roles_id
        INNER JOIN pbu_sr_leveling as sl ON sl.leveling_id = pg.leveling_id AND sl.security_roles_id = :security_roles_id
        WHERE pg.jenis_periode_id = :jenis_periode_id
        );' );
		$comm->execute( [
			':bu_id'             => $periode->jenisPeriode->bu_id,
			':periode_id'        => $periode_id,
			':security_roles_id' => $sri,
			':jenis_periode_id'  => $periode->jenis_periode_id
		] );
		$comm = Yii::app()->db->createCommand( "
		SELECT a.* FROM absensi AS a 
				INNER JOIN pbu_periode AS prd ON a.periode_id = prd.periode_id 
 				LEFT JOIN pbu_resign AS r ON r.pegawai_id = a.pegawai_id 
				WHERE  (r.resign_id IS NULL OR (r.resign_id IS NOT NULL AND r.tgl > DATE(prd.periode_start))) 
				AND a.tgl_masuk < DATE(prd.periode_end) ;" );
		return $comm->queryAll( true );
	}
	static function previewEmail( $bu_id ) {
		$comm = Yii::app()->db->createCommand( '
		SELECT pet.email_trans_id,pet.email,COALESCE(petd.nama_,pet.nama_) AS nama_
		FROM "public".pbu_email_trans AS pet
		LEFT JOIN "public".pbu_email_trans_detail AS petd ON petd.email_trans_id = pet.email_trans_id
		WHERE pet.bu_id = :bu_id' );
		return $comm->queryAll( true, [ ':bu_id' => $bu_id ] );
	}
	static function testDelay( $second ) {
		sleep( $second );
	}
}
