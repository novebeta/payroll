<?php

class m140528_030646_auto_number extends CDbMigration
{
	public function safeUp()
    {
        $this->createTable('{{auto_number}}', [
            'template_group' => 'VARCHAR(64) NOT NULL',
            'template_num' => 'VARCHAR(64) NOT NULL',
            'auto_number' => 'INT NOT NULL',
            'update_time' => 'INT'
        ]);
        $this->addPrimaryKey('auto_number_pk', '{{auto_number}}', ['template_group','template_num']);
    }

    public function safeDown()
    {
        $this->dropTable('{{auto_number}}');
    }
}