<?php

Yii::import('application.models._base.BaseSrCbgAreaBu');

class SrCbgAreaBu extends BaseSrCbgAreaBu
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function primaryKey()
    {
        return 'sr_cabang';
    }
}