jun.PeriodeJoinstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PeriodeJoinstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PeriodeJoinStoreId',
            url: 'PeriodeJoin',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'periode_join_id'},
                {name: 'periode_id'},
                {name: 'periode_id_child'},
            ]
        }, cfg));
    }
});
jun.rztPeriodeJoin = new jun.PeriodeJoinstore();
//jun.rztPeriodeJoin.load();
