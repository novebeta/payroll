jun.PeriodeJoinGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Periode Join",
    id: 'docs-jun.PeriodeJoinGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Periode Item',
            sortable: true,
            resizable: true,
            dataIndex: 'periode_id_child',
            renderer: jun.renderPeriode,
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztPeriodeJoin;
        jun.rztPeriodeCmp.load();
        jun.rztPeriodeLib.load();
        jun.rztPeriodeJoin.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var periode = Ext.getCmp('periode_join_parent_periode_id');
                    b.params.periode_id = periode.getValue();
                    if (b.params.periode_id == '') {
                        return false;
                    }
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                'Periode Induk : ',
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeCmp,
                    hiddenName: 'periode_id',
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    id: 'periode_join_parent_periode_id',
                    ref: '../periode_parent'
                },
                {
                    xtype: 'tbseparator'
                },
                'Periode Item : ',
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeCmp,
                    hiddenName: 'periode_id',
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    id: 'periode_join_item_periode_id',
                    ref: '../periode_item'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add Periode Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Periode Item',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.PeriodeJoinGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.periode_parent.on('select', this.onPeriodeParentSelect, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onPeriodeParentSelect: function () {
        jun.rztPeriodeJoin.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var periode_id = this.periode_parent.getValue();
        var periode_id_child = this.periode_item.getValue();
        Ext.Ajax.request({
            url: 'PeriodeJoin/create',
            method: 'POST',
            scope: this,
            params: {
                periode_id: periode_id,
                periode_id_child: periode_id_child
            },
            success: function (f, a) {
                jun.rztPeriodeJoin.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.periode_join_id;
        var form = new jun.PeriodeJoinWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PeriodeJoin/delete/id/' + record.json.periode_join_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPeriodeJoin.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
