<?php

/**
 * This is the model class for table "{{daily_master_gaji}}".
 *
 * The followings are the available columns in table '{{daily_master_gaji}}':
 * @property string $pegawai_id
 * @property string $nama_lengkap
 * @property string $leveling_id
 * @property string $golongan_id
 * @property string $area_id
 * @property string $master_id
 * @property string $amount
 * @property string $lcode
 * @property string $lnama
 * @property string $gcode
 * @property string $gnama
 * @property string $akode
 * @property string $anama
 * @property string $mkode
 * @property string $mnama
 * @property string $bu_id
 */
class DailyMasterGaji extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{daily_master_gaji}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, leveling_id, golongan_id, area_id, master_id, bu_id', 'length', 'max'=>36),
			array('nama_lengkap, lnama, gnama, anama, mnama', 'length', 'max'=>100),
			array('lcode, gcode, akode, mkode', 'length', 'max'=>20),
			array('amount', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pegawai_id, nama_lengkap, leveling_id, golongan_id, area_id, master_id, amount, lcode, lnama, gcode, gnama, akode, anama, mkode, mnama, bu_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pegawai_id' => 'Pegawai',
			'nama_lengkap' => 'Nama Lengkap',
			'leveling_id' => 'Leveling',
			'golongan_id' => 'Golongan',
			'area_id' => 'Area',
			'master_id' => 'Master',
			'amount' => 'Amount',
			'lcode' => 'Lcode',
			'lnama' => 'Lnama',
			'gcode' => 'Gcode',
			'gnama' => 'Gnama',
			'akode' => 'Akode',
			'anama' => 'Anama',
			'mkode' => 'Mkode',
			'mnama' => 'Mnama',
			'bu_id' => 'Bu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawai_id',$this->pegawai_id,true);
		$criteria->compare('nama_lengkap',$this->nama_lengkap,true);
		$criteria->compare('leveling_id',$this->leveling_id,true);
		$criteria->compare('golongan_id',$this->golongan_id,true);
		$criteria->compare('area_id',$this->area_id,true);
		$criteria->compare('master_id',$this->master_id,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('lcode',$this->lcode,true);
		$criteria->compare('lnama',$this->lnama,true);
		$criteria->compare('gcode',$this->gcode,true);
		$criteria->compare('gnama',$this->gnama,true);
		$criteria->compare('akode',$this->akode,true);
		$criteria->compare('anama',$this->anama,true);
		$criteria->compare('mkode',$this->mkode,true);
		$criteria->compare('mnama',$this->mnama,true);
		$criteria->compare('bu_id',$this->bu_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DailyMasterGaji the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
