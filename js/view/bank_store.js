jun.Bankstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Bankstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BankStoreId',
            url: 'Bank',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bank_id'},
                {name: 'kode_bank'},
                {name: 'nama_bank'},
            ]
        }, cfg));
    }
});
jun.rztBank = new jun.Bankstore();
//jun.rztBank.load();
