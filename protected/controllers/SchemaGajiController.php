<?php
class SchemaGajiController extends GxController
{
    public function actionCreate()
    {
        $model = new SchemaGaji;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SchemaGaji'][$k] = $v;
                }
                $model->attributes = $_POST['SchemaGaji'];
                $msg = "Data gagal disimpan.";
//	            $model->bu_id = '0949c440-d499-48a7-af6e-c675c0ab5e27';
	            if ($model->save()) {
		            $status = true;
		            $msg = "Data berhasil di simpan dengan id " . $model->schema_gaji_id;
	            } else {
		            $msg .= " " . CHtml::errorSummary($model);
		            $status = false;
	            }
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            $status = true;
            try {
                /** @var SchemaGaji $model */
                $model = $this->loadModel($id, 'SchemaGaji');
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SchemaGaji'][$k] = $v;
                }
                $msg = "Data gagal disimpan";
                $model->attributes = $_POST['SchemaGaji'];
//	            $model->bu_id = '0949c440-d499-48a7-af6e-c675c0ab5e27';
	            if ($model->save()) {
		            $status = true;
		            $msg = "Data berhasil di simpan dengan id " . $model->schema_gaji_id;
	            } else {
		            $msg .= " " . CHtml::errorSummary($model);
		            $status = false;
	            }
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SchemaGaji')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->order = 'seq';
        $params = [];
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['bu_id']) && $_POST['bu_id'] != null) {
            $criteria->addCondition('bu_id = :bu_id');
            $params[':bu_id'] = $_POST['bu_id'];
        }
        $criteria->params = $params;
        $model = SchemaGajiView::model()->findAll($criteria);
        $total = SchemaGajiView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}