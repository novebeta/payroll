<?php
Yii::import( 'application.models._base.BaseEmail' );
class Email extends BaseEmail {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->email_id == null ) {
			$command        = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid           = $command->queryScalar();
			$this->email_id = $uuid;
		}
		return parent::beforeValidate();
	}
	protected function afterSave() {
		parent::afterSave();
		$user_id = Yii::app()->user->id;
		/** @var Users $user */
		$user           = Users::model()->findByPk( $user_id );
		$user->email_id = $this->email_id;
		$user->save();
	}
	public function beforeSave() {
		$this->username_ = base64_encode( Yii::app()->getSecurityManager()->encrypt( $this->username_ ) );
		$this->passw_    = base64_encode( Yii::app()->getSecurityManager()->encrypt( $this->passw_ ) );
		return parent::beforeSave();
	}
	protected function afterFind() {
		$this->username_ = Yii::app()->getSecurityManager()->decrypt( base64_decode( $this->username_ ) );
		$this->passw_    = Yii::app()->getSecurityManager()->decrypt( base64_decode( $this->passw_ ) );
		parent::afterFind();
	}
//	public function decrypt() {
//		$this->username_ = Yii::app()->getSecurityManager()->decrypt( base64_decode( $this->username_ ) );
//		$this->passw_    = Yii::app()->getSecurityManager()->decrypt( base64_decode( $this->passw_ ) );
//	}
}