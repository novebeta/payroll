<?php
Yii::import( 'application.models._base.BasePayrollAbsensi' );
class PayrollAbsensi extends BasePayrollAbsensi {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->payroll_absensi_id == null ) {
			$command                  = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                     = $command->queryScalar();
			$this->payroll_absensi_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		return parent::beforeValidate();
	}
	public static function tarikAbsen( $periode_id ) {
		/** @var Periode $periode */
		$periode = Periode::model()->findByPk( $periode_id );
		if ( $periode == null ) {
			throw new Exception( 'Periode tidak ditemukan.' );
		}
		$comm  = Yii::app()->db->createCommand( "
	    SELECT pv.pegawai_id, 
			SUM(CASE WHEN pv.kode_ket = 0 OR pv.kode_ket = 3 THEN 1 ELSE 0 END) AS total_hari_kerja,
			SUM(CASE WHEN pv.kode_ket = 1 THEN 1 ELSE 0 END) AS total_lk,
			SUM(CASE WHEN pv.kode_ket = 2 THEN 1 ELSE 0 END) AS total_sakit,
			SUM(CASE WHEN pv.kode_ket = 4 THEN 1 ELSE 0 END) AS total_off,
			SUM(CASE WHEN pv.kode_ket = 5 THEN 1 ELSE 0 END) AS total_cuti_tahunan,
			SUM(CASE WHEN pv.kode_ket = 6 THEN 1 ELSE 0 END) AS total_cuti_menikah,
			SUM(CASE WHEN pv.kode_ket = 7 THEN 1 ELSE 0 END) AS total_cuti_bersalin,
			SUM(CASE WHEN pv.kode_ket = 8 THEN 1 ELSE 0 END) AS total_cuti_istimewa,
			SUM(CASE WHEN pv.kode_ket = 9 THEN 1 ELSE 0 END) AS total_cuti_non_aktif,
			SUM(CASE WHEN pv.kode_ket = 10 THEN 1 ELSE 0 END) AS total_presentasi,
			SUM(pv.real_lembur_pertama) AS total_lembur_1,
			SUM(pv.real_lembur_akhir) AS total_lembur_next,
			SUM(pv.real_lembur_hari) AS total_real_lembur_hari,
			SUM(pv.real_less_time) AS less_time
		FROM pbu_validasi AS pv
		WHERE pv.status_int = 1 AND pv.in_time >= :in_time AND pv.out_time <= :out_time
		GROUP BY pv.pegawai_id" );
		$datas = $comm->queryAll( true, [
			':in_time'  => $periode->periode_start,
			':out_time' => $periode->periode_end
		] );
		foreach ( $datas as $r ) {
			self::saveData( $periode_id, $r['pegawai_id'], $r['total_hari_kerja'], $r['total_lk'], $r['total_sakit'],
				$r['total_cuti_tahunan'], $r['total_off'], $r['total_lembur_1'], $r['total_lembur_next'],
				( $periode->jumlah_off + $r['total_real_lembur_hari'] ), $r['total_cuti_menikah'], $r['total_cuti_bersalin'],
				$r['total_cuti_istimewa'], $r['total_cuti_non_aktif'], $r['less_time'], $r['total_presentasi'] );
		}
		return count( $datas );
	}
	public static function saveData(
		$periode_id, $pegawai_id, $total_hari_kerja, $total_lk, $total_sakit,
		$total_cuti_tahunan, $total_off, $total_lembur_1, $total_lembur_next,
		$jatah_off, $total_cuti_menikah, $total_cuti_bersalin, $total_cuti_istimewa,
		$total_cuti_non_aktif, $less_time, $total_presentasi	) {
		$comm = Yii::app()->db->createCommand( "INSERT INTO pbu_payroll_absensi
          (payroll_absensi_id, periode_id, total_hari_kerja,locked,tdate,total_lk,total_sakit,total_cuti_tahunan,
          total_off,pegawai_id,total_lembur_1,total_lembur_next,jatah_off,total_cuti_menikah,total_cuti_bersalin,
          total_cuti_istimewa,total_cuti_non_aktif,less_time,total_presentasi) VALUES 
          (uuid(),:periode_id, :total_hari_kerja,0,now(),:total_lk,:total_sakit,:total_cuti_tahunan,
          :total_off,:pegawai_id,:total_lembur_1,:total_lembur_next,:jatah_off,:total_cuti_menikah,:total_cuti_bersalin,
          :total_cuti_istimewa,:total_cuti_non_aktif,:less_time,:total_presentasi) ON CONFLICT(periode_id,pegawai_id)
          DO UPDATE SET total_hari_kerja = :total_hari_kerja,total_lk = :total_lk,total_sakit = :total_sakit,
          total_cuti_tahunan = :total_cuti_tahunan, total_off = :total_off, total_lembur_1 = :total_lembur_1,
          total_lembur_next = :total_lembur_next,jatah_off = :jatah_off,
          total_cuti_menikah = :total_cuti_menikah,total_cuti_bersalin = :total_cuti_bersalin,
          total_cuti_istimewa = :total_cuti_istimewa,total_cuti_non_aktif = :total_cuti_non_aktif,
          less_time = :less_time,total_presentasi = :total_presentasi;" );
		return $comm->execute( [
			':periode_id'           => $periode_id,
			':pegawai_id'           => $pegawai_id,
			':total_hari_kerja'     => $total_hari_kerja,
//            ':locked' => 0,
			':total_lk'             => $total_lk,
			':total_sakit'          => $total_sakit,
			':total_cuti_tahunan'   => $total_cuti_tahunan,
			':total_cuti_menikah'   => $total_cuti_menikah,
			':total_cuti_bersalin'  => $total_cuti_bersalin,
			':total_cuti_istimewa'  => $total_cuti_istimewa,
			':total_cuti_non_aktif' => $total_cuti_non_aktif,
			':total_off'            => $total_off,
			':total_lembur_1'       => $total_lembur_1,
			':total_lembur_next'    => $total_lembur_next,
			':jatah_off'            => $jatah_off,
			':less_time'            => $less_time,
			':total_presentasi'     => $total_presentasi,
		] );
	}
}