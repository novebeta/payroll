jun.EmailTransHistorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.EmailTransHistorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmailTransHistoryStoreId',
            url: 'EmailTransHistory',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'email_trans_history_id'},
                // {name: 'pegawai_id'},
                {name: 'email'},
                {name: 'nama_'},
                {name: 'email_trans_id'},
                {name: 'tdate'},
                {name: 'periode_id'},
                {name: 'host_'},
                {name: 'email_name'},
                {name: 'note_'},
                {name: 'status'},
            ]
        }, cfg));
    }
});
jun.rztEmailTransHistory = new jun.EmailTransHistorystore();
//jun.rztEmailTransHistory.load();
