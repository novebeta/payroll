jun.Bustore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Bustore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BuStoreId',
            url: 'Bu',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'bu_id'},
                {name: 'bu_name'},
                {name: 'bu_nama_alias'}
            ]
        }, cfg));
    }
});
jun.rztBu = new jun.Bustore();
jun.rztBuLib = new jun.Bustore();
jun.rztBuLib.load();
jun.rztBuCmp = new jun.Bustore();
jun.rztBuCmp.on({
    scope: this,
    beforeload: {
        fn: function (a, b) {
            if(jun.bu_id == ''){
                return false;
            }
            b.params.bu_id = jun.bu_id;
        }
    }
});
jun.Itemstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Itemstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ItemstoreId',
            url: 'Trans/items',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'item'}
            ]
        }, cfg));
    }
});
jun.rztItem = new jun.Itemstore();