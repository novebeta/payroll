jun.EmailTransDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "EmailTransDetail",
    id: 'docs-jun.EmailTransDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel(),
    columns: [
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_',
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztEmailTransDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.EmailTransDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.EmailTransGrupPegawaiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.email_trans_detail_id;
        var form = new jun.EmailTransDetailWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelections();
        // var record = this.sm.getSelected();
        // Check is list selected
        if (record.length == 0) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        record.forEach(function (b) {
            jun.rztEmailTransDetail.remove(b);
        });
    }
});
