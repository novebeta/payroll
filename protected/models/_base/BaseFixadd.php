<?php

/**
 * This is the model base class for the table "{{fixadd}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Fixadd".
 *
 * Columns in table "{{fixadd}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $add_id
 * @property string $kode
 * @property string $nama
 *
 */
abstract class BaseFixadd extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{fixadd}}';
	}

	public static function representingColumn() {
		return 'kode';
	}

	public function rules() {
		return array(
			array('add_id, kode', 'required'),
			array('add_id', 'length', 'max'=>36),
			array('kode', 'length', 'max'=>20),
			array('nama', 'length', 'max'=>100),
			array('nama', 'default', 'setOnEmpty' => true, 'value' => null),
			array('add_id, kode, nama', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'add_id' => Yii::t('app', 'Add'),
			'kode' => Yii::t('app', 'Kode'),
			'nama' => Yii::t('app', 'Nama'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('add_id', $this->add_id, true);
		$criteria->compare('kode', $this->kode, true);
		$criteria->compare('nama', $this->nama, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}