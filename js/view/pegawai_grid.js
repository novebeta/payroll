jun.PegawaiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pegawai",
    id: 'docs-jun.PegawaiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik',
            width: 100,
            filter: {xtype: "textfield", filterName: "nik"}
        },
//        {
//            header: 'Kode Leveling',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'leveling_id',
//            renderer: jun.renderLevelKode,
//            width: 100
//        },
//        {
//            header: 'Kode Golongan',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'golongan_id',
//            renderer: jun.renderGolonganKode,
//            width: 100
//        },
        {
            header: 'Nama Lengkap',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_lengkap',
            width: 100,
            filter: {xtype: "textfield", filterName: "nama_lengkap"}
        },
        {
            header: 'Cabang',
            sortable: true,
            resizable: true,
            dataIndex: 'cabang_id',
            renderer: jun.renderCabang,
            width: 100,
            filter: {xtype: "textfield", filterName: "cabang"}
        }
    ],
    initComponent: function () {
        // if (jun.rztJabatanCmp.getTotalCount() === 0)
        jun.rztJabatanCmp.load();
        jun.rztBank.load();
        // if (jun.rztStatusPegawaiCmp.getTotalCount() === 0)
        jun.rztStatusPegawaiCmp.load();
        // if (jun.rztGolonganCmp.getTotalCount() === 0)
        jun.rztGolonganCmp.load();
        // if (jun.rztLevelingCmp.getTotalCount() === 0)
        jun.rztLevelingCmp.load();
        // if (jun.rztCabangCmp.getTotalCount() === 0)
        jun.rztCabangCmp.load();
        // if (jun.rztStatusCmp.getTotalCount() === 0)
        jun.rztStatusCmp.load();
        jun.rztJenisPeriodeCmp.load();
        this.store = jun.rztPegawai;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Buat',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    iconCls: 'silk13-pencil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete', hidden: true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Sinkron',
                    ref: '../btnSoap'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Resign',
                    ref: '../btnResign'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PegawaiGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnResign.on('Click', this.resignForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnSoap.on('Click', this.soapClick, this);
    },
    resignForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pegawai_id;
        var form = new jun.ResignWin({modez: 0, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    soapClick: function () {
        Ext.Ajax.request({
            url: 'Pegawai/sinkron/',
            method: 'POST',
            scope: this,
            params: {
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiCmp.reload();
                jun.rztPegawaiLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PegawaiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.pegawai_id;
        var form = new jun.PegawaiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPegawaiBank.load({
            params: {
                'pegawai_id': idz
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Pegawai/delete/id/' + record.json.pegawai_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPegawai.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
