<?php

/**
 * This is the model base class for the table "{{session}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Session".
 *
 * Columns in table "{{session}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property integer $expire
 * @property string $data
 *
 */
abstract class BaseSession extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{session}}';
	}

	public static function representingColumn() {
		return 'id';
	}

	public function rules() {
		return array(
			array('id', 'required'),
			array('expire', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>32),
			array('data', 'safe'),
			array('expire, data', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, expire, data', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'expire' => Yii::t('app', 'Expire'),
			'data' => Yii::t('app', 'Data'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('expire', $this->expire);
		$criteria->compare('data', $this->data, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}