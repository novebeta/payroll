<?php

/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 8/6/2015
 * Time: 14:46
 */

//Yii::import('application.components.SoapClientYii');
//Yii::import('application.components.GenerateLembur');
class GenerateCommand extends CConsoleCommand
{
    public function actionLembur($store) {
        try {
            $vv = ValidasiView::model()->findAllByAttributes(array('kode_cabang' => $store));
            foreach ($vv as $v){
                $duras = Fp::model()->durasi($v['pin_id'], $v['shift_id'], $v['in_time'], $v['out_time']);
                $validasi = Validasi::model()->findByPk($v['validasi_id']);
                
                $validasi->min_early_time = $duras['min_early'];
                $validasi->min_late_time = $duras['min_late'];
                $validasi->min_least_time = $duras['min_least'];
                $validasi->min_over_time = $duras['min_over'];
                $validasi->min_over_time_awal = $duras['min_over_sejam_awal'];
                $validasi->save();
            }
            
            $rslt = ResultView::model()->findAllByAttributes(array('kode_cabang' => $store));
            foreach ($rslt as $r){
                $result = Result::model()->findByPk($r['result_id']);
                
                $total_min_lembur_awal = Yii::app()->db->createCommand(
                                "SELECT SUM(min_over_time_awal) FROM pbu_validasi_view WHERE result_id = '" . $r['result_id'] . "'")
                                 ->queryScalar();
                $total_min_lembur_akhir = Yii::app()->db->createCommand(
                                 "SELECT SUM(min_over_time) FROM pbu_validasi_view WHERE result_id = '" . $r['result_id'] . "'")
                                 ->queryScalar();  
                $total_min_late_time = Yii::app()->db->createCommand(
                                 "SELECT SUM(min_late_time) FROM pbu_validasi_view WHERE result_id = '" . $r['result_id'] . "'")
                                 ->queryScalar();
                
                $result->total_min_lembur_awal = $total_min_lembur_awal;
                $result->total_min_lembur_akhir = $total_min_lembur_akhir;
                $result->total_min_late_time = $total_min_late_time;
                $result->save();
            }
//            $auww->save();
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
        }        
    }
}
