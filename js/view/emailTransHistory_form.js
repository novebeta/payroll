jun.EmailTransHistoryWin = Ext.extend(Ext.Window, {
    title: 'EmailTransHistory',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-EmailTransHistory',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'pegawai_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pegawai_id',
                        id: 'pegawai_idid',
                        ref: '../pegawai_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        id: 'emailid',
                        ref: '../email',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'nama_',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_',
                        id: 'nama_id',
                        ref: '../nama_',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'email_trans_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email_trans_id',
                        id: 'email_trans_idid',
                        ref: '../email_trans_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'tdate',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tdate',
                        id: 'tdateid',
                        ref: '../tdate',
                        maxLength: 30,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'periode_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'periode_id',
                        id: 'periode_idid',
                        ref: '../periode_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'host_',
                        hideLabel: false,
                        //hidden:true,
                        name: 'host_',
                        id: 'host_id',
                        ref: '../host_',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'email_name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email_name',
                        id: 'email_nameid',
                        ref: '../email_name',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'note_',
                        hideLabel: false,
                        //hidden:true,
                        name: 'note_',
                        id: 'note_id',
                        ref: '../note_',
                        maxLength: 600,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'status',
                        hideLabel: false,
                        //hidden:true,
                        name: 'status',
                        id: 'statusid',
                        ref: '../status',
                        maxLength: 1,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailTransHistoryWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'EmailTransHistory/update/id/' + this.id;
        } else {
            urlz = 'EmailTransHistory/create/';
        }
        Ext.getCmp('form-EmailTransHistory').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztEmailTransHistory.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-EmailTransHistory').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});