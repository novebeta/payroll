jun.Jabatanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Jabatanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JabatanStoreId',
            url: 'Jabatan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jabatan_id'},
                {name: 'kode_jabatan'},
                {name: 'nama_jabatan'}
            ]
        }, cfg));
    }
});
jun.rztJabatan = new jun.Jabatanstore();
jun.rztJabatanCmp = new jun.Jabatanstore();
jun.rztJabatanLib = new jun.Jabatanstore();
//jun.rztJabatan.load();
