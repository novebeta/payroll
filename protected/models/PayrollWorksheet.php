<?php
Yii::import( 'application.models._base.BasePayrollWorksheet' );
class PayrollWorksheet extends BasePayrollWorksheet {
	const CREATE = 'CREATE';
	const UPDATE = 'UPDATE';
	const UPLOAD = 'UPLOAD';
	const ADDED_VALUE = 'ADDED_VALUE';
	const SCHEMA_JOIN = 'SCHEMA_JOIN';
	const SCHEMA_GAJI = 'SCHEMA_GAJI';
	const SCHEMA_DAILY = 'SCHEMA_DAILY';
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->payroll_worksheet_id == null ) {
			$command                    = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                       = $command->queryScalar();
			$this->payroll_worksheet_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		return parent::beforeValidate();
	}
	public static function create(
		$periode_id, $pegawai_id, $schema_id, $trans_type, $amount
	) {
		$model = new PayrollWorksheet;
		if ( Lock::isPeriodeLocked( $periode_id ) ) {
			throw new Exception( 'Periode sudah di lock.' );
		}
		$model->payroll_worksheet_id = U::generate_uuid();
		$model->periode_id           = $periode_id;
		$model->pegawai_id           = $pegawai_id;
		$model->schema_id            = $schema_id;
		$model->trans_type           = $trans_type;
		$model->amount               = $amount;
		if ( ! $model->save() ) {
			throw new Exception( 'Gagal disimpan' );
		}
		return $model;
	}
}