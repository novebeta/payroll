jun.PegawaiWin = Ext.extend(Ext.Window, {
    title: 'Pegawai',
    modez: 1,
    width: 565,
    height: 647,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Pegawai',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "xdatefield",
                        fieldLabel: 'Join Date',
                        ref: "../tgl_masuk",
                        name: "tgl_masuk",
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        // format: "d M Y",
                        anchor: '100%'
                    },
                    {
                        xtype: "xdatefield",
                        fieldLabel: 'Birth Date',
                        ref: "../birth_date",
                        name: "birth_date",
                        //readOnly: true,
                        allowBlank: false,
                        // value: DATE_NOW,
                        // format: "d M Y",
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Lengkap',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_lengkap',
                        id: 'nama_lengkapid',
                        ref: '../nama_lengkap',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'mfcombobox',
                        searchFields: [
                            'kode',
                            'nama'
                        ],
                        itemSelector: 'tr.search-item',
                        tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                            '<th>Kode</th><th>Nama Level</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td>{kode}</td><td>{nama}</td>',
                            '</tr></tpl></tbody></table>'),
                        listWidth: 400,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Leveling',
                        store: jun.rztLevelingCmp,
                        hiddenName: 'leveling_id',
                        valueField: 'leveling_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Golongan',
                        store: jun.rztGolonganCmp,
                        hiddenName: 'golongan_id',
                        valueField: 'golongan_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cabang',
                        store: jun.rztCabangCmp,
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Jabatan',
                        store: jun.rztJabatanCmp,
                        hiddenName: 'jabatan_id',
                        valueField: 'jabatan_id',
                        displayField: 'nama_jabatan',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NIK',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nik',
                        id: 'nikid',
                        ref: '../nik',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        ref: '../email',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'NPWP',
                        hideLabel: false,
                        //hidden:true,
                        name: 'npwp',
                        ref: '../npwp',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status Pegawai',
                        store: jun.rztStatusPegawaiCmp,
                        hiddenName: 'status_pegawai_id',
                        valueField: 'status_pegawai_id',
                        displayField: 'kode',
                        anchor: '100%'
                    },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'Nama Bank',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'bank_nama',
                    //     ref: '../bank_nama',
                    //     maxLength: 50,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'Kota Bank',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'bank_kota',
                    //     ref: '../bank_kota',
                    //     maxLength: 50,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    // {
                    //     xtype: 'textfield',
                    //     fieldLabel: 'Rekening',
                    //     hideLabel: false,
                    //     //hidden:true,
                    //     name: 'rekening',
                    //     ref: '../rekening',
                    //     maxLength: 50,
                    //     //allowBlank: ,
                    //     anchor: '100%'
                    // },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        store: jun.rztStatusCmp,
                        hiddenName: 'status_id',
                        valueField: 'status_id',
                        displayField: 'nama',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Grup Periode',
                        store: jun.rztJenisPeriodeCmp,
                        hiddenName: 'jenis_periode_id',
                        valueField: 'jenis_periode_id',
                        displayField: 'nama_jenis',
                        anchor: '100%'
                    },
                    new jun.PegawaiBankGrid({
                        x: 5,
                        y: 185,
                        height: 175,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                    }),
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Create',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tdate',
                        ref: '../tdate',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'displayfield',
                        fieldLabel: 'Last Update',
                        hideLabel: false,
                        //hidden:true,
                        name: 'last_update',
                        ref: '../last_update',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PegawaiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Pegawai/update/id/' + this.id;
        } else {
            urlz = 'Pegawai/create/';
        }
        Ext.getCmp('form-Pegawai').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztPegawaiBank.data.items, "data")),
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiLib.reload();
                jun.rztPegawaiCmp.reload();
                // jun.rztPinLib.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Pegawai').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});