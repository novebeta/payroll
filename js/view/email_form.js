jun.EmailWin = Ext.extend(Ext.Window, {
    title: 'Email',
    modez: 1,
    width: 500,
    height: 400,
    layout: 'form',
    modal: true,
    padding: 5,
    iswin: !0,
    user_id: 0,
    closeForm: false,
    initComponent: function () {
        jun.rztEmail.on({
            scope: this,
            load: {
                fn: function (s, r, o) {
                    if (r.length > 0) {
                        Ext.getCmp('form-Email').getForm().loadRecord(r[0]);
                    }
                }
            }
        });
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Email',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Akun',
                        hideLabel: false,
                        //hidden:true,
                        name: 'name_',
                        id: 'name_id',
                        ref: '../name_',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Host',
                        emptyText: 'smtp1.example.com;smtp2.example.com',
                        hideLabel: false,
                        //hidden:true,
                        name: 'host_',
                        id: 'host_id',
                        ref: '../host_',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Port',
                        hideLabel: false,
                        //hidden:true,
                        name: 'port_',
                        id: 'port_id',
                        ref: '../port_',
                        maxLength: 5,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'SMTP Auth',
                        boxLabel: "SMTP Auth",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        hideLabel: false,
                        name: 'smtp_auth',
                        id: 'smtp_authid',
                        ref: '../smtp_auth',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'SMTP Secure',
                        hideLabel: false,
                        //hidden:true,
                        name: 'smtp_secure',
                        id: 'smtp_secureid',
                        ref: '../smtp_secure',
                        maxLength: 10,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Username',
                        hideLabel: false,
                        //hidden:true,
                        name: 'username_',
                        id: 'username_id',
                        ref: '../username_',
                        maxLength: 250,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Password',
                        hideLabel: false,
                        //hidden:true,
                        name: 'passw_',
                        id: 'passw_id',
                        ref: '../passw_',
                        inputType: "password",
                        maxLength: 250,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Google Credential',
                        hideLabel: false,
                        //hidden:true,
                        name: 'credentials_',
                        ref: '../credentials_',
                        height: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Google Token',
                        hideLabel: false,
                        //hidden:true,
                        name: 'token_',
                        ref: '../token_',
                        height: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Protokol',
                        store: new Ext.data.ArrayStore({
                            fields: ["kode", "nama"],
                            data: [
                                [0, "SMTP"],
                                [1, "GOOGLE API"]
                            ]
                        }),
                        hiddenName: 'mode_',
                        valueField: 'kode',
                        displayField: 'nama',
                        allowBlank: 0,
                        // readOnly: true,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Get Token',
                    hidden: false,
                    ref: '../btnToken'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnToken.on('click', this.onbtnTokenclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(false);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
        Ext.getCmp('form-Email').getForm().reset();
        jun.rztEmail.load({
            params: {
                id: this.user_id
            }
        });
    },
    onbtnTokenclick: function () {
        // var konten = '<a href="https://www.w3schools.com" target="_blank"/><br/>Enter verification code: ';
        if (this.credentials_.getValue().trim() === '') {
            Ext.MessageBox.alert("Warning", "Credentials harus diisi.");
            return;
        }
        Ext.Ajax.request({
            url: 'Email/token/',
            method: 'POST',
            scope: this,
            params: {
                credentials: this.credentials_.getValue()
            },
            success: function (f, a) {
                var credentials = this.credentials_.getValue();
                var response = Ext.decode(f.responseText);
                Ext.Msg.prompt("Open the following link in your browser", response.msg, function (btnText, sInput) {
                    if (btnText === 'ok') {
                        // this.token_.setValue(sInput);
                        Ext.Ajax.request({
                            url: 'Email/token/',
                            method: 'POST',
                            scope: this,
                            params: {
                                credentials: credentials,
                                token: sInput,
                            },
                            success: function (f, a) {
                                var response = Ext.decode(f.responseText);
                                this.token_.setValue(response.msg);
                                // this.close();
                            },
                            failure: function (f, a) {
                                switch (a.failureType) {
                                    case Ext.form.Action.CLIENT_INVALID:
                                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                        break;
                                    case Ext.form.Action.CONNECT_FAILURE:
                                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                                        break;
                                    case Ext.form.Action.SERVER_INVALID:
                                        Ext.Msg.alert('Failure', a.result.msg);
                                }
                            }
                        });
                    }
                }, this);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisabled: function (status) {
        // this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Email/create/';
        Ext.getCmp('form-Email').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztEmail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Email').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});