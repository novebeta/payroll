<?php
Yii::import('application.models._base.BaseCabang');
class Cabang extends BaseCabang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->cabang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->cabang_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public function isCabangHolding(){
        return (strpos(strtoupper($this->bu->bu_nama_alias),'HOLDING') !== false);
    }
    
    public function checkCabang(){
        $id = Yii::app()->user->getId();
        $nik = Users::model()->findByAttributes(array('id' => $id))->user_id;
        $store = Pegawai::model()->findByAttributes(array('nik' => $nik))->store;
        return $store;
    }
}
