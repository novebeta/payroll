<?php
Yii::import( 'application.components.U' );
Yii::import( "application.components.tbs_class", true );
Yii::import( "application.components.tbs_plugin_excel", true );
Yii::import( 'application.components.phpExcelReportHelper', true );
class ReportController extends GxController {
	private $TBS;
	private $logo;
	private $format;
	public $is_excel;
	public function init() {
		parent::init();
		if ( ! isset( $_POST ) || empty( $_POST ) ) {
			$this->redirect( url( '/' ) );
		}
		$this->logo   = bu() . "/images/bianti2.png";
		$this->TBS    = new clsTinyButStrong;
		$this->layout = "report";
		$this->format = $_POST['format'];
		if ( $this->format == 'excel' ) {
			$this->TBS->PlugIn( TBS_INSTALL, TBS_EXCEL );
			$this->is_excel = true;
		}
		error_reporting( E_ERROR );
	}
	public function actionAbsen() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$periode      = $_POST['periode_id'];
			$cabang       = $_POST['cabang_id'];
			$kode_periode = Yii::app()->db->createCommand(
				"SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'" )
			                              ->queryScalar();
//            $mutasi = U::report_fp($periode,NULL,$cabang);
			$mutasi = U::report_absen( $periode, $cabang );
//            $total;
			$dataProvider = new CArrayDataProvider( $mutasi, array(
				'id'         => 'Absensi',
				'pagination' => false
			) );
			if ( $this->format == 'excel' ) {
				header( 'Content-type: application/vnd.xls' );
				header( "Content-Disposition: attachment; filename=Absensi $kode_periode.xls" );
				echo $this->render( 'Absensi', array(
					'dp'           => $dataProvider,
					'periode_id'   => $periode,
					'kode_periode' => $kode_periode
				), true );
			} else {
				$this->render( 'Absensi', array(
					'dp'           => $dataProvider,
					'periode_id'   => $periode,
					'kode_periode' => $kode_periode
				) );
			}
		}
	}
	public function actionFp() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$periode      = $_POST['periode_id'];
			$pegawai      = $_POST['pegawai_id'];
			$cabang       = $_POST['cabang_id'];
			$kode_periode = Yii::app()->db->createCommand(
				"SELECT kode_periode FROM pbu_periode WHERE periode_id = '" . $periode . "'" )
			                              ->queryScalar();
			$nama         = Yii::app()->db->createCommand(
				"SELECT nama_lengkap FROM pbu_pegawai WHERE pegawai_id = '" . $pegawai . "'" )
			                              ->queryScalar();
			if ( ! $nama ) {
				$nama = 'ALL';
			}
			$kode_cabang  = Cabang::model()->findByPk( $cabang )->kode_cabang;
			$mutasi       = U::report_fp( $periode, $pegawai, $cabang );
			$dataProvider = new CArrayDataProvider( $mutasi, array(
				'id'         => 'Fp',
				'pagination' => false
			) );
			if ( $this->format == 'excel' ) {
				header( 'Content-type: application/vnd.xls' );
				header( "Content-Disposition: attachment; filename=Fp $kode_periode.xls" );
				echo $this->render( 'Fp', array(
					'dp'           => $dataProvider,
					'nama'         => $nama,
					'kode_periode' => $kode_periode,
					'kode_cabang'  => $kode_cabang
				), true );
			} else {
				$this->render( 'Fp', array(
					'dp'           => $dataProvider,
					'nama'         => $nama,
					'kode_periode' => $kode_periode,
					'kode_cabang'  => $kode_cabang
				) );
			}
		}
	}
	public function actionPrintRekapPegawai() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$mutasi = U::report_rekap_pegawai();
			if ( $this->format == 'excel' ) {
				$columnData = array(
					array(
						'A' => '#',
						'B' => 'tgl_masuk',
						'C' => 'nik',
						'D' => 'nama_lengkap',
						'E' => 'kode_leveling',
						'F' => 'kode_golongan',
						'G' => 'nama_jabatan',
						'H' => 'email',
						'I' => 'npwp',
						'J' => 'kode_status',
						'K' => 'bank_nama',
						'L' => 'bank_kota',
						'M' => 'rekening',
						'N' => 'gp',
						'O' => 'tj',
						'P' => 'um',
						'Q' => 'ut'
					)
				);
				$rowc       = count( $mutasi ) + 5;
				$field      = array(
					'A1'     => 'Rekap Pegawai ' . Yii::app()->params['nama_perusahaan'],
					"M$rowc" => $total_income = array_sum( array_column( $mutasi, 'gp' ) ),
					"N$rowc" => $total_income = array_sum( array_column( $mutasi, 'tj' ) )
				);
				$report     = new PhpExcelReportHelper();
				$report->loadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) .
				                       DIRECTORY_SEPARATOR . 'rekap_pegawai.xlsx' )
				       ->setActiveSheetIndex( 0 )
				       ->mergeBlock( $mutasi, $columnData, 5, 1 )
				       ->mergeField( $field )
					//->protectSheet()
					   ->downloadExcel( 'Rekap Pegawai ' . Yii::app()->params['nama_perusahaan'] );
			} else {
				$dataProvider = new CArrayDataProvider( $mutasi, array(
					'id'         => 'RekapPegawai',
					'pagination' => false
				) );
				$this->render( 'RekapPegawai', array(
					'dp' => $dataProvider
				) );
			}
		}
	}
	public function actionPrintMasterGaji() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$mutasi = U::report_master_gaji();
			if ( $this->format == 'excel' ) {
				$field        = array(
					'A2' => Yii::app()->params['nama_perusahaan']
				);
				$field_header = array(
					'A' => '#',
					'B' => 'lnama',
					'C' => 'gnama',
					'D' => 'anama'
				);
				$kol          = 'E';
				/** @var Master[] $m */
				$m           = Master::model()->findAll();
				$field_copy4 = [];
				$field_copy5 = [];
				foreach ( $m as $r ) {
					$field_header[ $kol ] = $r->kode;
					$field_copy4[]        = $kol . "4";
					$field_copy5[]        = $kol . "5";
					$field[ $kol . "4" ]  = $r->kode;
					$kol ++;
				}
				$field[ $kol . '4' ]  = 'Asumsi';
				$field_header[ $kol ] = 'asumsi';
				$field_copy4[]        = $kol . "4";
				$field_copy5[]        = $kol . "5";
				$columnData           = array(
					$field_header
				);
				$report               = new PhpExcelReportHelper();
				$report->loadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) .
				                       DIRECTORY_SEPARATOR . 'master_gaji.xlsx' )
				       ->setActiveSheetIndex( 0 )
				       ->copyFormatField( 'E4', $field_copy4 )
				       ->copyFormatField( 'E5', $field_copy5 )
				       ->mergeBlock( $mutasi, $columnData, 5, 1 )
				       ->mergeField( $field )
					//->protectSheet()
					   ->downloadExcel( 'Master Gaji ' . Yii::app()->params['nama_perusahaan'] );
			} else {
				$dataProvider = new CArrayDataProvider( $mutasi, array(
					'id'         => 'RekapPegawai',
					'pagination' => false
				) );
				$this->render( 'RekapPegawai', array(
					'dp' => $dataProvider
				) );
			}
		}
	}
	public function actionPrintLaporanBank() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$periode_id = $_POST['periode_id'];
			$tipe_bank  = $_POST['tipe_bank'];
			$bank_id    = $_POST['bank_id'];
			$header_col = CJSON::decode( $_POST['detil'] );
//			$header_col = U::report_rekap_payroll_header( $periode_id );
			$h            = '';
			$rHeader      = 5;
			$rData        = 6;
			$kol          = 'J';
			$field_copy4  = [];
			$field_copy5  = [];
			$field_header = [];
			$field        = [];
			foreach ( $header_col as $item ) {
				$h                        .= ',SUM(CASE WHEN ppd.nama_skema = \'' . $item['nama_col'] .
				                             '\' THEN ppd.amount ELSE 0 END) AS "' . $item['nama_col'] . '"';
				$field_header[ $kol ]     = $item['nama_col'];
				$field_copy4[]            = $kol . $rHeader;
				$field_copy5[]            = $kol . $rData;
				$field[ $kol . $rHeader ] = $item['nama_col'];
				$kol ++;
			}
			$mutasi = U::report_laporan_bank_payroll( $periode_id, $bank_id, $tipe_bank, $h );
			/** @var Periode $periode */
			$periode = Periode::model()->findByPk( $periode_id );
			/** @var Bank $bank */
			$bank = Bank::model()->findByPk( $bank_id );
			if ( $this->format == 'excel' ) {
				$field        = array_merge( $field, [
					'A1' => 'Laporan Bank ' . $bank->nama_bank,
					'A2' => 'Periode ' . $periode->kode_periode
				] );
				$field_header = array_merge( $field_header, [
					'A' => '#',
					'B' => 'kode_cab',
					'C' => 'nama_area',
					'D' => 'nik',
					'E' => 'nama_lengkap',
					'F' => 'kode_level',
					'G' => 'nama_jabatan',
					'H' => 'tgl_masuk',
					'I' => 'no_rekening'
				] );
//				$field_header = array(
//					'A' => '#',
//					'B' => 'kode_cab',
//					'C' => 'nama_area',
//					'D' => 'nik',
//					'E' => 'nama_lengkap',
//					'F' => 'kode_level',
//					'G' => 'nama_jabatan',
//					'H' => 'tgl_masuk',
//					'I' => 'no_rekening'
//				);
//				$field[ $kol . $rHeader ] = 'Take Home Pay';
//				$field_header[ $kol ]     = 'take_home_pay';
//				$field_copy4[]            = $kol . $rHeader;
//				$field_copy5[]            = $kol . $rData;
				$columnData = array(
					$field_header
				);
				$report     = new PhpExcelReportHelper();
				$report->loadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) .
				                       DIRECTORY_SEPARATOR . 'laporan_bank.xlsx' )
				       ->setActiveSheetIndex( 0 )
				       ->copyFormatField( 'I' . $rHeader, $field_copy4 )
				       ->copyFormatField( 'I' . $rData, $field_copy5 )
				       ->mergeBlock( $mutasi, $columnData, $rData, 1 )
				       ->mergeField( $field )
					//->protectSheet()
					   ->downloadExcel( 'Laporan Bank ' . $bank->nama_bank . ' Periode ' . $periode->kode_periode );
			} else {
				$dataProvider = new CArrayDataProvider( $mutasi, array(
					'id'         => 'RekapPegawai',
					'pagination' => false
				) );
				$this->render( 'RekapPegawai', array(
					'dp' => $dataProvider
				) );
			}
		}
	}
	public function actionPrintRekapPayroll() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$periode_id = $_POST['periode_id'];
			$header_col = U::report_rekap_payroll_header( $periode_id );
			$mutasi     = U::report_rekap_payroll( $periode_id, $_POST['hide_hk'] == 1 );
			/** @var Periode $periode */
			$periode = Periode::model()->findByPk( $periode_id );
			if ( $this->format == 'excel' ) {
				$field        = array(
					'A1' => 'Rekap Payroll ' . Yii::app()->params['nama_perusahaan'],
					'A2' => 'Periode ' . $periode->kode_periode
				);
				$field_header = array(
					'A' => '#',
					'B' => 'kode_cab',
					'C' => 'nama_area',
					'D' => 'nik',
					'E' => 'nama_lengkap',
					'F' => 'kode_level',
					'G' => 'nama_jabatan',
					'H' => 'tgl_masuk',
					'I' => 'total_hari_kerja',
					'J' => 'total_lk',
					'K' => 'total_sakit',
					'L' => 'total_cuti_tahunan',
					'M' => 'total_off',
					'N' => 'total_cuti_bersalin',
//					'N' => 'ganti_off',
//					'O' => 'total_lembur_1',
//					'P' => 'total_lembur_next'
				);
				$rHeader      = 5;
				$rData        = 6;
				$kol          = 'O';
				$field_copy4  = [];
				$field_copy5  = [];
				foreach ( $header_col as $r ) {
					$field_header[ $kol ]     = $r['nama_skema'];
					$field_copy4[]            = $kol . $rHeader;
					$field_copy5[]            = $kol . $rData;
					$field[ $kol . $rHeader ] = $r['nama_skema'];
					$kol ++;
				}
				$field[ $kol . $rHeader ] = 'Take Home Pay';
				$field_header[ $kol ]     = 'take_home_pay';
				$field_copy4[]            = $kol . $rHeader;
				$field_copy5[]            = $kol . $rData;
				$columnData               = array(
					$field_header
				);
				$report                   = new PhpExcelReportHelper();
				$report->loadTemplate( Yii::getPathOfAlias( 'application.views.reports' ) .
				                       DIRECTORY_SEPARATOR . 'rekap_payroll.xlsx' )
				       ->setActiveSheetIndex( 0 )
				       ->copyFormatField( 'L' . $rHeader, $field_copy4 )
				       ->copyFormatField( 'L' . $rData, $field_copy5 )
				       ->mergeBlock( $mutasi, $columnData, $rData, 1 )
				       ->mergeField( $field )
					//->protectSheet()
					   ->downloadExcel( 'Rekap Payroll ' . Yii::app()->params['nama_perusahaan'] );
			} else {
				$dataProvider = new CArrayDataProvider( $mutasi, array(
					'id'         => 'RekapPegawai',
					'pagination' => false
				) );
				$this->render( 'RekapPegawai', array(
					'dp' => $dataProvider
				) );
			}
		}
	}
	public function actionCetakSlip() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$payroll_id = $_POST['payroll_id'];
			/** @var Payroll $payroll */
			$payroll = Payroll::model()->findByPk( $payroll_id );
			if ( $payroll != null ) {
				$select  = "SELECT * FROM pbu_payroll_details AS am WHERE am.type_ = :type_ 
                  AND am.payroll_id = :payroll_id ORDER BY seq";
				$inc     = Yii::app()->db->createCommand( $select )->queryAll( true, [
					':type_'      => 1,
					':payroll_id' => $payroll->payroll_id
				] );
				$out     = Yii::app()->db->createCommand( $select )->queryAll( true, [
					':type_'      => - 1,
					':payroll_id' => $payroll->payroll_id
				] );
				$details = [];
				$j_inc   = count( $inc );
				$j_out   = count( $out );
				$cnt     = 0;
				if ( $j_inc >= $j_out ) {
					for ( $cnt; $cnt < $j_inc; $cnt ++ ) {
						$details[] = [
							'in_n' => $inc[ $cnt ]['nama_skema'],
							'in_a' => ': ' . format_number_print( $inc[ $cnt ]['amount'] ),
							'ot_n' => $cnt < $j_out ? $out[ $cnt ]['nama_skema'] : '',
							'ot_a' => $cnt < $j_out ? ': ' . format_number_print( $out[ $cnt ]['amount'] ) : '',
						];
					}
				} else {
					for ( $cnt; $cnt < $j_out; $cnt ++ ) {
						$details[] = [
							'in_n' => $cnt < $j_inc ? $inc[ $cnt ]['nama_skema'] : '',
							'in_a' => $cnt < $j_inc ? ': ' . format_number_print( $inc[ $cnt ]['amount'] ) : '',
							'ot_n' => $out[ $cnt ]['nama_skema'],
							'ot_a' => ': ' . format_number_print( $out[ $cnt ]['amount'] ),
						];
					}
				}
				for ( $cnt; $cnt < 11; $cnt ++ ) {
					$details[] = [
						'in_n' => '',
						'in_a' => '',
						'ot_n' => '',
						'ot_a' => '',
					];
				}
				$mPDF1 = Yii::app()->ePdf->mpdf( 'utf-8', array( 279.4, 228.6 ) );
				$mPDF1->WriteHTML( $this->render( 'CetakSlip', [
					'periode'  => $payroll->periode->kode_periode,
					'tgl'      => get_date_today( 'dd-MM-yyyy' ),
					'cabang'   => $payroll->kode_cab,
					'jabatan'  => $payroll->nama_jabatan,
					'gol'      => $payroll->kode_gol,
					'lvl'      => $payroll->kode_level,
					'hk'       => $payroll->total_hari_kerja,
					'nik'      => $payroll->nik,
					'nama'     => $payroll->nama_lengkap,
					'email'    => $payroll->email,
					'status'   => $payroll->nama_status,
					'npwp'     => $payroll->npwp,
					'ktp'      => '-',
					'inc'      => format_number_print( $payroll->total_income ),
					'pot'      => format_number_print( $payroll->total_deduction ),
					'thp'      => format_number_print( $payroll->take_home_pay ),
					'mandiri'  => '-',
					'tabungan' => '-',
					'cicilan'  => '-',
					'note'     => '-',
					'details'  => $details,
					'bu_id'    => $payroll->pegawai->cabang->bu_id
				], true ) );
				$mPDF1->Output();
			}
		}
	}
	public function actionRekapAbsensi() {
		if ( Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$periodeStart = Yii::app()->db->createCommand( "SELECT DATE_FORMAT(periode_start,'%Y-%m-%d') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'" )->queryScalar();
			$periodeEnd   = Yii::app()->db->createCommand( "SELECT DATE_FORMAT(periode_end,'%Y-%m-%d') FROM pbu_periode where periode_id = '" . $_POST['periode_id'] . "'" )->queryScalar();
			$kode_periode = Periode::model()->findByPk( $_POST['periode_id'] )->kode_periode;
			$kode_cabang  = Cabang::model()->findByPk( $_POST['cabang_id'] )->kode_cabang;
			$pegawai      = Yii::app()->db->createCommand(
				"select nik, nama_lengkap from pbu_pegawai where store = :kode_cabang order by nik"
			)->queryAll( true, [ ':kode_cabang' => $kode_cabang ] );
			$query        = "select * from 
                        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date,
                        DATE_FORMAT(adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i),'%d') tgl from
                        (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                        (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                        (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                        (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                        (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) s
                        where selected_date >= DATE_FORMAT(:from, '%Y-%m-%d') and selected_date <= DATE_FORMAT(:to, '%Y-%m-%d')";
			$tgl          = Yii::app()->db->createCommand( $query )->queryAll( true, [
				':from' => $periodeStart,
				':to'   => $periodeEnd
			] );
			$periode      = Yii::app()->db->createCommand( "
            select t2.*, 
            CASE
                WHEN kode_ket = 0 THEN 1
                WHEN kode_ket = 1 THEN 'LK'
                WHEN kode_ket = 2 THEN 'S'
                WHEN kode_ket = 3 THEN 1
                WHEN kode_ket = 4 THEN 0
                WHEN kode_ket is null THEN '-'
                ELSE 'C'
            END masuk
            from
            (
                    SELECT
                    t1.*, p.nama_lengkap, p.nik
                    FROM
                    (
                        $query
                    ) t1,
                    pbu_pegawai p
            ) t2
            LEFT JOIN pbu_validasi_view v on v.PIN = t2.nik and DATE_FORMAT(v.in_time, '%Y-%m-%d') = t2.selected_date
            order by t2.nik, t2.selected_date
            " )->queryAll( true, [
					':from' => $periodeStart,
					':to'   => $periodeEnd
				]
			);
			$data         = array();
			$cmasuk       = 1;
			$csakit       = 1;
			$clk          = 1;
			$coff         = 1;
			$ccuti        = 1;
			foreach ( $pegawai as $p ) {
				$row                 = [];
				$row['nik']          = $p['nik'];
				$row['nama_lengkap'] = $p['nama_lengkap'];
				foreach ( $tgl as $t ) {
					foreach ( $periode as $d ) {
						if ( $d['nik'] == $p['nik'] && $d['tgl'] == $t['tgl'] ) {
							$row[ $t['tgl'] ] = $d['masuk'];
//                            if($d['masuk'] == 1){
//                                $row['cmasuk'] = $cmasuk++;                                
//                            }   
							switch ( $d['masuk'] ) {
								case '0' :
									$row['coff'] = $coff ++;
									break;
								case '1' :
									$row['cmasuk'] = $cmasuk ++;
									break;
								case 'LK' :
									$row['clk'] = $clk ++;
									break;
								case 'S' :
									$row['csakit'] = $csakit ++;
									break;
								case 'C' :
									$row['ccuti'] = $ccuti ++;
									break;
							}
							if ( $coff <= 1 ) {
								$row['coff'] = '-';
							}
							if ( $cmasuk <= 1 ) {
								$row['cmasuk'] = '-';
							}
							if ( $clk <= 1 ) {
								$row['clk'] = '-';
							}
							if ( $csakit <= 1 ) {
								$row['csakit'] = '-';
							}
							if ( $ccuti <= 1 ) {
								$row['ccuti'] = '-';
							}
							break;
						}
					}
				}
				$coff   = 1;
				$cmasuk = 1;
				$clk    = 1;
				$csakit = 1;
				$ccuti  = 1;
				$data[] = $row;
			}
			$dataProvider = new CArrayDataProvider( $data, array(
				'id'         => 'RekapAbsensi',
				'pagination' => false
			) );
			if ( $this->format == 'excel' ) {
				header( 'Content-type: application/vnd.xls' );
				header( "Content-Disposition: attachment; filename=RekapPresensi $kode_periode $kode_cabang.xls" );
				echo $this->render( 'RekapAbsensi', array(
					'dp'           => $dataProvider,
					'tgl'          => $tgl,
					'from'         => sql2date( $periodeStart, 'dd MMM yyyy' ),
					'to'           => sql2date( $periodeEnd, 'dd MMM yyyy' ),
					'kode_periode' => $kode_periode,
					'kode_cabang'  => $kode_cabang
				), true );
			} else {
				$this->render( 'RekapAbsensi', array(
					'dp'           => $dataProvider,
					'tgl'          => $tgl,
					'from'         => sql2date( $periodeStart, 'dd MMM yyyy' ),
					'to'           => sql2date( $periodeEnd, 'dd MMM yyyy' ),
					'kode_periode' => $kode_periode,
					'kode_cabang'  => $kode_cabang
				) );
			}
		}
	}
	public function actionMandiri() {
		$ket1      = $_POST['ket1'];
		$ket2      = $_POST['ket2'];
		$rek_induk = $_POST['rek_induk'];
//		$tgl_proses = $_POST['tgl_proses'];
		$timestamp  = CDateTimeParser::parse( $_POST['tgl_proses'], 'yyyy-MM-dd' );
		$tgl_proses = Yii::app()->dateFormatter->format( 'yyyyMMdd', $timestamp );
		/** @var Periode $periode */
		$periode = Periode::model()->findByPk( $_POST['periode_id'] );
		if ( $periode == null ) {
			throw new CException( 'Periode tidak ditemukan' );
		}
		/** @var Payroll[] $payrolls */
		$payrolls = Payroll::model()->findAllByAttributes( [
			'periode_id' => $periode->periode_id
		] );
		if ( $payrolls == null ) {
			throw new CException( 'Payroll tidak ditemukan' );
		}
		$fileDownload  = 'MANDIRI.xlsx';
		$cellTglProses = 'B1';
		$cellRekInduk  = 'C1';
		$cellTotData   = 'D1';
		$cellTotAmount = 'E1';
		$firstRow      = 2;
		$colRek        = 'A';
		$colNama       = 'B';
		$colGaji       = 'G';
		$colKet1       = 'H';
		$colKet2       = 'I';
		$colBu         = 'AO';
		$colNik        = 'AP';
		/** @var Bu $bu */
//		$bu     = Bu::model()->findByPk( $periode->jenisPeriode->bu_id );
//		$kodeBu = $bu->bu_kode;
		try {
			$inputFileType = 'Xlsx';
			$inputFileName = Yii::getPathOfAlias( 'application.views.reports' ) .
			                 DIRECTORY_SEPARATOR . 'mandiri.xlsx';
			$reader        = \PhpOffice\PhpSpreadsheet\IOFactory::createReader( $inputFileType );
			/**  Load $inputFileName to a Spreadsheet Object  **/
			$spreadsheet = $reader->load( $inputFileName );
//			$worksheet = $spreadsheet->getSheetByName( 'Format Upload Mandiri' );
			$worksheet    = $spreadsheet->setActiveSheetIndexByName( 'Format Upload Mandiri' );
			$rowIndex     = $firstRow;
			$total_amount = 0;
			foreach ( $payrolls as $payroll ) {
				$thp = round( doubleval( $payroll->take_home_pay ) );
				$worksheet->setCellValue( $colRek . $rowIndex, $payroll
					->pegawai->getRekeningBank( 'MANDIRI', 'Payroll' ) )
				          ->setCellValue( $colNama . $rowIndex, substr( $payroll->pegawai->nama_lengkap, 0, 30 ) )
				          ->setCellValue( 'F' . $rowIndex, 'IDR' )
				          ->setCellValue( 'J' . $rowIndex, 'IBU' )
				          ->setCellValue( 'Q' . $rowIndex, 'N' )
				          ->setCellValue( 'R' . $rowIndex, 'payntshrol@gmail.com' )
				          ->setCellValue( $colGaji . $rowIndex, $thp )
				          ->setCellValue( $colKet1 . $rowIndex, $ket1 )
				          ->setCellValue( $colKet2 . $rowIndex, $ket2 )
				          ->setCellValue( $colBu . $rowIndex, $payroll->pegawai->cabang->kode_cabang )
				          ->setCellValue( $colNik . $rowIndex, $payroll->pegawai->nik );
				$rowIndex ++;
				$total_amount += $thp;
				$worksheet->setCellValue( $cellTglProses, $tgl_proses )
				          ->setCellValue( $cellTotData, count( $payrolls ) )
				          ->setCellValue( $cellRekInduk, $rek_induk )
				          ->setCellValue( $cellTotAmount, $total_amount );
			}
//			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $spreadsheet, "Xlsx" );
			$this->downloadExcel( $spreadsheet, $fileDownload );
		} catch ( Exception $e ) {
			throw new CException( $e->getMessage() );
		}
	}
	private function downloadExcel( $spreadsheet, $filename ) {
		// Redirect output to a client’s web browser (Xlsx)
		header( 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' );
		header( 'Content-Disposition: attachment;filename="' . $filename . '"' );
		header( 'Cache-Control: max-age=0' );
// If you're serving to IE 9, then the following may be needed
		header( 'Cache-Control: max-age=1' );
// If you're serving to IE over SSL, then the following may be needed
		header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' ); // Date in the past
		header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' ); // always modified
		header( 'Cache-Control: cache, must-revalidate' ); // HTTP/1.1
		header( 'Pragma: public' ); // HTTP/1.0
		try {
			$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter( $spreadsheet, 'Xlsx' );
			$writer->save( 'php://output' );
		} catch ( Exception $e ) {
			throw new CException( $e->getMessage() );
		}
	}
}
