<?php
Yii::import('application.models._base.BaseSrLeveling');
class SrLeveling extends BaseSrLeveling
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->sr_leveling == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sr_leveling = $uuid;
        }
        return parent::beforeValidate();
    }
}