jun.HistorySinkronGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sinkronisasi",
    id: 'docs-jun.HistorySinkronGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 100
        },
        {
            header: 'Add New Record',
            sortable: true,
            resizable: true,
            dataIndex: 'insert_',
            width: 100
        },
        {
            header: 'Update Record',
            sortable: true,
            resizable: true,
            dataIndex: 'update_',
            width: 100
        },
        {
            header: 'Time',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            width: 100
        },
        {
            header: 'User',
            sortable: true,
            resizable: true,
            dataIndex: 'user_id',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztPeriodeLib.load();
        this.store = jun.rztHistorySinkron;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'panel',
                    frame: false,
                    bodyStyle: 'background-color: #E4E4E4;',
                    height: 50,
                    width: 700,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    defaults: {flex: 1},
                    border: false,
                    // tbar: [],
                    items: [
                        {
                            xtype: 'toolbar',
                            // columns: 4,
                            // defaults: {
                            //     scale: 'small'
                            // },
                            items: [
                                {
                                    xtype: 'label',
                                    style: 'margin:5px',
                                    text: 'http://'
                                },
                                {
                                    xtype: 'textfield',
                                    ref: '../../../host',
                                    id: 'hosthistorySinkron_id',
                                    width: 250
                                },
                                {
                                    xtype: 'label',
                                    style: 'margin:5px',
                                    text: 'Authorization :'
                                },
                                {
                                    xtype: 'textfield',
                                    ref: '../../../auth',
                                    id: 'authhistorySinkron_id',
                                    width: 300
                                }
                            ]
                        },
                        {
                            xtype: 'toolbar',
                            // columns: 4,
                            // defaults: {
                            //     scale: 'large'
                            // },
                            items: [
                                {
                                    xtype: 'label',
                                    style: 'margin:5px',
                                    text: 'Model '
                                },
                                new jun.comboTipeSinkron({
                                    emptyText: '-  Pilih Sinkron  -',
                                    id: 'comboTipeSinkron_id',
                                    ref: '../../../status',
                                    width: 150
                                }),
                                {
                                    xtype: 'tbseparator'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Reload History',
                                    // height: 44,
                                    ref: '../../../btnReloadHistory'
                                },
                                {
                                    xtype: 'tbseparator'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Sinkron Data',
                                    // height: 44,
                                    ref: '../../../btnSinkronData'
                                },
                                {
                                    xtype: 'tbseparator'
                                },
                                {
                                    xtype: 'button',
                                    text: 'Sinkron by Bisnis Unit',
                                    // height: 44,
                                    ref: '../../../btnSinkronDataByUnit'
                                }
                            ]
                        }
                    ]
                }
            ]
        };
        jun.HistorySinkronGrid.superclass.initComponent.call(this);
        this.btnReloadHistory.on('Click', this.btnReloadHistoryClick, this);
        this.btnSinkronData.on('Click', this.btnSinkronDataClick, this);
        this.btnSinkronDataByUnit.on('Click', this.sinkronByBuClick, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    sinkronByBuClick: function () {
        Ext.MsgBoxCus.show({
            title: 'Bisnis Unit',
            msg: 'Pilih salah satu :',
            // value: 'choice 2',
            buttons: Ext.MessageBox.OKCANCEL,
            inputField: new Ext.form.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                forceSelection: true,
                fieldLabel: 'Bisnis Unit',
                store: jun.rztBuLib,
                hiddenName: 'bu_id',
                valueField: 'bu_id',
                displayField: 'bu_name',
                matchFieldWidth: !1,
                allowBlank: false,
                listWidth: 450,
                lastQuery: "",
                width: 250
            }),
            fn: function (buttonId, text) {
                if (buttonId == 'ok') {
                    var host = Ext.getCmp('hosthistorySinkron_id').getValue();
                    var auth = Ext.getCmp('authhistorySinkron_id').getValue();
                    var combo_value = Ext.getCmp('comboTipeSinkron_id').getValue();
                    if (host == '' || auth == '' || combo_value == '') {
                        Ext.Msg.alert('Failure', 'Host, Auth dan tipe harus diisi.');
                        return;
                    }
                    var params = {};
                    params.bu_id = text;
                    params.host = host;
                    params.auth = auth;
                    params.model = combo_value;
                    Ext.Ajax.request({
                        url: 'HistorySinkron/sinkron/',
                        method: 'POST',
                        scope: this,
                        params: params,
                        success: function (f, a) {
                            var response = Ext.decode(f.responseText);
                            Ext.MessageBox.show({
                                title: 'Info',
                                msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.INFO
                            });
                        },
                        failure: function (f, a) {
                            switch (a.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                    Ext.Msg.alert('Failure', a.result.msg);
                            }
                        }
                    });
                }
            }
        });
    },
    btnReloadHistoryClick: function () {
        var combo_value = Ext.getCmp('comboTipeSinkron_id').getValue();
        if (combo_value != '') {
            jun.rztHistorySinkron.load({
                params: {
                    'model': combo_value
                }
            });
        }
    },
    funcSinkron: function (params) {
        Ext.Ajax.request({
            url: 'HistorySinkron/sinkron/',
            method: 'POST',
            scope: this,
            params: params,
            success: function (f, a) {
                jun.rztPegawai.reload();
                jun.rztPegawaiCmp.reload();
                jun.rztPegawaiLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: (response.msg == '' ? 'Sinkronisasi berhasil' : response.msg),
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnSinkronDataClick: function () {
        var host = Ext.getCmp('hosthistorySinkron_id').getValue();
        var auth = Ext.getCmp('authhistorySinkron_id').getValue();
        var combo_value = Ext.getCmp('comboTipeSinkron_id').getValue();
        if (host == '' || auth == '' || combo_value == '') {
            Ext.Msg.alert('Failure', 'Host, Auth dan tipe harus diisi.');
            return;
        }
        var params = {};
        params.bu_id = jun.bu_id;
        params.host = host;
        params.auth = auth;
        params.model = combo_value;
        if (combo_value == 'validasi') {
            Ext.MsgBoxCus.show({
                title: 'Periode',
                msg: 'Which one?',
                scope: this,
                prompt: true,
                buttons: Ext.MessageBox.OKCANCEL,
                inputField: new Ext.form.ComboBox({
                    xtype: 'combo',
                    style: 'margin-bottom:2px',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    fieldLabel: 'Periode',
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                    valueField: 'periode_id',
                    emptyText: 'Pilih Periode',
                    ref: '../periode_id',
                    displayField: 'kode_periode',
                    width: 300,
                    anchor: '100%',
                    itemSelector: "div.search-item",
                    tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                        '<div style="width:100%;display:inline-block;">\n\
                            <span style="font-weight: bold">{kode_periode}</span>\n\
                            <br>\n\{periode_start} - {periode_end}\n\
                            </div>',
                        "</div></tpl>")
                }),
                fn: function (buttonId, text) {
                    if (buttonId == 'ok') {
                        // Ext.Msg.alert('Your Choice', 'You chose: "' + text + '".');
                        params.periode_id = text;
                        this.funcSinkron(params);
                    }
                }
            });
        } else {
            this.funcSinkron(params);
        }
    },
});