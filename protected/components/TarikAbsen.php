<?php

Yii::import('application.vendors.*');
require 'tad/TADFactory.php';
require 'tad/TAD.php';
require 'tad/TADResponse.php';
require 'tad/Providers/TADSoap.php';
require 'tad/Providers/TADZKLib.php';
require 'tad/Exceptions/ConnectionError.php';
require 'tad/Exceptions/FilterArgumentError.php';
require 'tad/Exceptions/UnrecognizedArgument.php';
require 'tad/Exceptions/UnrecognizedCommand.php';
Yii::import('application.components.U');

class TarikAbsen {

    public $count, $info;

    public function Absensi($tglin, $cabang) {
        $fp_id = 1;
        $count = 0;
        $info1 = $info2 = '';
        $tgl = date('Y-m-d');
        $hariini = date('Y-m-d', strtotime($tgl));
        $ipz = Yii::app()->db->createCommand(
                        "SELECT kode_ip FROM pbu_ip where cabang = '$cabang'")
                ->queryAll();
        
        foreach ($ipz as $k) {
            $ip = $k['kode_ip'];
            $cabang = Yii::app()->db->createCommand(
                            "SELECT cabang FROM pbu_ip where kode_ip = '$ip'")
                    ->queryScalar();
            $comkey = Ip::model()->findByAttributes(array('kode_ip' => $ip))->com_key;
//        $tgl = get_date_today('yyyy-MM-dd%');
            $options = [
                'ip' => "$ip", // '169.254.0.1' by default (totally useless!!!).
                'internal_id' => $fp_id, // 1 by default.
                'com_key' => $comkey, // 0 by default.
                'description' => 'TAD1', // 'N/A' by default.
                'soap_port' => 80, // 80 by default,
                'udp_port' => 4370, // 4370 by default.
                'encoding' => 'utf-8'    // iso8859-1 by default.
            ];
            $tad_factory = new TADPHP\TADFactory($options);
            $tad = $tad_factory->get_instance();

            try {
                $att_logs = $tad->get_att_log();
            } catch (Exception $ex) {
                $info1 = "Gagal! " . $ex->getMessage() . " cabang $cabang" . PHP_EOL;
                continue;
            }

//            $kemarin = date('Y-m-d', strtotime($tgl . "-1 days"));
            $filtered_att_logs = $att_logs->filter_by_date(
                    ['start' => "$tglin", 'end' => "$tglin"]
            );
            $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
            if (!isset($result->Row)) {
                $info2 = "Data absen cabang $cabang pada tgl $tglin tidak ada." . PHP_EOL;
                continue;
            }            
            Fp::model()->deleteAll("DateTime_ between '$tglin 00:00:00' AND '$tglin 23:59:59' AND cabang = '$cabang'");
            $multi = new CDbMultiInsertCommand(new Fp());
            $ROW = $result->Row;
            foreach ($ROW as $row) {
                $length = strlen($row->PIN);
                if($length == 5){
                    $pinold = substr($row->PIN, 1,4);
                    $pin = "10$pinold";
                } else {
                    $pin = substr($row->PIN, -6);
                }

                $fp = new Fp();
                $fp->PIN = $pin;
                $fp->PIN_real = $row->PIN;
                $fp->DateTime_ = $row->DateTime;
                $fp->Verified = $row->Verified;
                $fp->Status = $row->Status;
                $fp->WorkCode = $row->WorkCode;
                $fp->terminal_id = $fp_id;
//                $fp->tdate = NOW();
                $fp->tdate = $hariini . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
                $fp->cabang = $cabang;
                $multi->add($fp);
                //echo "Menyimpan $pin pada tanggal $row->DateTime dari $cabang" . PHP_EOL;
                $count++;
            }
            if ($multi->getCountModel() > 0) {
                $multi->execute();           
            }
        }
        $all = array($count,$info1,$info2);
        return $all;
    }
//    public function Absensi($tgl, $ip) {
//        $fp_id = 1;
//        $this->count = 0;
//        $this->info = [];
//        $cabang = Yii::app()->db->createCommand(
//                        "SELECT cabang FROM pbu_ip where kode_ip = '$ip'")
//                ->queryScalar();
////        $tgl = get_date_today('yyyy-MM-dd%');
//        $options = [
//            'ip' => "$ip", // '169.254.0.1' by default (totally useless!!!).
//            'internal_id' => $fp_id, // 1 by default.
//            'com_key' => 12369, // 0 by default.
//            'description' => 'TAD1', // 'N/A' by default.
//            'soap_port' => 80, // 80 by default,
//            'udp_port' => 4370, // 4370 by default.
//            'encoding' => 'utf-8'    // iso8859-1 by default.
//        ];
//        $tad_factory = new TADPHP\TADFactory($options);
//        $tad = $tad_factory->get_instance();
//        try {
//            $att_logs = $tad->get_att_log();
//        } catch (Exception $ex) {
//            $this->info[] = "Gagal! Tidak ada koneksi : " . $ex->getMessage();
//            return;
//        }
//
//        if ($att_logs->is_empty_response()) {
//            $this->info[] = 'The employee does not have logs recorded';
//            return;
//        }
//
//        $kemarin = date('Y-m-d', strtotime($tgl));
//        $filtered_att_logs = $att_logs->filter_by_date(
//                ['start' => "$kemarin", 'end' => "$kemarin"]
//        );
////        $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
//        $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
//        if (!isset($result->Row)) {
//            $this->info[] = "Data absen cabang <b>$cabang</b> pada tgl <b>$kemarin</b> tidak ada.";
//            return;
//        }
////        Fp::model()->deleteAll('terminal_id = :terminal_id AND DATE(DateTime_) = :date', array(':terminal_id' => $fp_id, ':date' => $tgl));
//        Fp::model()->deleteAll("DateTime_ like '$kemarin%' AND cabang = '$cabang'"); //        
////        var_dump($result);
//        $multi = new CDbMultiInsertCommand(new Fp());
//        $ROW = $result->Row;
//
//        if (isset($ROW->PIN)) {
//            foreach ($ROW as $row) {
//                $fp = new Fp();
//                $fp->PIN = substr($ROW->PIN, -6);
//                $fp->PIN_real = $ROW->PIN;
//                $fp->DateTime_ = $ROW->DateTime;
//                $fp->Verified = $ROW->Verified;
//                $fp->Status = $ROW->Status;
//                $fp->WorkCode = $ROW->WorkCode;
//                $fp->terminal_id = $fp_id;
//                $fp->tdate = NOW();
//                $fp->cabang = $cabang;
//                $multi->add($fp);
//                $this->info[] = "Menyimpan $ROW->PIN pada tanggal $ROW->DateTime dari $cabang" . PHP_EOL;
//                $this->count++;
//            }
//        } else {
//            foreach ($ROW as $row) {
//                $fp = new Fp();
//                $fp->PIN = substr($row->PIN, -6);
//                $fp->PIN_real = $row->PIN;
//                $fp->DateTime_ = $row->DateTime;
//                $fp->Verified = $row->Verified;
//                $fp->Status = $row->Status;
//                $fp->WorkCode = $row->WorkCode;
//                $fp->terminal_id = $fp_id;
//                $fp->tdate = NOW();
//                $fp->cabang = $cabang;
//                $multi->add($fp);
//                $this->info[] = "Menyimpan $row->PIN pada tanggal $row->DateTime dari $cabang" . PHP_EOL;
//                $this->count++;
//            }
//        }
//
//        $this->info[] = "Total absen yang disimpan $this->count" . PHP_EOL;
//        if ($multi->getCountModel() > 0) {
//            $multi->execute();
//        }
//    }

}
