jun.EmailTransDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.EmailTransDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmailTransDetailStoreId',
            url: 'EmailTransDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'email_trans_detail_id'},
                {name: 'email_trans_id'},
                {name: 'pegawai_id'},
                {name: 'nama_'},
                {name: 'grup_'}
            ]
        }, cfg));
    }
});
jun.rztEmailTransDetail = new jun.EmailTransDetailstore();
//jun.rztEmailTransDetail.load();
