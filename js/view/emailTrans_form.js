jun.EmailTransWin = Ext.extend(Ext.Window, {
    title: 'Pegawai',
    modez: 1,
    width: 400,
    height: 352,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    selectedz: null,
    initComponent: function () {
        jun.rztPegawaiEmail.load();
        this.items = new Ext.grid.GridPanel({
            id: 'grid-pegawai-email',
            store: jun.rztPegawaiEmail,
            ref: '../grid',
            // multiSelect: true,
            height: 275,
            viewConfig: {
                forceFit: true
            },
            sm: new Ext.grid.RowSelectionModel(),
            // plugins: [new Ext.ux.grid.GridHeaderFilters],
            // reserveScrollOffset: true,
            columns: [
                {
                    header: 'NIK',
                    sortable: true,
                    resizable: true,
                    dataIndex: 'nik',
                    width: 100
                },
                {
                    header: 'Nama Lengkap',
                    sortable: true,
                    resizable: true,
                    dataIndex: 'nama_lengkap',
                    width: 100
                },
            ]
        });
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        Ext.getCmp('grid-pegawai-email').getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.selectedz = sm.getSelections();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        Ext.Ajax.request({
            url: 'EmailTrans/create/',
            method: 'POST',
            scope: this,
            params: {
                pegawai: Ext.encode(Ext.pluck(this.selectedz, "data")),
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztPegawaiEmail.reload();
                jun.rztEmailTrans.reload();
                this.btnDisabled(false);
                // var response = Ext.decode(f.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                // this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.EmailTransGrupPegawaiWin = Ext.extend(Ext.Window, {
    title: 'Pegawai',
    modez: 1,
    width: 300,
    height: 352,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    selectedz: null,
    initComponent: function () {
        jun.rztPegawaiEmail.load();
        this.items = new Ext.grid.GridPanel({
            id: 'grid-pegawai-email',
            store: jun.rztPegawaiEmail,
            ref: '../grid',
            frameHeader: !1,
            header: !1,
            // multiSelect: true,
            height: 275,
            viewConfig: {
                forceFit: true
            },
            sm: new Ext.grid.RowSelectionModel(),
            // plugins: [new Ext.ux.grid.GridHeaderFilters],
            // reserveScrollOffset: true,
            columns: [
                {
                    header: 'NIK',
                    sortable: true,
                    resizable: true,
                    dataIndex: 'nik',
                    width: 50
                },
                {
                    header: 'Nama Lengkap',
                    sortable: true,
                    resizable: true,
                    dataIndex: 'nama_lengkap'
                },
            ]
        });
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailTransGrupPegawaiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        Ext.getCmp('grid-pegawai-email').getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.selectedz = sm.getSelections();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        this.selectedz.forEach(function (r) {
            var c = jun.rztEmailTransDetail.recordType,
                d = new c({
                    pegawai_id: r.data.pegawai_id,
                    email: r.data.email,
                    nama_: r.data.nama_lengkap,
                });
            jun.rztEmailTransDetail.add(d);
        });
        this.btnDisabled(false);
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.EmailTransGrupWin = Ext.extend(Ext.Window, {
    title: 'Pegawai',
    modez: 1,
    width: 400,
    height: 382,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    selectedz: null,
    initComponent: function () {
        jun.rztPegawaiEmail.load();
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_',
                        ref: '../nama_',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        ref: '../email',
                        maxLength: 100,
                        anchor: '100%'
                    },
                    new jun.EmailTransDetailGrid({
                        height: 230,
                        ref: '../emailTransDetailGrid',
                        frameHeader: !1,
                        header: !1,
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailTransGrupWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        // Ext.getCmp('grid-pegawai-email').getSelectionModel().on('rowselect', this.getrow, this);
    },
    // getrow: function (sm, idx, r) {
    //     this.selectedz = sm.getSelections();
    // },
    onWinClose: function () {
        jun.rztEmailTransDetail.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        Ext.Ajax.request({
            url: 'EmailTrans/creategrup/',
            method: 'POST',
            scope: this,
            params: {
                nama_: this.nama_.getValue(),
                email: this.email.getValue(),
                pegawai: Ext.encode(Ext.pluck(jun.rztEmailTransDetail.data.items, "data")),
                bu_id: jun.bu_id
            },
            success: function (f, a) {
                jun.rztPegawaiEmail.reload();
                jun.rztEmailTrans.reload();
                this.btnDisabled(false);
                // var response = Ext.decode(f.responseText);
                // Ext.MessageBox.show({
                //     title: 'Info',
                //     msg: response.msg,
                //     buttons: Ext.MessageBox.OK,
                //     icon: Ext.MessageBox.INFO
                // });
                // this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.EmailTransGridPreviewWin = Ext.extend(Ext.Window, {
    title: 'Email Preview',
    modez: 1,
    width: 675,
    height: 352,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    selectedz: null,
    initComponent: function () {
        this.items = new jun.EmailTransGridPreview({
            frameHeader: !1,
            header: !1,
            ref: '../grid',
            height: 275
        });
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Send',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.EmailTransGridPreviewWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        // this.btnDisabled(true);
        // var json = Ext.pluck(grid.getSelectionModel().getSelections(),'json');
        Ext.Ajax.request({
            url: 'EmailTransHistory/sendAll', //+ record.json.email_trans_history_id,
            method: 'POST',
            scope: this,
            params: {
                details: Ext.encode(Ext.pluck(Ext.getCmp('docs-jun.EmailTransGridPreview').getSelectionModel().getSelections(), 'json')),
                periode_id: Ext.getCmp('email_trans_history_periode_id').getValue()
            },
            success: function (f, a) {
                // jun.rztEmailTransHistory.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        // this.btnDisabled(false);
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});