jun.EmailTransHistoryGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Email History",
    id: 'docs-jun.EmailTransHistoryGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_'
        },
        {
            header: 'Waktu Kirim',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate'
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note_'
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status'
        }
    ],
    initComponent: function () {
        jun.rztPeriodeCmp.load();
        this.store = jun.rztEmailTransHistory;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 100
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                // 'Pegawai :',
                // {
                //     xtype: 'mfcombobox',
                //     searchFields: [
                //         'nik',
                //         'nama_lengkap'
                //     ],
                //     itemSelector: 'tr.search-item',
                //     tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                //         '<th>NIK</th><th>Nama Lengkap</th></tr></thead>',
                //         '<tbody><tpl for="."><tr class="search-item">',
                //         '<td>{nik}</td><td>{nama_lengkap}</td>',
                //         '</tr></tpl></tbody></table>'),
                //     listWidth: 400,
                //     mode: 'local',
                //     forceSelection: true,
                //     store: jun.rztPegawaiCmp,
                //     hiddenName: 'pegawai_id',
                //     valueField: 'pegawai_id',
                //     displayField: 'nik',
                //     anchor: '100%',
                //     id: 'payroll_worksheet_pegawai_id',
                //     ref: '../../../pegawai'
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                'Periode :',
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPeriodeCmp,
                    hiddenName: 'periode_id',
                    valueField: 'periode_id',
                    displayField: 'kode_periode',
                    anchor: '100%',
                    id: 'email_trans_history_periode_id',
                    ref: '../periode'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Reload',
                    ref: '../btnReload'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Send All Email',
                    ref: '../btnSendAll'
                }
            ]
        };
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: 'Tambah',
        //             ref: '../btnAdd'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Ubah',
        //             ref: '../btnEdit'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Hapus',
        //             ref: '../btnDelete'
        //         }
        //     ]
        // };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.EmailTransHistoryGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnSendAll.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.EmailTransHistoryWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.email_trans_history_id;
        var form = new jun.EmailTransHistoryWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var periode_id = Ext.getCmp('email_trans_history_periode_id').getValue();
        if (periode_id == '') {
            Ext.MessageBox.alert("Warning", "Anda belum periode");
            return;
        }
        var form = new jun.EmailTransGridPreviewWin();
        form.show(this);
        // Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin mengirim slip gaji?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        // var record = this.sm.getSelected();
        // // Check is list selected
        // if (record == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
        //     return;
        // }
    }
});
