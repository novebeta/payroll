<?php

Yii::import('application.models._base.BaseArea');

class Area extends BaseArea {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->area_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->area_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
