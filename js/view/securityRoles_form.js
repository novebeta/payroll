jun.SecurityRolesWin = Ext.extend(Ext.Window, {
    title: "Security Role",
    modez: 1,
    width: 800,
    height: 500,
    layout: "form",
    modal: !0,
    padding: 3,
    resizable: !1,
    closeForm: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-SecurityRoles",
                labelWidth: 100,
                labelAlign: "left",
                layout: "accordion",
                ref: "formz",
                border: !1,
                anchor: "100% 100%",
                items: [
                    {
                        xtype: "panel",
                        title: "Description",
                        layout: "form",
                        bodyStyle: "padding: 10px",
                        items: [
                            {
                                xtype: "textfield",
                                fieldLabel: "Role Name",
                                hideLabel: !1,
                                name: "role",
                                id: "roleid",
                                ref: "../role",
                                maxLength: 30,
                                anchor: "100%"
                            },
                            {
                                xtype: "textarea",
                                fieldLabel: "Description",
                                hideLabel: !1,
                                name: "ket",
                                id: "ketid",
                                ref: "../ket",
                                maxLength: 255,
                                anchor: "100%"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Master Section",
                        layout: "column",
                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Shift",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "101"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Shift per Hari",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "102"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Keterangan",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "103"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Template",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "106"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Barang Asset",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "116"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Bank",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "103"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Chart of Accounts Class",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "104"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "Master Bank",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "134"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Grup Periode",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "129"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Periode",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "105"
                            },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Periode Join",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "133"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Chart of Accounts",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "106"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Skema Harga",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "107"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Purchase Price",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "108"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Sales Price",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "109"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Area",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "110"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Customers",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "111"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Salesman",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "112"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Supplier",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "113"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Kategori Sales Order",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "114"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Pajak",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "115"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Group Barang Assets",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "117"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "Area",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "115"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "116"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Leveling",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "117"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Golongan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "118"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Komponen Gaji",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "119"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Master Gaji",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "120"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Schema",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "127"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Formula",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "121"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status Pegawai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "123"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Jabatan",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "124"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Cabang",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "125"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Status",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "126"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Pegawai",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "122"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Resign",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "132"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Fix Add",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "130"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Add Value",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "131"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Bisnis Unit",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "128"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Sinkronisasi",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "135"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Template Email",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "136"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Transaction Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Assign Shift",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "224"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Review Absensi Fase 1",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "223"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Review Absensi Fase 2",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "225"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: "Payroll",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "226"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Payroll Worksheet",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "229"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Absensi Payroll",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "227"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Lock Payroll",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "228"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "History Email",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "230"
                            },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Pembelian Barang Asset",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "224"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Purchase Requisition",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "201"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Purchase Order",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "202"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Goods Receival Notes",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "203"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Supplier Invoices",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "204"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Supplier Payments",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "205"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Supplier Return",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "206"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Supplier Credit Notes",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "207"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Sales Orders",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "208"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Delivery",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "209"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Sales Invoices",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "210"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Customer Payments",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "211"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Customer Return",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "212"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Customer Credit Notes",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "213"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Claim",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "214"
                            // },
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Production",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "215"
                            //},
                            //{
                            //    columnWidth: .33,
                            //    boxLabel: "Production Return",
                            //    value: 0,
                            //    inputValue: 1,
                            //    uncheckedValue: 0,
                            //    name: "216"
                            //},
                            // {
                            //    columnWidth: .33,
                            //     boxLabel: "Inventory Adjustments",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "217"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Bank Deposits",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "218"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Bank Payments",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "219"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "General Journal",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "220"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Bank transfer",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "221"
                            // },
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: "Buat Laba Rugi",
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "222"
                            // }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Report Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            // {
                            //     columnWidth: .33,
                            //     boxLabel: 'Report FP & Absen',
                            //     value: 0,
                            //     inputValue: 1,
                            //     uncheckedValue: 0,
                            //     name: "301"
                            // },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Pegawai',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "302"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Master Gaji',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "305"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Payroll',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "303"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Rekap Absensi',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "304"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: 'Report Bank',
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "306"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "Administration Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "User Management",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "401"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Email",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "404"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Security Roles",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "402"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Backup / Restore",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "403"
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        title: "General Section",
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;padding: 10px",
                        defaultType: "checkbox",
                        items: [
                            {
                                columnWidth: .33,
                                boxLabel: "Change Password",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "001"
                            },
                            {
                                columnWidth: .33,
                                boxLabel: "Logout",
                                value: 0,
                                inputValue: 1,
                                uncheckedValue: 0,
                                name: "002"
                            }
                        ]
                    },
                    new jun.CabangBuGrid({
                        modez: this.modez == 0 ? 0 : this.id
                        // frameHeader: !1,
                        // header: !1
                    }),
                    new jun.LevelingSRGrid({
                        modez: this.modez == 0 ? 0 : this.id
                        // frameHeader: !1,
                        // header: !1
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Save & Close",
                    ref: "../btnSaveClose"
                },
                {
                    xtype: "button",
                    text: "Close",
                    ref: "../btnCancel"
                }
            ]
        };
        jun.SecurityRolesWin.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSaveClose.on("click", this.onbtnSaveCloseClick, this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnCancel.on("click", this.onbtnCancelclick, this);
    },
    btnDisabled: function (a) {
        this.btnSaveClose.setDisabled(a);
    },
    onActivate: function () {
        this.btnSaveClose.hidden = !1;
    },
    saveForm: function () {
        this.btnDisabled(!0);
        var a;
        this.modez == 1 || this.modez == 2 ? a = "SecurityRoles/update/id/" + this.id : a = "SecurityRoles/create/";
        Ext.getCmp("form-SecurityRoles").getForm().submit({
            url: a,
            timeOut: 1e3,
            scope: this,
            params: {
                cabang: Ext.encode(Ext.pluck(
                    jun.rztCabangBu.data.items, "data")),
                level: Ext.encode(Ext.pluck(
                    jun.rztLevelingSR.data.items, "data"))
            },
            success: function (a, b) {
                jun.rztSecurityRoles.reload();
                var c = Ext.decode(b.response.responseText);
                this.close();
//                    this.closeForm ? this.close() : (c.data != undefined && Ext.MessageBox.alert("Pelayanan", c.data.msg),
//                        this.modez == 0 && Ext.getCmp("form-SecurityRoles").getForm().reset());
            },
            failure: function (a, b) {
                Ext.MessageBox.alert("Error", "Can't Communicate With The Server");
                this.btnDisabled(!1);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = !0;
        this.saveForm(!0);
    },
    onbtnSaveclick: function () {
        this.closeForm = !1;
        this.saveForm(!1);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});