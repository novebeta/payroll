jun.EmailTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Template Email Trans",
    id: 'docs-jun.EmailTransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_',
            width: 100
        },
        {
            header: 'Email',
            sortable: true,
            resizable: true,
            dataIndex: 'email',
            width: 100
        },
        {
            header: 'Is Grup',
            sortable: true,
            resizable: true,
            dataIndex: 'grup_',
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztEmailTrans;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add Group',
                    ref: '../btnAddGroup'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Ubah',
                //     ref: '../btnEdit'
                // },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.EmailTransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnAddGroup.on('Click', this.loadFormGrup, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.EmailTransWin({modez: 0});
        form.show();
    },
    loadFormGrup: function () {
        var form = new jun.EmailTransGrupWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.email_trans_id;
        var form = new jun.EmailTransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'EmailTrans/delete/id/' + record.json.email_trans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztEmailTrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.EmailTransGridPreview = Ext.extend(Ext.grid.GridPanel, {
    title: "Template Email Trans",
    id: 'docs-jun.EmailTransGridPreview',
    iconCls: "silk-grid",
    // view: new Ext.grid.GroupingView({
    //     forceFit: true
    // }),
    viewConfig: {
        forceFit: true
    },
    columns: [
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_',
            width: 100
        },
        {
            header: 'Email',
            sortable: true,
            resizable: true,
            dataIndex: 'email',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztEmailGroupPreview.load({
            params: {
                bu_id: jun.bu_id
            }
        });
        this.store = jun.rztEmailGroupPreview;
        jun.EmailTransGridPreview.superclass.initComponent.call(this);
    },
});