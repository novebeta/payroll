jun.Resignstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Resignstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ResignStoreId',
            url: 'Resign',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'resign_id'},
                {name: 'tgl'},
                {name: 'note_'},
                {name: 'user_id'},
                {name: 'tdate'},
                {name: 'pegawai_id'}
            ]
        }, cfg));
    }
});
jun.rztResign = new jun.Resignstore();
//jun.rztResign.load();
