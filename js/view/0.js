Ext.reg('uctextfield', Ext.ux.form.UCTextField);
jun.bulan = new Ext.data.ArrayStore({
    fields: ["noBulan", "namaBulan"],
    data: [
        ["01", "Januari"],
        ["02", "Februari"],
        ["03", "Maret"],
        ["04", "April"],
        ["05", "Mei"],
        ["06", "Juni"],
        ["07", "Juli"],
        ["08", "Agustus"],
        ["09", "September"],
        ["10", "Oktober"],
        ["11", "November"],
        ["12", "Desember"]
    ]
});
jun.comboBulan = Ext.extend(Ext.form.ComboBox, {
    displayField: "namaBulan",
    valueField: "noBulan",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    emptyText: "Pilih Bulan",
    selectOnFocus: !0,
    lastQuery: "",
    initComponent: function () {
        this.store = jun.bulan, jun.comboBulan.superclass.initComponent.call(this)
    }
});
jun.rotationSchema = new Ext.data.ArrayStore({
    fields: ["kode", "nama"],
    data: [
        ["D", "DAILY"],
        ["M", "MONTHLY"]
    ]
});
jun.cmbRotationSchema = Ext.extend(Ext.form.ComboBox, {
    displayField: "nama",
    valueField: "kode",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    emptyText: "Pilih Rotasi",
    selectOnFocus: !0,
    lastQuery: "",
    initComponent: function () {
        this.store = jun.rotationSchema;
        jun.cmbRotationSchema.superclass.initComponent.call(this);
    }
});
jun.methodPay = new Ext.data.ArrayStore({
    fields: ["id", "text"],
    data: [
        ["Tunai", "Tunai"],
        ["Debet", "Debet"],
        ["Transfer", "Transfer"],
        ["Cek", "Cek"],
        ["Bilyet Giro (BG)", "Bilyet Giro (BG)"]
    ]
});
jun.comboPayment = Ext.extend(Ext.form.ComboBox, {
    displayField: "text",
    valueField: "id",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    emptyText: "Pilih Cara Bayar",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.methodPay, jun.comboPayment.superclass.initComponent.call(this)
    }
});
jun.kelompok = new Ext.data.ArrayStore({
    fields: ["id", "text"],
    data: [
        [0, "Konsumen"],
        [1, "Karyawan"],
        [2, "Pemasok"],
        [3, "Lainnya"]
    ]
});
jun.cmbKelompok = Ext.extend(Ext.form.ComboBox, {
    displayField: "text",
    valueField: "id",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    emptyText: "Pilih Kelompok",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.kelompok, jun.cmbKelompok.superclass.initComponent.call(this)
    }
});
jun.Gender = new Ext.data.ArrayStore({
    fields: ["code", "desc"],
    data: [
        ["M", "Laki-laki"],
        ["F", "Perempuan"]
    ]
});
jun.cmbGender = Ext.extend(Ext.form.ComboBox, {
    displayField: "desc",
    valueField: "code",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    emptyText: "Pilih Jenis Kelamin",
    selectOnFocus: !0,
    name: "gender",
    hiddenName: "gender",
    initComponent: function () {
        this.store = jun.Gender, jun.cmbGender.superclass.initComponent.call(this)
    }
});
jun.PeriodeGaji = new Ext.data.ArrayStore({
    fields: ["code", "desc"],
    data: [
        ["DAILY", "Harian"],
        ["WEEKLY", "Mingguan"],
        ["MONTHLY", "Bulanan"]
    ]
});
jun.cmbPeriodeGaji = Ext.extend(Ext.form.ComboBox, {
    displayField: "desc",
    valueField: "code",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    // emptyText: "",
    selectOnFocus: !0,
    name: "periode_",
    hiddenName: "periode_",
    initComponent: function () {
        this.store = jun.PeriodeGaji;
        jun.cmbPeriodeGaji.superclass.initComponent.call(this);
    }
});
jun.SatuanMax = new Ext.data.ArrayStore({
    fields: ["code", "desc"],
    data: [
        ["CTN", "CTN"],
        ["BAG", "BAG"],
        ["KRAT", "KRAT"],
        ["JRGN", "JRGN"]
    ]
});
jun.cmbSatuanMax = Ext.extend(Ext.form.ComboBox, {
    displayField: "desc",
    valueField: "code",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    emptyText: "Pilih Satuan Terbesar",
    selectOnFocus: !0,
    name: "sat_max",
    hiddenName: "sat_max",
    initComponent: function () {
        this.store = jun.SatuanMax, jun.cmbSatuanMax.superclass.initComponent.call(this)
    }
});
jun.active = new Ext.data.ArrayStore({
    fields: ["activeVal", "activeName"],
    data: [
        [0, "Non Aktif"],
        [1, "Aktif"]
    ]
});
jun.comboActive = Ext.extend(Ext.form.ComboBox, {
    displayField: "activeName",
    valueField: "activeVal",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    hiddenValue: 1,
    value: 1,
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.active, jun.comboActive.superclass.initComponent.call(this)
    }
});
jun.renderActive = function (a, b, c) {
    return a == 1 ? "Aktif" : "Non Aktif"
};
jun.renderSudah = function (a, b, c) {
    return a == 1 ? '<img alt="" src="js/ext340/examples/shared/icons/fam/accept.png" class="x-action-col-icon x-action-col-0  " ext:qtip="Sudah Final">' :
        '<img alt="" src="js/ext340/examples/shared/icons/fam/error.png" class="x-action-col-icon x-action-col-0  " ext:qtip="Belum Final">';
};
jun.bagian = new Ext.data.ArrayStore({
    fields: ["id", "text"],
    data: [
        ['Salesman', "Salesman"],
        ['Driver', "Driver"],
        ['Helper', "Helper"]
    ]
});
jun.comboBagian = Ext.extend(Ext.form.ComboBox, {
    displayField: "id",
    valueField: "text",
    mode: "local",
    forceSelection: !0,
    typeAhead: !0,
    triggerAction: "all",
    name: "bagian",
    hiddenName: "bagian",
    emptyText: "Pilih Bagian",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.bagian, jun.comboBagian.superclass.initComponent.call(this)
    }
});
jun.faktor = new Ext.data.ArrayStore({
    fields: ["id", "text"],
    data: [
        [1, "Penambah"],
        [-1, "Pengurang"]
    ]
});
jun.cmbFaktor = Ext.extend(Ext.form.ComboBox, {
    displayField: "text",
    valueField: "id",
    mode: "local",
    forceSelection: !0,
    typeAhead: !0,
    triggerAction: "all",
    hiddenName: "type_",
    emptyText: "Pilih Faktor",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.faktor;
        jun.cmbFaktor.superclass.initComponent.call(this);
    }
});
jun.renderFaktor = function (a, b, c) {
    if (a == -1) {
        return 'Pengurang';
    } else if (a == 1) {
        return 'Penambah';
    }
    return '';
};
jun.OpenClose = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "CLOSED"]
    ]
});
jun.comboOpenClose = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.OpenClose;
        jun.comboOpenClose.superclass.initComponent.call(this);
    }
});
jun.StatusGRN = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "SUPPLIER INVOICE"]
    ]
});
jun.comboStatusGRN = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusGRN;
        jun.comboStatusGRN.superclass.initComponent.call(this);
    }
});
jun.StatusSO = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "RECEVIED"],
        ["2", "INVOICED"],
        ["3", "CANCELED"]
    ]
});
jun.StatusDe = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "INVOICED"]
    ]
});
jun.getShift = function (a) {
    var b = jun.rztShiftLib, c = b.findExact("shift_id", a);
    return b.getAt(c);
};
jun.renderKodeShift = function (a, b, c) {
    var jb = jun.getShift(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode_shift;
};
jun.getDays = function (a) {
    var b = jun.rztDaysLib, c = b.findExact("day_id", a);
    return b.getAt(c);
};
jun.renderNamaDays = function (a, b, c) {
    var jb = jun.getDays(a);
    if (jb == null) {
        return '';
    }
    return jb.data.day_name;
};
jun.getPin = function (a) {
    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
    return b.getAt(c);
};
jun.getNama = function () {
    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
    return b.getAt(c);
};
jun.renderPIN = function (a, b, c) {
    var jb = jun.getPin(a);
    if (jb == null) {
        return '';
    }
    return jb.data.PIN;
};
jun.renderNamaLengkap = function (a, b, c) {
    var jb = jun.getPin(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama_lengkap;
};
jun.getStatus = function (a) {
    var b = jun.rztStatusLib;
    c = b.findExact("status_id", a);
    return b.getAt(c);
};
jun.renderStatus = function (a, b, c) {
    var jb = jun.getStatus(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};
jun.getLevel = function (a) {
    var b = jun.rztLevelingLib;
    c = b.findExact("leveling_id", a);
    return b.getAt(c);
};
jun.renderLevelKode = function (a, b, c) {
    var jb = jun.getLevel(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode;
};
jun.renderLeveling = function (a, b, c) {
    var test = jun.rztLevelingLib.findExact("leveling_id", a);
    var data = jun.rztLevelingLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.renderLevelNama = function (a, b, c) {
    var jb = jun.getLevel(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};
jun.getGolongan = function (a) {
    var b = jun.rztGolonganLib;
    c = b.findExact("golongan_id", a);
    return b.getAt(c);
};
jun.renderGolongan = function (a, b, c) {
    var test = jun.rztGolonganLib.findExact("golongan_id", a);
    var data = jun.rztGolonganLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.renderGolonganKode = function (a, b, c) {
    var jb = jun.getGolongan(a);
    if (jb == null) {
        return '';
    }
    return jb.data.kode;
};
jun.renderGolonganNama = function (a, b, c) {
    var jb = jun.getGolongan(a);
    if (jb == null) {
        return '';
    }
    return jb.data.nama;
};
jun.comboStatusSO = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusSO;
        jun.comboStatusSO.superclass.initComponent.call(this);
    }
});
jun.comboxx = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusSO;
        jun.comboStatusSO.superclass.initComponent.call(this);
    }
});
jun.comboStatusDe = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusDe;
        jun.comboStatusDe.superclass.initComponent.call(this);
    }
});
jun.StatusPO = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "RECEIVED"],
        ["2", "INVOICED"],
        ["3", "CANCELED"]
    ]
});
jun.comboStatusPO = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusPO;
        jun.comboStatusPO.superclass.initComponent.call(this);
    }
});
jun.StatusPR = new Ext.data.ArrayStore({
    fields: ["val", "name"],
    data: [
        ["0", "OPEN"],
        ["1", "CLOSED"],
        ["2", "CANCELED"],
        ["3", "REJECTED"]
    ]
});
jun.comboStatusPR = Ext.extend(Ext.form.ComboBox, {
    displayField: "name",
    valueField: "val",
    typeAhead: !0,
    mode: "local",
    forceSelection: !0,
    triggerAction: "all",
    name: "status",
    hiddenName: "status",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusPR;
        jun.comboStatusPR.superclass.initComponent.call(this);
    }
});
jun.renderSatusPR = function (a, b, c) {
    var test = jun.StatusPR.findExact("val", a);
    var data = jun.StatusPR.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "UNKNOWN";
    return data.data.name;
};
jun.renderSatusPO = function (a, b, c) {
    var test = jun.StatusPO.findExact("val", a);
    var data = jun.StatusPO.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "UNKNOWN";
    return data.data.name;
};
jun.renderSatusGRN = function (a, b, c) {
    var test = jun.StatusGRN.findExact("val", a);
    var data = jun.StatusGRN.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "UNKNOWN";
    return data.data.name;
};
jun.renderSatusSO = function (a, b, c) {
    var test = jun.StatusSO.findExact("val", a);
    var data = jun.StatusSO.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "UNKNOWN";
    return data.data.name;
};
jun.renderPegawai = function (a, b, c) {
    var test = jun.rztPegawaiLib.findExact("pegawai_id", a);
    var data = jun.rztPegawaiLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama_lengkap;
};
jun.renderNIK = function (a, b, c) {
    var test = jun.rztPegawaiLib.findExact("pegawai_id", a);
    var data = jun.rztPegawaiLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nik;
};
jun.renderPeriode = function (a, b, c) {
    var test = jun.rztPeriodeLib.findExact("periode_id", a);
    var data = jun.rztPeriodeLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.kode_periode;
};
jun.renderArea = function (a, b, c) {
    var test = jun.rztAreaLib.findExact("area_id", a);
    var data = jun.rztAreaLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.renderMaster = function (a, b, c) {
    var test = jun.rztMasterLib.findExact("master_id", a);
    var data = jun.rztMasterLib.getAt(test);
    if (data == null || data == "" || data == undefined)
        return "";
    return data.data.nama;
};
jun.Golstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Golstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GolStoreId',
            url: 'Gol',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {
                    name: 'gol_id'
                },
                {
                    name: 'nama_gol'
                }
            ]
        }, cfg));
    }
});
jun.rztGol = new jun.Golstore();
jun.rztGolLib = new jun.Golstore();
jun.rztGolCmp = new jun.Golstore();
jun.getGrup = function (a) {
    var b = jun.rztGrup, c = b.findExact("grup_id", a);
    return b.getAt(c)
};
jun.getPerlengkapan = function (a) {
    var b = jun.rztPerlengkapanItem;
    c = b.findExact("perlengkapan_item_id", a);
    return b.getAt(c);
};
jun.getNotePerlengkapan = function (a) {
    var b = jun.rztPerlengkapanItem;
    c = b.findExact("perlengkapan_item_id", a);
    return b.getAt(c);
};
jun.getTglPerlengkapan = function (a) {
    var b = jun.rztPerlengkapanItem;
    c = b.findExact("perlengkapan_item_id", a);
    return b.getAt(c);
};
jun.getBarang = function (a) {
    var b = jun.rztBarangLib;
    c = b.findExact("barang_id", a);
    return b.getAt(c)
};
jun.renderPerlengkapanNote = function (a, b, c) {
    var jb = jun.getNotePerlengkapan(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.note;
};
jun.getWilayah = function (a) {
    var b = jun.rztWilIntLib;
    c = b.findExact("wil_int_id", a);
    return b.getAt(c)
};
jun.getTax = function (a) {
    var brg = jun.getBarang(a);
    var grp = jun.getGrup(brg.data.grup_id);
    var vat = 0;
    if (grp.data.tax == "1" && grp.data.vat > 0) {
        vat = grp.data.vat / 100;
    }
    return vat;
};
jun.isCatJasa = function (a) {
    var brg = jun.getBarang(a);
    var grp = jun.getGrup(brg.data.grup_id);
    return grp.data.kategori_id == 1;
};
jun.renderWilayah = function (a, b, c) {
    var jb = jun.getWilayah(a);
    return jb.data.nama_wilayah;
};
// jun.getGol = function (a) {
//     var b = jun.rztGolLib, c = b.findExact("add_id", a);
//     return b.getAt(c)
// };
// jun.renderGol = function (a, b, c) {
//     var jb = jun.getGol(a);
//     return jb.data.nama_gol;
// };
jun.getGol = function (a) {
    var b = jun.rztGolLib, c = b.findExact("gol_id", a);
    return b.getAt(c)
};
jun.renderGol = function (a, b, c) {
    var jb = jun.getGol(a);
    return jb.data.nama_gol;
};
jun.getKategori = function (a) {
    var b = jun.rztKategori, c = b.findExact("kategori_id", a);
    return b.getAt(c)
};
jun.renderKategori = function (a, b, c) {
    var jb = jun.getKategori(a);
    return jb.data.nama_kategori;
};
jun.getNegara = function (a) {
    var b = jun.rztNegaraLib, c = b.findExact("negara_id", a);
    return b.getAt(c)
};
jun.renderNegara = function (a, b, c) {
    var jb = jun.getNegara(a);
    return jb.data.nama_negara;
};
jun.getProvinsi = function (a) {
    var b = jun.rztProvinsiLib, c = b.findExact("provinsi_id", a);
    return b.getAt(c)
};
jun.renderProvinsi = function (a, b, c) {
    var jb = jun.getProvinsi(a);
    return jb.data.nama_provinsi;
};
jun.getKota = function (a) {
    var b = jun.rztKotaLib, c = b.findExact("kota_id", a);
    return b.getAt(c)
};
jun.renderKota = function (a, b, c) {
    var jb = jun.getKota(a);
    return jb.data.nama_kota;
};
jun.getBank = function (a) {
    var b = jun.rztBankLib, c = b.findExact("bank_id", a);
    return b.getAt(c)
};
jun.renderBank = function (a, b, c) {
    var jb = jun.getBank(a);
    return jb.data.nama_bank;
};
jun.getShift = function (a) {
    var b = jun.rztShiftLib, c = b.findExact("shift_id", a);
    return b.getAt(c)
};
jun.renderKodeShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.kode_shift;
};
jun.renderInShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.in_time;
};
jun.renderOutShift = function (a, b, c) {
    var jb = jun.getShift(a);
    return jb.data.out_time;
};
jun.getShift2 = function (a) {
    var b = jun.rztAssignShift, c = b.findExact("shift_id", a);
    return b.getAt(c)
};
jun.renderKodeShift2 = function (a, b, c) {
    var jb = jun.getShift2(a);
    return jb.data.kode_shift;
};
jun.getCabang = function (a) {
    var b = jun.rztCabangCmp, c = b.findExact("cabang_id", a);
    return b.getAt(c)
};
jun.renderCabang = function (a, b, c) {
    var jb = jun.getCabang(a);
    return jb.data.kode_cabang;
};
jun.getSchema = function (a) {
    var b = jun.rztSchemaLib, c = b.findExact("schema_id", a);
    return b.getAt(c)
};
jun.renderSchemaPaycode = function (a, b, c) {
    var jb = jun.getSchema(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.paycode;
};
jun.getUser = function (a) {
    var b = jun.rztUsersLib, c = b.findExact("id", a);
    return b.getAt(c)
};
jun.renderUser = function (a, b, c) {
    var jb = jun.getUser(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.user_id + " [" + jb.data.name_ + "]";
};
//jun.getPin = function (a) {
//    var b = jun.rztPinLib, c = b.findExact("pin_id", a);
//    return b.getAt(c)
//};
jun.BackupRestoreWin = Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Backup / Restore",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4; padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file restore (*.pbu.gz)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Delete All Transaction",
                    hidden: !1,
                    ref: "../btnDelete"
                },
                {
                    xtype: "button",
                    text: "Download Backup",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Upload Restore",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.BackupRestoreWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
        this.btnDelete.on("click", this.deleteRec, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().reset();
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "Site/BackupAll";
        var form = Ext.getCmp('form-BackupRestoreWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-BackupRestoreWin").getForm().standardSubmit = false;
        Ext.getCmp("form-BackupRestoreWin").getForm().url = "site/RestoreAll";
        if (Ext.getCmp("form-BackupRestoreWin").getForm().isValid()) {
            Ext.getCmp("form-BackupRestoreWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your restore...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Confirm', 'Are you sure delete all transaction?', this.btnDeleteClick, this);
    },
    btnDeleteClick: function (btn) {
        if (btn == 'no') {
            return;
        }
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: 'site/DeleteTransAll',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
//======================================= Untuk Baca File Excel===============================
jun.dataStockOpname = '';
jun.dataSales = '';
jun.namasheet = '';

function readFileExcel(e) {
    dataStockOpname = "";
    if (itemFile.files.length == 0) return;
    var files = itemFile.files;
    var f = files[0];
    {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
            var data = e.target.result;
            var wb;
            var arr = fixdata(data);
            wb = XLS.read(btoa(arr), {type: 'base64'});
            jun.dataStockOpname = to_json(wb);
            jun.namasheet = wb.SheetNames[0];
        };
        reader.readAsArrayBuffer(f);
    }
}

function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLS.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}

function fixdata(data) {
    var o = "", l = 0, w = 10240;
    for (; l < data.byteLength / w; ++l) o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l * w + w)));
    o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
    return o;
}

//======================================= END OF : Untuk Baca File Excel=============================
Ext.form.MFComboBox = Ext.extend(Ext.form.ComboBox, {
    xtype: 'combo',
    searchFields: [this.valueField],
    doQuery: function (q, forceAll) {
        q = Ext.isEmpty(q) ? '' : q;
        var qe = {
            query: q,
            forceAll: forceAll,
            combo: this,
            cancel: false
        };
        if (this.fireEvent('beforequery', qe) === false || qe.cancel) {
            return false;
        }
        q = qe.query;
        forceAll = qe.forceAll;
        if (forceAll == undefined) {
            forceAll = false;
        }
        if (forceAll === true || (q.length >= this.minChars)) {
            if (this.lastQuery !== q) {
                this.lastQuery = q;
                if (this.mode == 'local') {
                    this.selectedIndex = -1;
                    if (forceAll) {
                        this.store.clearFilter();
                    } else {
                        this.store.clearFilter();
                        if (q != '') {
                            this.store.filterBy(function (r, i) {
                                var reg = q.toLowerCase() + ".*";
                                var patt = new RegExp(reg);
                                var hasil = false;
                                for (var i = 0, len = this.searchFields.length; i < len; i++) {
                                    var str = r.get(this.searchFields[i]);
                                    test = false;
                                    if (str != undefined) {
                                        test = patt.test(str.toLowerCase());
                                    }
                                    hasil = hasil || test;
                                }
                                return hasil;
                            }, this);
                        }
                    }
                    this.onLoad();
                } else {
                    this.store.baseParams[this.queryParam] = q;
                    this.store.load({
                        params: this.getParams(q)
                    });
                    this.expand();
                }
            } else {
                this.selectedIndex = -1;
                this.onLoad();
            }
        }
    }
});
Ext.reg('mfcombobox', Ext.form.MFComboBox);
//JENIS BANK
jun.TipeBank = new Ext.data.ArrayStore({
    fields: ["tipe_bank"],
    data: [
        ["BPR"],
        ["Payroll"],
        ["Pribadi"]
    ]
});
jun.comboTipeBank = Ext.extend(Ext.form.ComboBox, {
    displayField: "tipe_bank",
    valueField: "tipe_bank",
    typeAhead: !0,
    mode: "local",
    forceSelection: true,
    triggerAction: "all",
    name: "tipe_bank",
    hiddenName: "tipe_bank",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.TipeBank;
        jun.comboTipeBank.superclass.initComponent.call(this);
    }
});
//STATUS FFOTOCOPY
jun.StatusFotocopy = new Ext.data.ArrayStore({
    fields: ["fotocopy", "status"],
    data: [
        [0, "Tidak Ada"],
        [1, "Ada"]
    ]
});
jun.comboStatusFotocopy = Ext.extend(Ext.form.ComboBox, {
    displayField: "status",
    valueField: "fotocopy",
    typeAhead: !0,
    mode: "local",
    forceSelection: true,
    triggerAction: "all",
    name: "fotocopy",
    hiddenName: "fotocopy",
    emptyText: "Pilih Status",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.StatusFotocopy;
        jun.comboStatusFotocopy.superclass.initComponent.call(this);
    }
});
//RENDER BANK
jun.getBank = function (a) {
    var b = jun.rztBank;
    var c = b.findExact("bank_id", a);
    return b.getAt(c);
};
jun.renderBank = function (a, b, c) {
    if (a == null || a == "" || a == undefined)
        return "";
    var jb = jun.getBank(a);
    if (jb == null || jb == "" || jb == undefined)
        return "";
    return jb.data.nama_bank;
};
//JENIS SINKRON
jun.TipeSinkron = new Ext.data.ArrayStore({
    fields: ["model_", "tipe_sinkron"],
    data: [
        ["bu", "Bisnis Unit"],
        ["area", "Area"],
        ["cabang", "Cabang"],
        ["leveling", "Leveling"],
        ["golongan", "Golongan"],
        ["jabatan", "Jabatan"],
        ["statusPegawai", "Status Pegawai"],
        ["status", "Status"],
        ["pegawai", "Pegawai Lengkap"],
        ["pegawaiNon", "Pegawai Non Attr"],
        ["resign", "Resign"],
        ["validasi", "Validasi"],
    ]
});
jun.comboTipeSinkron = Ext.extend(Ext.form.ComboBox, {
    displayField: "tipe_sinkron",
    valueField: "model_",
    typeAhead: !0,
    mode: "local",
    forceSelection: true,
    triggerAction: "all",
    name: "tipe_sinkron",
    hiddenName: "tipe_sinkron",
    emptyText: "Pilih Sinkron",
    selectOnFocus: !0,
    initComponent: function () {
        this.store = jun.TipeSinkron;
        jun.comboTipeSinkron.superclass.initComponent.call(this);
    }
});