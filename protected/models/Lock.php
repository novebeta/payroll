<?php
Yii::import('application.models._base.BaseLock');
class Lock extends BaseLock
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->lock_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->lock_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function isPeriodeLocked($periode_id)
    {
        $periode = Lock::model()->findByAttributes(['periode_id' => $periode_id]);
        return $periode != null;
    }
}