jun.Validasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Validasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ValidasiStoreId',
            url: 'Validasi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'validasi_id'},
                {name: 'pegawai_id'},
                {name: 'pin_id'},
                {name: 'PIN'},
                {name: 'in_time'},
                {name: 'out_time'},
                {name: 'min_early_time'},
                {name: 'min_late_time'},
                {name: 'min_least_time'},
                {name: 'min_over_time_awal'},
                {name: 'min_over_time'},
                {name: 'tdate'},
                {name: 'kode_ket'},
                {name: 'no_surat_tugas'},
                {name: 'status_int'},
                {name: 'result_id'},
                {name: 'user_id'},
                {name: 'status_pegawai_id'},
                {name: 'shift_id'},
                {name: 'approval_lembur'},
            ]
        }, cfg));
    }
});
jun.rztValidasi = new jun.Validasistore();
//jun.rztValidasi.load();
