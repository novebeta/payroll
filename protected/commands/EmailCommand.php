<?php
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
/**
 * Created by PhpStorm.
 * User: nove
 * Date: 9/3/18
 * Time: 1:06 PM
 */
class EmailCommand extends CConsoleCommand {
	public function actionSendAll( $periode_id, $email_id, $email_session_id ) {
		Yii::import( 'application.extensions.vendor.phpmailer.Exception' );
		Yii::import( 'application.extensions.vendor.phpmailer.PHPMailer' );
		Yii::import( 'application.extensions.vendor.phpmailer.SMTP' );
//		echo Yii::app()->runtimePath;
//		exit(0);
		/** @var Periode $periode */
		$periode = Periode::model()->findByPk( $periode_id );
		/** @var Email $email */
		$email = Email::model()->findByPk( $email_id );
//		$email->decrypt();
		/** @var EmailTrans[] $emailTrans */
//		$emailTrans = EmailTrans::model()->findAll();
		$emailSession = EmailSession::model()->findByPk( $email_session_id );
		if ( $emailSession == null ) {
			echo "> Email session tidak ditemukan.\n";
			exit( 1 );
		}
		$detailsArr = CJSON::decode( $emailSession->param, true );
		$emailTrans = array_column( $detailsArr, 'email_trans_id' );
		try {
			foreach ( $emailTrans as $row ) {
				$email_tran = EmailTrans::model()->findByPk( $row );
				/** @var PHPMailer $mail */
				$mail = new PHPMailer();
				$mail->isSendmail();
				$mail->Host       = $email->host_;
				$mail->Username   = $email->username_;
				$mail->Password   = $email->passw_;
				$mail->Mailer     = 'smtp';
				$mail->Port       = $email->port_;
				$mail->SMTPAuth   = $email->smtp_auth == 1;
				$mail->SMTPSecure = $email->smtp_secure;
				$mail->SetFrom( $mail->Username, 'PAYROLL' );
				$mail->Subject = 'SLIP ' . strtoupper( $periode->kode_periode );
				$mail->IsHTML( true );
				$mail->ClearAddresses();
				$mail->ClearAttachments();
				$mail->AddAddress( $email_tran->email );
//				$mail->AddAddress( 'novebeta@gmail.com' );
				$model                 = new EmailTransHistory;
				$model->email          = $email->username_;
				$model->email_name     = $email->name_;
				$model->email_trans_id = $email_tran->email_trans_id;
				$model->periode_id     = $periode_id;
				$hasil                 = '';
				$attArr                = [];
				$nameArr               = [];
				if ( $email_tran->grup_ == 1 ) {
					echo "> Proses grup " . $email_tran->nama_ . "\n";
					foreach ( $email_tran->emailTransDetails as $email_trans_detail ) {
						echo "\t >> atas nama : " . $email_trans_detail->nama_ . "\n";
						$payroll = Payroll::model()->findByAttributes( [
							'pegawai_id' => $email_trans_detail->pegawai_id,
							'periode_id' => $periode->periode_id
						] );
						if ( $payroll != null ) {
							$attArr[ "SLIP_GAJI_" . strtoupper( $payroll->nama_lengkap ) .
							         "_" . strtoupper( $periode->kode_periode ) . ".PDF" ] =
								$payroll->slipGajiString( $this );
							$nameArr[]                                                     = $payroll->nama_lengkap;
							echo "\t \t >>> Add to email attachment.\n";
						} else {
							echo "\t \t >>> Payroll not found. Skip.\n";
							continue;
						}
					}
				} else {
					echo "> Proses " . $email_tran->nama_ . "\n";
					$payroll = Payroll::model()->findByAttributes( [
						'pegawai_id' => $email_tran->pegawai_id,
						'periode_id' => $periode->periode_id
					] );
					if ( $payroll != null ) {
//					$email_html                                                    = $payroll->slipGajiString( $this );
						$attArr[ "SLIP_GAJI_" . strtoupper( $payroll->nama_lengkap ) .
						         "_" . strtoupper( $periode->kode_periode ) . ".PDF" ] =
							$payroll->slipGajiString( $this );
						$nameArr[]                                                     = strtoupper( $payroll->nama_lengkap );
						echo "\t \t >>> Add to email attachment.\n";
					} else {
						echo "\t >> Payroll not found. Skip.\n";
						continue;
					}
				}
				$mail->msgHTML( $this->generateBodyMessage( $nameArr, $email ) );
				foreach ( $attArr as $key => $att ) {
					$mail->addStringAttachment( $att, $key );
				}
				echo "\t >> sending..... \n";
				$status = 0;
				$msg    = '';
				if ( ! $mail->Send() ) {
					$msg    = $mail->ErrorInfo;
					$status = 1;
				} else {
					$msg    = "OK";
					$status = 0;
				}
				echo "\t >> Message : $msg \n";
				$model->status = $status;
				$model->note_  = $msg;
				$model->save();
				sleep( mt_rand( 40, 50 ) );
			}
		} catch ( Exception $e ) {
			echo "\t >> FATAL ERROR : " . $e->getMessage();
		} catch ( CException $e ) {
			echo "\t >> FATAL ERROR : " . $e->getMessage();
		}
	}
	public function actionSendAllGoogle( $periode_id, $email_id, $email_session_id ) {
//		echo Yii::app()->runtimePath;
//		exit(0);
		/** @var Periode $periode */
		$periode = Periode::model()->findByPk( $periode_id );
		/** @var Email $email */
		$email = Email::model()->findByPk( $email_id );
		/** @var EmailSession $emailSession */
		$emailSession = EmailSession::model()->findByPk( $email_session_id );
		if ( $emailSession == null ) {
			echo "> Email session tidak ditemukan.\n";
			exit( 1 );
		}
		$detailsArr = CJSON::decode( $emailSession->param, true );
		$emailTrans = array_column( $detailsArr, 'email_trans_id' );
		foreach ( $emailTrans as $row ) {
			/** @var EmailTrans $email_tran */
			$email_tran            = EmailTrans::model()->findByPk( $row );
			$model                 = new EmailTransHistory;
			$model->email          = $email->username_;
			$model->email_name     = $email->name_;
			$model->email_trans_id = $email_tran->email_trans_id;
			$model->periode_id     = $periode_id;
			$hasil                 = '';
			$attArr                = [];
			$nameArr               = [];
			if ( $email_tran->grup_ == 1 ) {
				echo "> Proses grup " . $email_tran->nama_ . "\n";
				foreach ( $email_tran->emailTransDetails as $email_trans_detail ) {
					echo "\t >> atas nama : " . $email_trans_detail->nama_ . "\n";
					$payroll = Payroll::model()->findByAttributes( [
						'pegawai_id' => $email_trans_detail->pegawai_id,
						'periode_id' => $periode->periode_id
					] );
					if ( $payroll != null ) {
						$attArr[ "SLIP_GAJI_" . strtoupper( $payroll->nama_lengkap ) .
						         "_" . strtoupper( $periode->kode_periode ) . ".PDF" ] =
							base64_encode( $payroll->slipGajiString( $this ) );
						$nameArr[]                                                     = $payroll->nama_lengkap;
						echo "\t \t >>> Add to email attachment.\n";
					} else {
						echo "\t \t >>> Payroll not found. Skip.\n";
						continue;
					}
				}
			} else {
				echo "> Proses " . $email_tran->nama_ . "\n";
				$payroll = Payroll::model()->findByAttributes( [
					'pegawai_id' => $email_tran->pegawai_id,
					'periode_id' => $periode->periode_id
				] );
				if ( $payroll != null ) {
//					$email_html                                                    = $payroll->slipGajiString( $this );
					$attArr[ "SLIP_GAJI_" . strtoupper( $payroll->nama_lengkap ) .
					         "_" . strtoupper( $periode->kode_periode ) . ".PDF" ] =
						base64_encode( $payroll->slipGajiString( $this ) );
					$nameArr[]                                                     = strtoupper( $payroll->nama_lengkap );
					echo "\t \t >>> Add to email attachment.\n";
				} else {
					echo "\t >> Payroll not found. Skip.\n";
					continue;
				}
			}
			$hasil = false;
			while ( $hasil === false ) {
				$hasil = self::sentGoogle( $email->credentials_, $email->token_, 'PAYROLL', $email->username_, $email_tran->nama_,
					$email_tran->email,
//					'novebeta@gmail.com',
					'SLIP ' . strtoupper( $periode->kode_periode ),
					$this->generateBodyMessage( $nameArr, $email ),
					$attArr );
				if ( $hasil === false ) {
					sleep( 60 );
					$email = Email::model()->findByPk( $email_id );
				}
			}
			echo "\t >> sending.....\n";
			$status = 0;
			$msg    = '';
			if ( $hasil !== true ) {
				$msg    = $hasil;
				$status = 1;
			} else {
				$msg    = "OK";
				$status = 0;
			}
			echo "\t >> Message : $msg \n";
			$model->status = $status;
			$model->note_  = $msg;
			$model->save();
			$emailSession->delete();
			sleep( mt_rand( 40, 50 ) );
		}
//		} catch ( phpmailerException $e ) {
//			echo "\t >> FATAL ERROR : " . $e->getMessage();
//		}
	}
	private function generateBodyMessage( $nameArr, $email ) {
		return "Dear " . implode( ",<br>", $nameArr ) . "<br><br>Terlampir adalah slip gaji anda.<br>" .
		       "Silakan gunakan password Anda untuk membukanya sesuai dengan petunjuk dibawah ini.<br><br>Password slip gaji ini adalah ddmmyyxxxxxx , dimana :" .
		       "<table style=\"font-family:Verdana;font-size:11px\">
<tbody><tr>
<td>
<table style=\"font-family:Verdana;font-size:11px\">
<tbody><tr style=\"background-color:#d4d4d4\">
<td>*
</td>
<td>
dd
</td> 
<td> 
Dua digit tanggal lahir Anda, contoh: 01
</td> 
</tr> 
<tr style=\"background-color:#d4d4d4\">
<td>*
</td>
<td>
mm
</td>
<td>
Dua digit bulan kelahiran anda, contoh : 01
</td>
</tr>
<tr style=\"background-color:#d4d4d4\">
<td>*
</td>
<td> 
yy 
</td> 
<td> 
Dua digit terakhir dari tahun lahir Anda, contoh: tahun kelahiran 1986, ketik 86
</td> 
</tr> 
<tr style=\"background-color:#d4d4d4\"> 
<td>*
</td> 
<td> 
xxxxxx 
</td> 
<td> 
Enam digit dari NIK Anda, contoh: NIK 123456, ketik 123456.
</td> 
</tr> 
<tr> 
<td></td> 
</tr> 
</tbody></table> 
Password slip gaji sesuai contoh di atas adalah: <b>010186123456</b>
</td>
</tr>
</tbody></table>" .
		       " <br><br>Jika Anda memerlukan informasi lebih lanjut, silakan menghubungi layanan email " .
		       "<a href=\"mailto:" . $email->username_ . "\" target=\"_blank\">$email->username_</a>" .
		       "<br><br>Hormat Kami,<br><br><br>Payroll<br><br>
<p>Perhatian: E-mail ini (termasuk seluruh lampirannya, bila ada) hanya 
ditujukan kepada penerima yang tercantum di atas. Jika Anda bukan 
penerima yang dituju, maka Anda tidak diperkenankan untuk memanfaatkan, 
menyebarkan, mendistribusikan, atau menggandakan e-mail ini beserta 
seluruh lampirannya. Mohon kerjasamanya untuk segera memberitahukan 
Pihak Payroll di alamat email yang tercantum di atas serta 
menghapus e-mail ini beserta seluruh lampirannya. </p>";
	}
	private function sentGoogle(
		$credentials, $token,
		$fromName, $fromMail, $toName, $toMail, $strSubject,
		$strBodyHtml, $attachments = []
	) {
		$client = self::getClient( CJSON::decode( $credentials, true ), CJSON::decode( $token, true ) );
		if ( $client == null ) {
			return false;
		}
		$service = new Google_Service_Gmail( $client );
		try {
			$nl = "\r\n";
//			$fromName      = "PAYROLL";
//			$fromMail      = "novebeta@gmail.com";
//			$toName        = "novebeta";
//			$toMail       = "novebeta@gmail.com";
			$strFrom = "From: $fromName<$fromMail>" . $nl;
			$strTo   = "To: $toName<$toMail>" . $nl;
//			$strSubject    = "Verificaion mail";
//			$strBodyHtml   = "Tanya jawab gaji ke ::: novebeta@gmail.com" .
//			                 "<br><br>Sekian dan Terima Kassih.";
//			$strFileName = "SpecialOffer.pdf";
//			$strEncodeFile = "JVBERi0xLjQKJeLjz9MKMyAwIG9iago8PC9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL01lZGlhQm94IFswIDAgNzkyLjAwMCA2NDguMDAwXQovVHJpbUJveCBbMC4wMDAgMC4wMDAgNzkyLjAwMCA2NDguMDAwXQovUmVzb3VyY2VzIDIgMCBSCi9Hcm91cCA8PCAvVHlwZSAvR3JvdXAgL1MgL1RyYW5zcGFyZW5jeSAvQ1MgL0RldmljZVJHQiA+PiAKL0NvbnRlbnRzIDQgMCBSPj4KZW5kb2JqCjQgMCBvYmoKPDwvRmlsdGVyIC9GbGF0ZURlY29kZSAvTGVuZ3RoIDE4MzU+PgpzdHJlYW0KeJy9Wl1zEzcU1XN+hR7bGRArrT55akjSkARCmpgyTKcPCaSBQBoIoQz/vkfrlXa1tonju2Y8ttfr63N17zl7pSsvV3x/oxLGOv5t48mEP/pdcilFVVV88g/fmeC7eLy7MT13sfFo90Tyiy/5N7C72fjrb17xt/m79p23eF74YFu4z1waJVRt8IOK20pYI7muha4CN1oKWRn+5oo/2lN8+5r/kSEbKDWAmo6t/3pzwXkcUXRtqko4JbnxXgSn+eQt57+wE/aM7bEjxtku22T7OObsAEfH7DVeX+F5+CufXMLBHPjjXQ58UQfJv8HPJZ77vHCkpOdX3AQrgtb5zEd+En/XZIwr/E7F37Up5cMwhxlbMkzrhPO2DfOI7SCkPfaCbbOdFFDn505QVwntqgHoY+TqkF2z/9g5u2JneL3BGcUqJpmfdZKDqby7RzBWw692QmvT+t0CK08iM2z3B166lK3iGAHXPgwcx4C3IY8okhMM4fVS7u/yleReVyLIqnXF70+SDEZ4belAtYZMvaYDZS2SgZL+qEDWeOFwBd4JtOA91TCJq9erUl9BWFXdg3P83kgUBufbYRxCWAcLJUV16fyUi8JlVLTEBRsfnsm1OVdGT7VZeD9gE3a0Np+1MkLZoc8Y8cO1+TQV5izjBz7324I16U8n4/t2Ulhp5sR7gNL8He+b7A0e1+wr+5fd4vNzdoqjU3YRS/faxpW0XtXC5EvuEIN5zjbXrvbC6eMm5ji5T3CpHTaT/stmKK/WykxW/yAFR/D7E/Q/k4OfoP/C5y4yvjl/5TG29mdi3WN/4nVr3erWAV9p1TreaWS2x56tW9+l26G+Ralv1HiH5dlvONsODxZbWBM+X7vyy3GeNJVwgrGdrF39Mxlau/ZLj08bPqZtRVyD76+x6KWrYFYVipnlvC4hd4O5RbvIq1t9IQb9ejkCThRYQKhkoKiX2o4AFGWAUY0ABDadlHcDTRfZ3zbmNagbWgnvsXJWaGQqiWbUOTSsIZ9AL5pstLdCOdvZpBPRBoRJ1cdRBlJQdQHUGnVAyaiPFClT6Kk6qFrVILGESlYdVrLqY9VKC2v7WDoE4WtTYCWrDitZ9bFit2KM7WHFtkMFVWAlqw4rWRVY4K6SdT/tGt/6clzJqpf41qqPlbZchlstd7Qq63rn7d7LSmXDBFFD0t2mxCGWBZs4utfi+O5rWQtn3MDdosp/z1DqAGGiXx/G8gIRvIh7EuPFEbUWrFxPHFZqIUO9JPiK04TWQoe0XxSX26fskr3H0RHakA/xucI+h0dWXD0DHue5Y/YJr449aHva9n2V6aCVUOlkBaAkl8FonyAF+1gJcchmTtO/tDQWJkE1jf3KCcjyoCYgSUFhJeNVHujLpve8aLvQD/ETQQoD8C4LZiQZlA4IMhiM9AiPp5ErggQWBq/iZiyBemrQiXrgeevmUD9hN83xFwz3Gse3BAkMnPSzYEaRQOmAIIHBSLdQEN/g+bFJxbQyHBPksDARFClQg09SwCArU+fBTZq9qEv0oRzV8JSd4XlLrAUDF138esRpoXRCEMNgtN20ENvFOe3x0jJYmAY3yrRATUAriNqjTa/9XEFsQwjnTYV4t7ochg66PMjx5DBwsrocyECJfjJQIpoMlIiOf+EonXl4BmrPUPXetYvBWzynkwJhSsykD5yRamAmuQQlkEwFyiRTgTLJVKBEsjHCIsKumsWOaO4239JUDiDHobIEJVBJBcpUUoEylVSgRKWuhAsm5z3uJ580Dfuk2VNf4S6CTOkAehxKS1ACpVSgTCkVKFNKBUqUKiOCp9xhkAikAmXSyECJNCpQJo0KlEmjAiXSpBTSEvrvTBoVKJNGBkqkUYEyaVSgTBoVqCVNBStq84M9tWVJIwMl0uhALWlkoEQaGSiRRgZKpHkpTJ163EmzJj1FVx5XpOdYm77Fp0+pPyWRWjiKs1/Xkig0I26cDYvSDYXyHySmW7iv1LRnMSxMiWK+SQZxL4ucjCQSa/HVGFc2FSjTTAZKNFOBMplUoEzaXUCEv6KUQanPt7JOmj3oc+jtKeR81Rwd4dz3e/xXf89xZCEUA5mtBppZFpqq4OddAJT/x0rXFOlQgYxF1yKHQK/BxQX73v4/cNOUnQexTUBKHjbpebj4duNldVBj9nBpk3urqe9oSQi5KABXUX+FlaNRA6Bt9p6dsa/N+B4QRqdQBw3p9ob2IqcC5YucCpRDq6xwmrAlmUOjAuXQqEApNBmkCIqym1c5LGz1AKipcLiurjGZf1ztvhLtYKWr9vaMuvb5TO/GEuWc0HU0ki6AbJPPRKNYCOO9Dp1VgupbxZpVmdB3GBTa4qpwmKx6WK1VgRXiXYKyhxXvBzHNbUQ9rNaqw0pWfawoGmV1D8tKeHS6wEpWHVay6mNZWQuNRr/DyveD9LCSVYeVrPpYU6Jm7xr5H96nlx0KZW5kc3RyZWFtCmVuZG9iagoxIDAgb2JqCjw8L1R5cGUgL1BhZ2VzCi9LaWRzIFszIDAgUiBdCi9Db3VudCAxCi9NZWRpYUJveCBbMCAwIDc5Mi4wMDAgNjQ4LjAwMF0KPj4KZW5kb2JqCjUgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9jYSAxCi9DQSAxCj4+CmVuZG9iago2IDAgb2JqCjw8L1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUwCi9CYXNlRm9udCAvTVBERkFBK0RlamFWdVNhbnNDb25kZW5zZWQKL0VuY29kaW5nIC9JZGVudGl0eS1ICi9EZXNjZW5kYW50Rm9udHMgWzcgMCBSXQovVG9Vbmljb2RlIDggMCBSCj4+CmVuZG9iago3IDAgb2JqCjw8L1R5cGUgL0ZvbnQKL1N1YnR5cGUgL0NJREZvbnRUeXBlMgovQmFzZUZvbnQgL01QREZBQStEZWphVnVTYW5zQ29uZGVuc2VkCi9DSURTeXN0ZW1JbmZvIDkgMCBSCi9Gb250RGVzY3JpcHRvciAxMCAwIFIKL0RXIDU0MAovVyBbIDMyIFsgMjg2IDM2MCA0MTQgNzU0IDU3MiA4NTUgNzAyIDI0NyAzNTEgMzUxIDQ1MCA3NTQgMjg2IDMyNSAyODYgMzAzIF0KIDQ4IDU3IDU3MiA1OCA1OSAzMDMgNjAgNjIgNzU0IDYzIFsgNDc4IDkwMCA2MTUgNjE3IDYyOCA2OTMgNTY4IDUxOCA2OTcgNjc3IDI2NSAyNjUgNTkwIDUwMSA3NzYgNjczIDcwOCA1NDIgNzA4IDYyNSA1NzEgNTQ5IDY1OSA2MTUgODkwIDYxNiA1NDkgNjE2IDM1MSAzMDMgMzUxIDc1NCA0NTAgNDUwIDU1MSA1NzEgNDk1IDU3MSA1NTQgMzE2IDU3MSA1NzAgMjUwIDI1MCA1MjEgMjUwIDg3NiA1NzAgNTUwIDU3MSA1NzEgMzcwIDQ2OSAzNTMgNTcwIDUzMiA3MzYgNTMyIDUzMiA0NzIgNTcyIDMwMyA1NzIgNzU0IF0KIDE2MCAxNjAgMjg2IF0KL0NJRFRvR0lETWFwIDExIDAgUgo+PgplbmRvYmoKOCAwIG9iago8PC9MZW5ndGggMzQ2Pj4Kc3RyZWFtCi9DSURJbml0IC9Qcm9jU2V0IGZpbmRyZXNvdXJjZSBiZWdpbgoxMiBkaWN0IGJlZ2luCmJlZ2luY21hcAovQ0lEU3lzdGVtSW5mbwo8PC9SZWdpc3RyeSAoQWRvYmUpCi9PcmRlcmluZyAoVUNTKQovU3VwcGxlbWVudCAwCj4+IGRlZgovQ01hcE5hbWUgL0Fkb2JlLUlkZW50aXR5LVVDUyBkZWYKL0NNYXBUeXBlIDIgZGVmCjEgYmVnaW5jb2Rlc3BhY2VyYW5nZQo8MDAwMD4gPEZGRkY+CmVuZGNvZGVzcGFjZXJhbmdlCjEgYmVnaW5iZnJhbmdlCjwwMDAwPiA8RkZGRj4gPDAwMDA+CmVuZGJmcmFuZ2UKZW5kY21hcApDTWFwTmFtZSBjdXJyZW50ZGljdCAvQ01hcCBkZWZpbmVyZXNvdXJjZSBwb3AKZW5kCmVuZAoKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8L1JlZ2lzdHJ5IChBZG9iZSkKL09yZGVyaW5nIChVQ1MpCi9TdXBwbGVtZW50IDAKPj4KZW5kb2JqCjEwIDAgb2JqCjw8L1R5cGUgL0ZvbnREZXNjcmlwdG9yCi9Gb250TmFtZSAvTVBERkFBK0RlamFWdVNhbnNDb25kZW5zZWQKIC9DYXBIZWlnaHQgNzI5CiAvWEhlaWdodCA1NDcKIC9Gb250QkJveCBbLTkxOCAtNDYzIDE2MTQgMTIzMl0KIC9GbGFncyA0CiAvQXNjZW50IDkyOAogL0Rlc2NlbnQgLTIzNgogL0xlYWRpbmcgMAogL0l0YWxpY0FuZ2xlIDAKIC9TdGVtViA4NwogL01pc3NpbmdXaWR0aCA1NDAKIC9TdHlsZSA8PCAvUGFub3NlIDwgMCAwIDIgYiA2IDYgMyA4IDQgMiAyIDQ+ID4+Ci9Gb250RmlsZTIgMTIgMCBSCj4+CmVuZG9iagoxMSAwIG9iago8PC9MZW5ndGggMzEyCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nO3P51YIAABA4XsOSlZEsiKUEdmibMnIJquBvP9D8A71w4/ve4B7zq1N2tb2djTQYDsbale729Pe9jXc/g400sEONdrhxjrS0Y51vBONd7JTTXS6M51tsqnOdb4LXWy6S11upitd7VrXu9HNbnW72e50t7nmu9f9HvSwRz3uSU9b6FmLPe9FL3vVUq9709ve9b4PfexTn1vuS1/71vd+tNJqa633s1/9bmOz8//82YIGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/Cf+AghWEvEKZW5kc3RyZWFtCmVuZG9iagoxMiAwIG9iago8PC9MZW5ndGggMTE1MjAKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0xlbmd0aDEgMjU5NTYKPj4Kc3RyZWFtCnic7XwJXFTV/vg599w74AWU3azUi4hLIqSIuyXCICibgFu5MMwMiwJDMwOG5pJr5oKlYqK5ZWhoZmZoabZYaZltPsuemaWZmT319dJnyBz+33PuHWZQNFte7/0+nz/jnXvuOd99O997VBBGCDVHMxBBWakZkd3Hty97EGZ+hCvLWGgolpfJXyKEk+DSGUvtCspv3QchIROeaU5xbuFDPUonICTCM9qSa7AVI0/kgZAEj8g7t6AsZ/aj7TLg2RuhO5/KMxtM3u0MFKG2G2G9Zx5M+GDdN/AMPFD7vEL7w+sXR05FSIFHvLDAYjQMLknJRihkIUwcLDQ8XCx+LIJ87Rg/pchQaD55+kIbeLYjNMBcbLHZ6x9FDyA0ntFTiq3m4n4e/4Th+DqQIQ8R6W68BIkwjpJWAoc26p18jnIEf9DKS6cjnqIgiGdRUf3HyFEvt8/qLCLFOy1Hb2IU6x26QBqIKz0K8ekshOtP1oPuWdJRpIoMpJD6c7c2vhu+Mb+LiOmggztGApqG1tXXcyy415+uf4tDqRREMJ8OrOiJmiEZeSFv5AMeaoF8kR/yRwEoEAWhYNQS3YFaoTvRXUC7NWqD2gKtENQOhaL2KAx1QB1RJ9QZ3YO6oHDUFUWgSHQv6oa6oyjUA0WjnqgX6o36oL6oH+qPBqD70P1oIIpBg1AsikN6FI8GowSUiIagoSgJJaMUlIrS0DCUjjJQJhqORqCRaBQaDbZ+EI1BY9E4NB5lIQPKZirgaLQbHYLPm6garcZV8JQD0w/BzDphB5qDSmBmPz6E5wtdYa4KXUJHAHIeOkSqwTxDQMZDAP+FJKCfcSbaCTT64EDcx0MnIjFF3Cmmi7vFs+Jh1Eu0iYfFLNGGo8gGaYRUBVcf8g748j2wx258EtnQq+QciSJ7xTixOTpJDpNqdAa4QAwAj3K0EU0BWQKxBU0XpgjpMHNAOowq4WOB9cN4DT4C0r2KZ6Gj6CkiCgloDT4Keh1CV9AskilMB4dFCTkg/wGgdRjwK5ENHHgUy4gKXWAOpAde2fy7NekqHeWfS2g6cM5EG3W7dYEeocCFWawK78c/6paidegIGUMeIsfxHDFU3CwmoHLVAiQLlQPtSoajy8FloDv7TGHUhUliFq5G58Qsj2yg/Q7TCHjuFNJBoxy0F65JOl/QqR+eQ+aDpGy1NTrsMUSMBHyg4DEVtEbIQqLRBBhNQdvQDtSVVKByoMT11fWSrgDmavEb0LkcLxKuoMMkDuIsR7wAtobQRBUI7fLQSSIRMApXfLcLYYmm7QOHjVIOjg7pGn7do+LroWxHadt9ypTd9fVpo8S7pNHbpbu3kzDP7WJY6Dc3W/yma/jQtFHKdoc+TqOqz4qDuYxRMGRPMA3z+ji+xphul8LgT2LWdsWYpzzu+3ho38d9zX27Ip6NnkiAbCQwGkMryGVpI4yhigX4hfiFhfiFjCEr6z4UPnD0oBUeza/+ZNV1BisJOBiQD0PuE8hTFBYVFOoX5UdCCQ7etWuX/5MBlEpHHQ/RVdgMdF8k1cJaDgt0sV8oUA71w0NW4n88BVBHhS7sArjFUJsekfYBXFuACyEhhIQERJGQoBB+hQbwKzqEX2QbrR2KPQaMxbrxS8biVvTtIbgt3Td2yRj6y9gnxtFv8YAk+hXWjyFz6A4yjxrwWmqopDtW0my8hl0rcUolXstqzkp6hNTpAqGCdIJKgXAo6dChY3RwcEu/Dh2ie/TsFR0VBA9BMOkXHBwUqPMgfjpdUCCs9+wZ3aODYHkAj39t/Ji9pgMvv737gYynhw59OuODNw+/8UB+ofmQ1W6hR3BXoWvXnQNjMD7YfmvFM3ubn/9ebHvXC/dEiHR46M7Vz+1vAZWy0mfCqBFZR2mqX9EDo/KgDprrT+uskC1eUO9CoaZFgWztGOOo7oxvx+5MmNB2TEb3+bAoHBrgtib1Txk9OiV59OjkxZufW7Rk0+a6C8mjR6Wkjn5AOLmkbteSu8ufe668vGqT8MSy2TOXL585a/n0L/fsOX58z97jgmH5zNnLls1+tGL6L//S+Rzf89rfj+999Uvwlb3+tGQG2ZpBZYXdUePeC+OePXtF6QSPgNCOOuCPmP24FaO6cxOGckHxG+lJry8ZXhFDN+D3ovtLm73GZVw7/ejYD0uP0R/LHu7S9eCzQ5cnpSy+b1xpNAkdtn7Uk2/dP1Aod1wdfcg6m9Jp9PTS0SNxwOczvjHeP7X/hnfat6+J7GYZFZXLCjCLNzyUxxuPNog0NczY2hr6k9Bf5w+7CkgtBAX6twztIET38O8l9C8rKX14yWNz5z6m8/+O3nf2LO135jx+9+uT+O0feWFHSwG3m4obEBXsHxQoeIT29I/uISxbMm/u3HlLJpWW6vx/pP1Pfk37nj+D3zl7Fr/F8HoKQ4gRbOUHD5LkEQYySR3DwnpJkDNhxEhfw7HR9NMc+kkPHEtf64Ejc3CEePKt/dmH6Dxcdih7/1vGQ7iMzjsEtPZDjsRLIsQE6AZZwbIpOsSPpOHL1GsFlfEVSXQcqXYcEbpWwwbD5V5c748rEGWNSa+oIMihY/uW2YfQrfQNPJCtP4BPCgOFWcxeAUDyAXyG3iXM2shx34GvWYBLOG7oO/v2UcrmUf0goVCzMcQb/ttKGg5m/qUQYqOs/jSUKzVuIWIFP1//qO7+fr5CR/4dymeEIQsXL164CH5OXbhwCi4pjR6mH8B1GAhG4R44ah210bl0HrXhRbgMT8aLgO/7kLDnga8M8oT4SdFhUcwKZ3BnuhcPqsGd62qrRVvC7oTao9Vc/nkAfwJkgZ4kDACjYe/nSQtx6BHdk0UlS2idgLOFyXVbcnFOxEh9RdnwfQVFr6d+cvW+9Jb/rK6unoSf6Fu4InFSxaDYD7p1//6tMc8Wt6bnOf2FoKsA9DvBAysOIuQcqx1qKoaGRmsDd3bCjzOX0ot1w1/KStqZveWlquWrn5lbvnT+0Orc3JfTPvppBglr+/aSEz+Fhe3v1r2ifPbyqknFtintO+xUlE92PLKF+VRgfZ4YAXYQeCRDsYzyY0WYRYPwCo3Gh+498/rrLzvWS2F1p8nhuqjNdB3O2u+Mh9PkZ8Btrfrbj8mFggJRY9FB4gPC+44vO6V2+gZ70J+uDH9h3LDns1bX1KxOfALSq5o+2aIFvfDDP+llRTnU7d6a1atr2ndgss0Am4Rw/7dvXLECeAEQCGPI0odHAgKjCFmTy8snTykvvxC3MG7nPp/odVmHzl/+4NwVHFEft5D0e3XD+j171m94VSjb3b4D/YleHDmWXjz/Hf2BR0Y2frYN02sF+Poq6KXjekFQhqwgWfSO1/D7jinS0RG1j0pdeI85FeRryeUL5fXevXY6axWLimBXDQ0IdJlFGDJz2bKZUCnpwZKZZw6+9+1M+6ylF7/66uKymJmlJbNnl5TOFN6pnDevctXceZUjlB0zXvr445dm7FDavVv+xffff1H+LjbYZ860w6XWUTEQZLmD2aoXt75/AKucrCjxrGFuwU7eIOj7em6iHusM75+/fOjcFXqkXr8QtwHjTQEjtgUDYR/sP2IMbnH+OxzMU2gtfbCNsMJpRB4/x0DvCjFU3ZN78f07KPTYvn0sv8VQymF43eAwzVjF4zC8euxj9QOgHF801JD9aJwUL1Yx22OMg3A0lvR148n6a9PEmeQSfYIurcGfVOFPGN39OEuKJxu0PgMikH32izMZ7LVpZMPmS2ruutEMiOY0ORBZSy7V0MgqGlmDCxm92eDUdLAhYTZkvUhAKNt2mkp0Dx0kuy4IZ+3a9eLWqGHDoiJKDYPXjxq+bUzVezHDUruEekg6SvETleaZI0ZHj+s2uig+dm+f3m+tTZo/YkRkdKug/j3U/Cujq3UvSRsghtjboVt0YJ0WMmzDi45SW4SOHdozOdQNh2dYr5Y6KBXtO6qbT8/2Ud1FWAjyRR7CthkWy/SZRYUz8A89F4xd9eZblWMXRj86a3mfPuPov9ZYPhixaF1e9rhfFtm/HjfsIfrv+VX0mM328OSH7Lhb9T482BI7mJ6qE1qVP7Nx8cJnN9KE5MRfDh6sHZo0y6EEn3xx4t60WQtiBubQl99cS3+YkFc4cpjFkDtr6lSc+FoNHjJ1+rxt67K/m0J/oR/rQM/mUGe+4nUGKi4mIazYQ2MWKpT+G1+k6z8S0GtTHBsmvyE1d7Qi22q74On0UZZjOwGvCPA8YddT+F6lGqhhEBbinmQheCIOXbpo0VJ6AvvMmTVrDh2AP/74G1vxvGWXT9E2wnuOE/MWLJwj5ND7LNaHiqveeHH+hkDl0FMH/w5xkgs5tBH83wr4qBWG7zg9ewWBLxQEKQS8xGfGfFzy3eXL35V8PGb8Z6X0Q2g9xuHuD38mZR8dP44eoMfoF/TAuPFHEhLwWpyL8/DawaA16CG11fQALaJUcmEh6h3EPo6DMaEO+gO14BI8DxdCrE+hC6TIa5PwHTgCh+OWVXQFnQFNSwXIyuzSEuh5abVXu14mwxxmYYZjhrC1bg2rrfHVjtPVKjx5FeCbucGHvkxExw4hyHG+hoEmVDt68XxhdqBgB6jrYbwrcFO/pbthxET6OT6RT5fQd+jT2Ij7zfqHyXx66pWrV68YVhzFT1Q5pmcMxytxIS7CKxPiPx+fBeb6hH5KPwxDmg41mg6w/UIXw1v6l2uEZTU1jgJYcqwWTLVdhAOOPho8nqj2CgAPcABR24XvRfXraA5fU2n56rQd8+WaVz7Q9y4zAiB0XT9XV7yl8f1a3fsBFiIRSjxn++APYLz3LzqmMNbzhUmOhLrTwkeObpr9fgEcSZUVLF0jLK7bC3RZg8DXpVmw7s3sC/so6xc5VdwCj8SjcIsX6bM19Nnt0tE6T3K1tovUtg6JqPYbpx22ab6JwswMDPGykLn9smMrYFxrK35T20X85lpbtWaAf6S4Jvsi1Uv8W/js3XPn3n33++/fxXfhdLqNnoHP8zhDSqK7YfQd3Y0T8J2wlrCRPkjXsNqON8IWCJsgUuNVbMXjNYDHa6NkC4UGAU/8dvqCBdPB7JWnzp499W2NFOn46Ml5c5+sOn38xCnHZiYnvarJ2bqxnAFqV++SVmiQts/F4wfatG2hygrSgex7bxS59iN64qIg4GexgQnMFaijizW5PwC5A1inBp1UywD2FhbKOxoue4MupHu/ot6vvL110NSCd2tw5dlTJY6D386eO3e2sDd4yTSah6dXZDvmS0c/O7boVSHVcWEeVBO172F9aTjo1dFZJ/hLXMsb27aOHZ1tAPl77u7hy9dPrCj84E16zZH1uc3yt9xV1ZMXFH3w8rUvx78tbXynV88ZpUZz21Zdvqj54ut7Iz/Wxz82reiRtnd0fWPLu992YDFeC7oVgG6w2xGtZZfEYhpaQ0Mhf2uPSmouoHUg37MA58djVd1TIKpYB4D81u16+oUXVu8Cc9XTOojGkktHjlwiC+rG0C/pZ/ge3F6l4azXsGeyWg1/hBaO+n24Gm95Dd4Ajl5rKZ7Tcg8NQEh3Wot9Dso7eRw6YDd4684PwGF37qYvsPj75lvA9BcvsAtywLf2En+vca8DWM1H+GPfj1Nw2n6ahg/vh+Zj1tv4a+01P6ruqjDFMYe0VnO/DvAn8FhFhOODZcS3sR1b36btWSpXCjl1Pzn6CAdUedMBfnVDLcSsnccheWLbuhdIzrVdZFjd29LRymuW6krxCeR817sb3uvVLoN1LyFr8LRjx+hMXWD5L3Xl173HBIDsK/mLDHuP0S3lNF7i73yB6slFCOznodFCNzrz8891gVe/KteJ5axPIZvhneyo2vtwE5Ax+NzL1E7tL+NzoMcRvICWsLcxXH+BtiZb6Vb+3gTyk611g+jW8nLOa6V4SUjU5fC1AKAzhD5Oj+ty6OO4lNePIujb94tToP6FuXet0b1AqIauI6pRzyM8/N4qu+nxqqqq3s9NWVVz+tT3FY+N2Jj04JZhx48JUTlTsm1f7Oyc5Hi0Osfw5obX3vCfviAiorpjxzrObxPEYwTo7sVz0tl/AkvMG3j+KtehIzMsebRi1uyKitmzKhyHe6y17D5zZrdlbY9Nm4TIQ2fPHoJLSDcZ6F56FT57DabNQBT0NYM+s0GfVu756Ny32yH11YDMil+UsGHHjg0Ji+L1yzO+pT9DY5v8pBi9tUuX04cPn+7Spbp9e3wfbo79cd9QLjejex5Y+HK5WV3m5lHrWLB7s0b0VVU91hbvOnNmV/FaikCJFStACVIjjPvlx80mA47DnvCJM9AgTRGNvugNcgeiu7gXg68X2t+Da+Mhejt0Xq9szHpp1Og9WT/Rs9jr9KF/VAnLJi/Y5C2Me2DfgR49tt0TjntjGQfAq/+Jt1fs3LZGjfV7gNFW0IFV8SBmmSC1csD2yLwsbF3zgB770YtVa9eurtYFrkzLM5bXRZJPylP2bOEy0hHkPMjoxd9M3XzXUvWaVuvcDQPdKtEvnztn2bI5c5dXnflx5KrExMVDntkQta741VOnXi1eF1UlDDj45ZcHD3z55Xl6ip5r3eal8Htee/1BYzbuiwkWcd9sI+tZNkE+/6zJDiKrzP3YCSGPSfJzVbFl7qpNm/qtn/D8S8JGxxhhzdo1+zY65ukCHWvMpotM/ucBNxNoqO8IrMOAvn7bs/AjZl1bpws8h4T6r+gIDuOFWqgV09k9qLBPPnN/+Dg9R1j42RurDSt1nc5x24J8YgfAa9xHbNqEL/zNcU7oeoyiTbrAunx82vGzY6sQ6jgBOC55uDQgiS7wF+1cCOjpcsHW7eBBfdNw19qlfpCq/r77t5as2bIpZ8KMiqrcidOXb9rUZ01hUSWZ/0jp5VPMGOtXM2MIazasev0Zxzwxa1tu9iPIaVfgc4Ndg37FrkCCm1WLXcLjouX1p4luZzM95qyqnDunsnLOsX//+9gXly+Tk2ffe+/s9wcPnFtND9If6T/oAdwHYjYQ92Z1go4QI4Amz7ewBoG0AtGocKRs2tRQH3C9s2hsdmzTydVuFQKfb0i3RrF8dxPvWq7oZf2XM4WFHg153aOqqqEOOba5JbWp+pcrml2FK0CfdfgdrjNqS+HK3YM6LqoAuTdP8runFdnp73don2MHmDTHKEnc/0aoN8z/TfQVupv1FffGrxg25ZHRc3q/uOyrt4ZtN4zZMbJk2oOVfSrnv//KmA3i/ds6dcrMHJgY0vyelfNX14SG7ouOHj1saFpYi/bLZ67Z2obz7QZyb5LWqPHAygOYBiIf9gFWJvxwOh5Ot8Tmbtny+pNlZdIa+la5Y938lMq1nwpZ5fg+tSddDbKf4zEVyM783doObU/BTz9bbJm3qqqq/9oJz+/E6/CrQpXDsHbtvo3ClGvrtuYYL5HNWi/gLWbxd3W1kej1Au6H+22nc78Rs+oyydZr65jMydBvrwA43nME8D/wQklCk6s++nD/Rx9W0av7v/j7fsCoIBPYdW0dqaiboOZZNPDwAFwvfsbAN1qWvORnuhRP30+P07/vx4/RlW9jL+wlZjlOOt7Eu2mCMEQIpg/hcpVGT4hVJidUDEmn1YtevXjvIwyLn1M82xCXFNGG5qnC5x2cXJkwN1NMqVtGCjh+KuRgGeA36kFyiL/jSaGo7nPB4tgsZm2uO750Mwnj8N/RHeQFqBvQnkncNa5TQb5bq7VdfQ9mOUK29f76KcOMXr0ezXrq694DpienG03DkqfvW7LsxMUV9nLb8ksnlpaPWnT16cWt7lq8+uqiUcDjB9oaz9K1bjjvnrVS1/oq66oa82+pMWXn8U4pVKYNUoSGfnf/tORhJmA57f4+X68wTO/de7phxdd99o0ov7p68V2tFj99dfHI8qUnLi23ldtXXDyxjJ/T4CNSPOnG3xb4LtixA/s0dCEtg9kHuEv6cetHGWfJks5nYWZa5ejx60ca53hKHj4Lhic/RbrtSI3rJwpEui85Y0eqvj8fJvG/GxbGdMLD/nlhfIv+l1FrT/5XxUee3xLG7y+1WHh1U12S9w/N2N97eyLnD+B5FFKQyWfa1U21p7x/aPhbZuePUUxHY3gawKugkAjXKRxMUtGLOk+0WFyGVuo+R2bpaWTHtehF4SO0Bq6lZCXqCev7AX6xsBI9APd3hEKIzGWoDK734ZoH10K4HoCL0ZkB1wq4psJlB9hjcC1mNJwX+RTN9ogC/B9Rc/ES2il5oFxpAdopLoSrPTwvg+cJaKfQll3168XdMA/vTLqesJYNVzXKFUeqdyhJO8UngZa5/pqkR+sYTY82aIB4AfWEOQfc07ku7OjsI/QS57+y/gLotVLMR0WAu4lcRma4m8UiZBYeQ/fw8RK0SUDoeQHVnxS7qGMPgjaxeTGXw29icMJlwH8DGYVPUTdYWy0OQj2l0ygZ7tFsTA6gVLDDd8D/B3bXbMl+grVPOBqIHkEHcTA2YStegrfiT/GPuE6QhWChvZAuTBAqhc+En0l7kkXs5FVyRfQU24g9xARxjFghyVIHaYz0uLRDel/6WheoC9f11aXpJugm657Sbded8hA97vMo9tjq8YbHKY+rnnd49vZM88zznOz5uOdqzx2eb3h+3MyzWe9mpmbLmr3b7Du5vRwvj5Lt8mJ5tfyx/JPX3V5JXo94rfY65lXr3ca7u3eat9X7Se9q7y+9f/C52yfJR41XI8lEXVAeVDkBdsWVLCrFIB6d7O/e74Ti64zFp9RI5d9B8KSOBYB7XhsTmH9RG4swflcbS0D9uDbWQf0+q43ZSVWtNoY3eOyljX38n8adtHFz1CPgHW3si7wCftHGfqhZoMj+1YYI9Q3fG+ijjaEfDRqgjQXkGZSrjQnMW7SxCOOV2lhCdwTt18Y61CHouDb2RO2CsTb2Qn2DQ7WxT1jf4LHauDnK67dCG/ui4H712tgP+fcPirUUl1nzc/PsSidjZ6X7vfdGKdllyqB8u81uNRsKw5XEImOEElNQoKQzKJuSbraZraVmU4R8A2pPhpppKC2cYCnKVQYZ8m6CGGeeYBhRohjzDEW5ZptisJqV/CKluCS7IN+omCyFhvwiJ0yGocimxFqKTOYim9k0yGKZ2ORCk5MjzFZbvqVI6R4R1VMFYOvX4+RYikA4O+iaZ7cX942MNMF8aUmEzVJiNZpzLNZcc0SR2R7PwZioTNkG+yidbGazkm0usEzqHKHchmIRyuCCsuI8m5JfWGyx2s0mJcdqKVRirOZSTRQnD27IEtWQ7mxk2cUdVDQoqmgN3pC73vJHvtFvt+1y5TrO+TbZoNitBpO50GCdqFhyrqciy2lma2G+jfsh36bkma1m4JVrNRSB6uGgO6gFaGAxsHO4YrcohqIypRg8BwiWbDtYLB9MYFCMILQMkPY8s9NORqOlsBjAGYA9D6iDlZlnlU7tuEnadQZiJsVgs1mM+QbgJ5ssxpJCc5HdYGfy5OQXgJM6MYocQcmw5NgngfnbdeaSWM3FVoupxGjmZEz5oFh+dondzGSQGyGEg5uNBSUmJsmkfHuepcQOwhTma4wYB6tqSiBbYgN4pk64UmhmWss8QGx54W48whnPSItVsZnBDwCdD6Jq6l/HmgkHZIuZoe2yajrOaFIeBNYNCMwNOSXWImBo5ogmi2KzhCu2kuwJZqOdzTD9ciwFEGxMISMkTD7Tw9ZXljOBnCHbUmrmGqhRxAVoCIIiix3cYFNnmVeKXRGgrim2PENBgZxt1qwGYkCWGBrpaSmCuLAqhRaruUm1FXtZsTnHAIwiVKEarxYayiBbAN2Un5PPAs1QYIfQgwEQNZhMXHPVdCxBDVaQq6TAYJUZI5PZlp9bxMXIVXMVkFiEGoxAxMYwnPLYrufESMrAgBvMUNA0AQ3HKYeLGohXVFCm5LuFuczUsZrZP07ksGxgY4ZkfnGmhxlizmzlSJMsVpNNadeQh+0Yb+eC3I6lbTtuMvBMkpYv2WbIJEa1BHzAbFJqyW8QzPywHTJGMRQXQ3oZsgvMbEHVHSizgexySp7BruQZbEDRXNTIJizqXNFtUkqgCKtyuUSVuXCqhrfyqs1SwLKau405yaAUsOoBueIELDYYJxpyQTHIwyKLzEL1twVVI1ZQsEBEc0EOEypBr8SnpmQqGanxmSNj0vVKYoaSlp46IjFOH6e0i8mA53bhysjEzITU4ZkKQKTHpGSOVlLjlZiU0crQxJS4cEU/Ki1dn5Ehp6YriclpSYl6mEtMiU0aHpeYMlgZBHgpqZlKUmJyYiYQzUzlqBqpRH0GI5asT49NgMeYQYlJiZmjw+X4xMwUoAnCpSsxSlpMemZi7PCkmHQlbXh6WmqGHmjEAdmUxJT4dOCiT9aDEkAoNjVtdHri4ITMcEDKhMlwOTM9Jk6fHJM+NFwBYqmgcrrCQSJASqCh6Ecw5IyEmKQkZVBiZkZmuj4mmcEy6wxOSU3Wy/Gpw1PiYjITU1OUQXpQJWZQkl6VDVSJTYpJTA5X4mKSYwYzdZxMGJiqjsscMkMYrE/Rp8ckhSsZafrYRDYAOyam62MzOSTYHiyRxMWNTU3J0A8bDhMA52QRLo9M0HMWoEAM/InlknH1U0BdRiczNT2zQZSRiRn6cCUmPTGDeSQ+PRXEZf5MjecRMBzsyZyXosnLfMTmbowOgGLYmoJx+pgkIJjBxIAJuREsRJf+YaO52M5iW0tutTTyMqrWznAetWoRgBAeXASJq87xIWxLkFl811Grm2vDZttxuFp6efmA6IadSC29plIzVEAbKyUWq2xhxWRSvo1nOmyBhRZ1z1NshgJgBlgsizgU1EpDAaDZGsRslFCyczMstuYDyiRrvh2KiWIogVlr/mRtG7Zq2xTXQHFpwLi4ioMqv9VsK4ZdKr/UXFAWAbBWtpdxSfKLoFcr1FTn5jPa+zpbBbuSy4mbLHYZOroIRZZ5x/WHW6fbbXn/nD5IVvsg5ff0QbKrD1J+Zx8k39gHaUXeyCnZnHtGEw2qq2GR/0ivpDh7Jfl/o1eSVT/8x3olWU3YP9QryX9iryS7eiXld/ZKcqO+4Hf0SvLNeiXl9nsl2a1Xck/fRu0S7OdQJP6sdknW2iXlD7VLciNx+Xvjn90yyUUW5Q+3TPKf2jLJWsuk/P6WSb6+ZVJ+T8skN9kyKb+lZZIzY0YkD0llYsck/K7uSHZp/ke6I9nZHSl/pDuS3bsj5Xd1R3KT3ZHyR7ojFqyNEqWh8ZFv2vgov6HxkW/d+Ci30fjIvPFp3Dv8ekNjd8IP5E2DHAG3iFueXEVOyp+YH5kPFeThiOK84kitjLkdmTUciaFYZEHFqAxZUT7KRXnIjhTUCRlRZ7h3R/fCJwpG2QChoEEAY0c2uKzIjAyoEIXDbCIqAvgIGMWgAvgoKL2Blo0/meFuBpxS+DYBpHwbXHs2cM0ETqXAi/2XoiKAZnIYAOe3cYyD0QTAG4FKAMIIsAZOzcwxDFwjBagUwXcxwGQD3XyAUwDfAtwNfO16OhmcCqMQy6UzwWoR520CKS3wmfgbMG4fcgTXzgYyWbjE3UHHKLCZOwUn/q/xyeHrquXsml+ZJe1gh74oEj4mDb4U4CMAzgJ3K9jGzHGt3IoRQMMMOPFu1JxWdXr2xvhha0wmM/e2GWxuQZMAlvn2z/EYozQYVsoAJo9j5sNaMZfbzq3BLGDlGCyeGNXS66xyvR6uiCxpFJE300aGT1O6q140wMjdajfmhoy6/oGPfFv59udnedP+dumcDysyH9n5DIuyQm7riTBnAQ/8mixMszROr5BTc+VDPpcpj6+ZNb1yOZcizevhmt9Vb6nc1BhT4zmcy2Xh3i/i+MVazqkcLEDVrsVYvhYFBk5DtbSs0bRzKa6PJyOHY3GoUndSYNCq7GosO3OWeaudW5S0454z8LxmdxuXywg4Bk0/mWeBESK0kFOx8xWnfXJgVKBlUqcGGV0cWPVh8tshftXoZxxdNmEzxTxrTMDByLGd0pi4BnYea9mwauerKg/5FhzCtWw2gmQlnIpqk0k8BvJ41bFrlinkc+4aOXWwNopKVdoSbsNwN++wcSH3p+pr2a2C2AA7/CZ6hDfoGckriMIpq/mg0s7XrNrY+7fW2mk5Vdrihoi2c7lcUefSaBK3R+FtcXBmQw6v2kWahmY3jib+zXiE8zuzxASAMHJ6KozTfyyOC7TK5vSQUdth8hv8YYOdg2VnpiYd+4/dFl4ZXD5wr0UuC9xYCYoA3q5lg60RrDNXXBZzrwHueArX2cAll3ltbhxrqjXUvcRwC39a+C6naL4v5HdX/bgdX9j5TsR2ToOmUUQjS90Kl9mkTNtbVO7M5jlcRpMWSQU8Tq0NM6qkzKYmN5+7R51zBzXwHTGf14wC/iQ3aGTikjJ/FblZI7fRvqpyctZQA48eNXadPK63j+1XdXJKKWsauCLMwH10+xI05nO9PZqSLVzzdwHHy79JNZcbvGPlddbA64qLrnPG1hCRzny5fvcwa3XOzLVwcprEtTJx/HZN7IftGvS+HkOGNedu284tytScSbpuf8nm+W5xk7VEywNnnJTCan4TFjOjh7mdi7RMLoaPunsZeEU1N2C4+12V2TkjN5kpebzCK/xu02Q080i6WZw4a11TtdvEdwK1E3a3V1NWld0s5+7D35urNl41nXu1K9ucmcQ6h4KG3sOqYTSmWMwjeiJ852oeU/dDFlVyQ1X9T1aqm2uVreWIXdsPcxoslYD0nE8qSoEnxicVnjLRSOgj0/laIswp0Melw8oIeGK/PiSO+yWGr7D1djwbR8KYUUxFwzktlUY6fDPao2GG0Vb4M3saCvApQIvh6tEozkMP1DJAslQYM9rJMJsEd70GxzBiYWY4PLPxYMS6UJUf+yUmmTx3GB6TRZU0E+ZdXBtLlcg5OiVLhqd0oJ+grbJfmJLI6TH5w3l/xMYpmpyq5dI5dWYjRpnRjAWJkvgTmx0O9zSAy+D2jOE6q9KmcB3iYV3VRc8lUD2hShTLfzHLaA7BfmVLJrcC45SpQYZzPzJ94jg+4zqUQ6mSpWpeZmMXlQjNlqoczP4jGjhncP2T4KNw/TP5L4VhvokB+k66ztgZzCkwuWVujeFcvxhuh1TOYRCHY1Zk9kxqiLh0N6/EcnsxvzHJ4zinGG6RjCY1cVJz905T0SE3cBjM9dNzSyVx6Aywox7gExtm1HhM5LrGarZWaapxr8ZEkpt1Y7mOzLPDgKtei6kYbrvGWjA/jeTyu7RQPRCjfce62czl/RTNu055MjnnzCasMpLnop5DxXBfZzTkSDzP32RN8uENEeaqAcO1+ExtkKyxfZ155IS7ndqh0nLybuzBOB5PSZqEGQ3WUCHkW9BVa5ce9jUjf8+xN9Ttxju3e9fo6kbd+85wt1rr3gmoVXgwhy28Ds41q74tqXuW613HvXdr6g3b+Xas9vLOrtfVfai1u0Q7C3J1vSben6s9oK2hK7HwPtDS0JlM4quuPb1YOzuxNHrPY5wNfO8Pb+Dl3ItctNS+0sC7BcbN1oQ1b75DyTe8GRbz/V7lMomP7VpnwvQr0WDZ/OTr3oad5z83+kBp0gdOXZrqHNztb+X+LtbepfK5hVk/GaHRtSLne5nLJswC6rla4XVed0Ufo9YXXX+qwGyQ6ya5idtaRuoZHeMp83rlPOP67586/dmnvP9L50Fyo/Og6zuv/9x5kNzkeZDyF58Hybd1HtS4kze6yeQ663BC3t4JalMnLPJ/7VxJueFcSf7/50pu50quE4b/m+dKcqMd9r93riQ38bb2v3CuJDd5ruTS6K85V5JvcV7w15wryei3niu5/tbpzzxXcuVb43Olm+2+Nz9dUt/P1U7if+10SUaNT5eaPt34a06X5FtYV3Gz4P/2KZPMY+zGbuavP2WS/4dPmeTrTplc77p/5SmT/KunTMpfdsok/4ZTJuU/dsokcxuMAKpDuLSqtWNg/a87O5Kb9Pl/6+xIvuHsSPmvnR3JNz07cp0B/efPjuTfcHZ0K7r/2bMjZ2W9+Y5y44mP/DtOfNxPaf7MEx/5D5343PjO9vtOfGS3E59bnTv8GSc09hvoD0SukwaZ82FPEX/g31xFcrtMhCuSy2biXVME71+LYa5xN9b0vzK78V+JoYbfRl8/jf2u1Bt/dgszBtZfo6Q2kPwSRq52J/+uIFeak8uU/EzJv8LIT83JPyvIpTBy8fEY6SIlFyrIPyrIj7XkfC35gZJzfcn3g8hZSr7rTs58myGdqSDfAuC3GeT0qUjpdC05FUm+oeRrSk52J18FkhMV5EtKjvuTv08lX+whxyj5DMA/m0qO/m2wdHQq+dtgcuTTu6QjlHx6F/mEko8p+YiSDyk5XEE+ONRG+oCSQ23I+93Je5S8O8dPevdu8k4weZuS/ZS8RcmblLxByeuU7KPkNUr2UrKHklf9yCtzw6RXKNm9a4+0m5JdNWOlXXvIrhlizcthUs3YgfWkZqD4chjZSclLFWQHJS9Ssp2SFyjZZiLPNydbt4RJW01kS7W/tCWMVPuT50Do52rJZko2UVJFybP+ZCMlz2xoLj3TnWxoTtabyDoAWVdB1lKy5mlvaQ0lT3uT1ataSatNZFWlr7SqFan0JStl8hQlKyp8pBWUVPiQ5YC0vIIsW9pcWtaJLG1OnqwlTyzZIz1ByZLysdKSPWTJDLF8cZhUPpaUDxQXh5FFlCxcECEtpGRBBHkc1Hw8hsx/zEuaH0ge8yLzYGKeicwFS80NI3P8yGxKZs30k2ZRMtOPPErJDEqmUzKwftrUqdI0SqZOJY+YyJTMIGlKGJlMSRklDzcnk7xJqUxKKLHXElstsdaSh2pJMSUWSoooKQghEymZ4DdImpBB8inJm0py4SGHEjMlJkqMlGRTYuhLsmrJOG8ylpIHKXmAktGjZGl0LRklk5HBraSR3ckISoYD5+GDSGYQycC+UsYdJD2QDBsSIA2jJM2LpFKSkuwrpVCS7EuSKBkKK0MpGZLoKw0JIImtfaREX5LgQwZTEl9B9BUkjpJYoasUW0sG7SExQ8lASu6n5L4B/tJ9gWRA/xbSAH/Sv5+P1H9gfQvSz4f0paQPJb17BUq9a0mvnr5Sr0DSM9pL6ulLor1IjzYkyod07+Yldaekmxe5N9JLuteHRHqRiK7NpAhf0rUZCe9OutwTJnUxkXs6+0v3hJHO/qRTxzCpUwzpGEY6hHlJHVqQMC/SnpJQStq1ICGgZ4g/UUykbS1pAyq0MZHWPuRusODdlNxVS+4cRFrBQytK7jCRlmCplpQEA1JwKxJESSAlAZT4A4A/JX6gq98g4juVtDCR5pT4eAdLPpR4A7R3MPGiRPYlzSjxBDBPSjwCic5ERFgUIQKCCMwSSgR4FroS7EsQJXg3Ns1ZhLv8X/hB/20BbvnT+v8Bj4NvdAplbmRzdHJlYW0KZW5kb2JqCjEzIDAgb2JqCjw8L1R5cGUgL0ZvbnQKL1N1YnR5cGUgL1R5cGUwCi9CYXNlRm9udCAvTVBERkFBK0RlamFWdVNhbnNDb25kZW5zZWQtQm9sZAovRW5jb2RpbmcgL0lkZW50aXR5LUgKL0Rlc2NlbmRhbnRGb250cyBbMTQgMCBSXQovVG9Vbmljb2RlIDE1IDAgUgo+PgplbmRvYmoKMTQgMCBvYmoKPDwvVHlwZSAvRm9udAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9CYXNlRm9udCAvTVBERkFBK0RlamFWdVNhbnNDb25kZW5zZWQtQm9sZAovQ0lEU3lzdGVtSW5mbyAxNiAwIFIKL0ZvbnREZXNjcmlwdG9yIDE3IDAgUgovRFcgNTQwCi9XIFsgMzIgWyAzMTMgNDEwIDQ2OSA3NTQgNjI2IDkwMSA3ODUgMjc1IDQxMSA0MTEgNDcwIDc1NCAzNDIgMzc0IDM0MiAzMjkgXQogNDggNTcgNjI2IDU4IDU5IDM2MCA2MCA2MiA3NTQgNjMgWyA1MjIgOTAwIDY5NiA2ODYgNjYwIDc0NyA2MTUgNjE1IDczOCA3NTMgMzM0IDMzNCA2OTcgNTczIDg5NiA3NTMgNzY1IDY1OSA3NjUgNjkzIDY0OCA2MTQgNzMwIDY5NiA5OTMgNjk0IDY1MSA2NTIgNDExIDMyOSA0MTEgNzU0IDQ1MCA0NTAgNjA3IDY0NCA1MzMgNjQ0IDYxMCAzOTEgNjQ0IDY0MSAzMDggMzA4IDU5OCAzMDggOTM4IDY0MSA2MTggNjQ0IDY0NCA0NDQgNTM2IDQzMCA2NDEgNTg2IDgzMSA1ODAgNTg2IDUyMyA2NDEgMzI5IDY0MSA3NTQgXQogMTYwIDE2MCAzMTMgXQovQ0lEVG9HSURNYXAgMTggMCBSCj4+CmVuZG9iagoxNSAwIG9iago8PC9MZW5ndGggMzQ2Pj4Kc3RyZWFtCi9DSURJbml0IC9Qcm9jU2V0IGZpbmRyZXNvdXJjZSBiZWdpbgoxMiBkaWN0IGJlZ2luCmJlZ2luY21hcAovQ0lEU3lzdGVtSW5mbwo8PC9SZWdpc3RyeSAoQWRvYmUpCi9PcmRlcmluZyAoVUNTKQovU3VwcGxlbWVudCAwCj4+IGRlZgovQ01hcE5hbWUgL0Fkb2JlLUlkZW50aXR5LVVDUyBkZWYKL0NNYXBUeXBlIDIgZGVmCjEgYmVnaW5jb2Rlc3BhY2VyYW5nZQo8MDAwMD4gPEZGRkY+CmVuZGNvZGVzcGFjZXJhbmdlCjEgYmVnaW5iZnJhbmdlCjwwMDAwPiA8RkZGRj4gPDAwMDA+CmVuZGJmcmFuZ2UKZW5kY21hcApDTWFwTmFtZSBjdXJyZW50ZGljdCAvQ01hcCBkZWZpbmVyZXNvdXJjZSBwb3AKZW5kCmVuZAoKZW5kc3RyZWFtCmVuZG9iagoxNiAwIG9iago8PC9SZWdpc3RyeSAoQWRvYmUpCi9PcmRlcmluZyAoVUNTKQovU3VwcGxlbWVudCAwCj4+CmVuZG9iagoxNyAwIG9iago8PC9UeXBlIC9Gb250RGVzY3JpcHRvcgovRm9udE5hbWUgL01QREZBQStEZWphVnVTYW5zQ29uZGVuc2VkLUJvbGQKIC9DYXBIZWlnaHQgNzI5CiAvWEhlaWdodCA1NDcKIC9Gb250QkJveCBbLTk2MiAtNDE1IDE3NzcgMTE3NV0KIC9GbGFncyAyNjIxNDgKIC9Bc2NlbnQgOTI4CiAvRGVzY2VudCAtMjM2CiAvTGVhZGluZyAwCiAvSXRhbGljQW5nbGUgMAogL1N0ZW1WIDE2NQogL01pc3NpbmdXaWR0aCA1NDAKIC9TdHlsZSA8PCAvUGFub3NlIDwgMCAwIDIgYiA4IDYgMyA2IDQgMiAyIDQ+ID4+Ci9Gb250RmlsZTIgMTkgMCBSCj4+CmVuZG9iagoxOCAwIG9iago8PC9MZW5ndGggMzEyCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nO3P51YIAABA4XsOSlZEsiKUEdmibMnIJquBvP9D8A71w4/ve4B7zq1N2tb2djTQYDsbale729Pe9jXc/g400sEONdrhxjrS0Y51vBONd7JTTXS6M51tsqnOdb4LXWy6S11upitd7VrXu9HNbnW72e50t7nmu9f9HvSwRz3uSU9b6FmLPe9FL3vVUq9709ve9b4PfexTn1vuS1/71vd+tNJqa633s1/9bmOz8//82YIGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/Cf+AghWEvEKZW5kc3RyZWFtCmVuZG9iagoxOSAwIG9iago8PC9MZW5ndGggMTIwNDEKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0xlbmd0aDEgMjY5MDgKPj4Kc3RyZWFtCnic7X0JfBRF9nBVV/ckdA4YcnBDJyGcIUBCEokiCbkI5CIHh1yZZCbJQOZwZsIhN3grcilqVAREBFRAlhWEgCC6wiqyLEZFRGX544GAyB8VQ6byvaruSSYQWO/d7/t96XR3ddWrd79XrwpEhBFCgWg+Iqg4t6B/THHOU6eg5xzcxaUWg11+VP4EIZwFt650mku5/bWBvggJhewus5dbxgtTLyIkbgD4l8oNTjvyRT4ISTCO/MsrZ5bd0/rsK/BdjFCn+RUmgzGgXYkfQsrnMB5fAR0Bl3VnEAprD9/dKyyuGe/26RsP37cBvYcrbaWG/IhxwxAKPwjjBy2GGXbdBtEfoYil8K1YDRbTmClTx8P3VoSGrLHbnK6GBegOhAyMvmJ3mOyzDWu/hG8XQj4vICItFGqQCPzESk8Cha7qm3yIyoS2IJGfr47oREEQv0TWhn8gd4Pcvbi3iBT/vLI0I1KQ0uDWBdNgXO1jwaeLEW5oaADZi6VaRg2xH4LUn85auzM8MX+L6GF46+CNkYDmotVsLsyCd8Pphjc4lIpBRBLA+YAmWyEZ+SF/FAAWao3aID1qi4JQMApBoagdao86oI6oE+DugrqiboArDIWjCNQdRaIeqCfqhXqjPqgvikL9UDTqjwaggSgGxaJBKA7FowR0CxqMEtGt6DY0BN2OhqIklIyGoRSUitJQOspAw1EmGoFGoiyUjXJQLspDo1A+KkCFqAiNRmPQWDQOdD0eTUAT0SQ0GRUjAyphIuAyHIvLUA36GtpD0FpUR7qBzAIqg172fgkXgofVoBKAXCDegwvhbRHXIQHG54mHAYWAYwHXndCKFNfhGrQLnYHZC/Biabh0B4PmumK4fpAO4IvSYGEwGitaxCHiNnGBuA0gqsQycQHaCs/BwlHxaXGWeESchcYyznAWuxkfqBqPwBGoWqjGqbgDThUOo/2c/6G4Gt8qvSO9g2pRLc4DyJfQdEHGb+NLuD8ei7fBrB/QD7gbfMUJcfgC/hI4fgIdJWMlGVWjJbgtfNWgw8D3GXQJOcHsZWiJVCv0BW85gE6hD6EfoSlYgGcX0k+qhesi2oCmgGZOYUGq1QX7hIllwhV0Di8S1gtXcAQW4GqLu4E2J5HDYrH4tvgAjIJ2sEBiSTcyDJ4TGIRUi6uBi1O6MjwT4Ng1i0W1cEDYCTLuRScR89kpwgRhllCNTuLNeBdwjNA9eLNY7FMidkLVumpxLLrAdIOOCodBH3lcHw+hh3QD0Q+iDl0kWbgYIn8/s5K0HyMc5jNC1xatxCN8FoEkiCSgWeCtCB3CSNqvXgDlq+uCVoo9ySrgXRDmePSGZ6LDwmBSgp7m1wq8E61AO5ETAQrSY4ePThKJgFGU0marEJlp3Jo0aqxycFxYv6hrPpU2PspWlLc1YKays6Ehb6zYSRq3Veq8lUT6bhUjI07daPBUv6iReWOVnbhXWqqGNq04FToLxkKTfUE39Kel8jFGdasUCb+ZxVuV0grlwTYPRiQ+2MaU2I9HuC8SIMIJtCbQleR7aR20ITMG6cP0kWH6sAnkyfr3hHfdg+hKn8Arlxy63tzrwRuEDPAQCWIf9KknEQQmhIHdU6O/isaptEaqdR+gk/Ba97v0aTUGtpBNRA9zOH6sj+AX0dfuAJdyg8Oxm8HNgbz3sLQXMkY3gCOxQbEkNiSMhMEdERTB7rgwfuMI8KNBo8+PeXvMT/TD2zGiR0e/DZ8HR1/FPYdSPGA07n9h6AVpL52HF9B5tfTCh3QBnsfuD3HbWrzAvYleUHPZUnpMvFcXDFmpD2QfBJh79OipDw1tp+/Zo0fcoPiEhNgQ+Aphve1CQ0OCdT5Er9OFBIcG6ePj4wb1EN5dju/8p7H8fUvXD//6yWG8/I7td8Bv5zM7T3y6PCk746OcnOH0GO4nRffGuluHijhB12f7xr8cavXlad/wTrRPf4me0vXavWPnW4FkGJbEtLhbU+gr9CweNiw1hfEooVsbTuvcEHt+kFM7Qd6MBT7DGQuxMYyDnjGMrYhwxi7EYGyY13c7Lzjp0aT09KTktPSkDfv2bXjhjTfc1rfJ8INXKf/ct+8FPpyeIVTMcjhnzXI6Zm34aM+eEyf21ByvP6YLOL5nz8cf79lzfMNsh3POHKdjNretteG0VAe89YDsjTCOj08I0wk+OKynDjhATIFcjbExoNEePSI4O0SnMZUgHMH2AQMl3D7QOA770oVjJx2tOk8/mb+8R/cze/NfKS58fHh6VtT9ibc9OfU2cz/yGb09fbP1Vfo/U+leS3oqDjmx/NSUuCmJz73ZpQs9OyD61vjwMfRY/+kZzrW9e/NEz3wP38N9j3sevkfzOTa2il4SdkA+CGCe3EOIG9SWMR8S3FbYQb9/8O57HsB+TpeTXvoRgxbw/u/P01tPnqSJHO8KmPuSOjcovm3cIKFnWGjbkGDB51En/GC/B+5e9CC9dAH/7eRJ/Nb57+nQEydoyo8qT90hizwLOtPDhyT5RAJfUs/IyARJH6uPJM/SD3HvVPr+k/RYKu7HH0/iKHH/y5sX76Qb8Nidize/vPhVPJZueBVwHQAHSZdE8A2QDyIFcIXFQVja8Ae0D/ziDyTRffmc+7Lgf07wV+mX0JMCxovAs1AQi+CgGpz66b478CJ6hD6EqzhMKa4RqoXPmN4AJswoxLnfET6jJ9lYDUsEMF8bq2FRD5PnsDHUMExYq+k7Frzx+EHa422p9icL+MrYhtPii5ofI9yGeYO+DfMD3AaFKUjPnyRvss0+ebLdWozvoJ/TK/QH+jlWsIx9sSKcw+3PnKFf0TNffYXb08XUgldgJ3bhFdQCtI9ArXM30JY5XxJPY/qwj3AM/Ru+BdbRmPpCLJO3hmPd8Ktx9Acu5zKYUwo8tefZLEyPeVADZz4Q3rExIgt4IUjIrR+Azw4bOs88ar+16oz1O+x356u4Ez2DO+FTw+ammudnZ+HhffudO3bXsVc43lkgqx3w9oIPljbEsHCeV9RQjIiI0xoaIYURIuPve4S+Rz8u/0eZYd/EB5evXPHg8oXzZzsKNxWVHzJgHRZnk8ie+x/79MvISNw7PmFKaZn5yviJoyf16Y07Ksrr+xa9wGMyH2TaDHoQuGdjyKDgWBFMF3rhBXo3np2IF+zbR19zvyQ+4V5CNtfn06/pRdwGj1D94xHgfSHM76LaN4QpAoUEo+YiAOcHyCT3jp55fX/AbekH9EfH5zbrW2PmLls2d8Qmg1RLz3zpH0C/vXyJXhgYg/unpz9QNe3+vv3UNcEBNFZLF8AXujfPZlhNEz6MKAQUilAdIyZUeGo8/EwYPx7nFT0+/KmXA299aNQRNz1zgdbRUzgPd5+4RTi1SPsRDtNz/fq+XjNwIL18/CL9DD+AzdiBX1C4bUA/dpBPx+QLwWE4bBb53n2ZbsYX3TOl2uNXRXEXWxvswOMy7q8RUJ024zLSk9WYf6j2VbpDT1Bwk3qEhSUVFSXF5eX0uVmz6Q+X3XPueuAh+iO9CgxffvjjcQWjxo4dVTBOeHqa1VpVZbVVzeu9ad6ev721d96m3n32LP309OlPl+7BReOKi8eNm1wMOpsM/CwGnbVnOktQLZHA8iykLhSr6im8B/bQB2Z3Fjw+4qmXWw95aNR7btzxPPbBCt1CT0x+GY8bPwFUOWFCGA7uC3qKicF+H3+Hw+k0+gR9hI7rJlxYtGjh3XcvXLRItdd78AgSi7UagQeWPuw9nEZ3s1sspnPoepYBGKwL8guD5TWCmmE06M8gy7ApWqYRA2EKy2FDpHTxbWYPjHEIjsNSWv0zpPTqXHEhOUy301fr8JFz+AjDfQD3lNLJCQ8fIfw6IC5ksFfnkhPffHKe+7AXzqA4jpMDERM5XEcHnqMD65i3s30OIq+CjbvA3gQyAF/xm6cAbmIfWLIkkEGYS3ffNjkxdsCY7MyNEyzbC098mVGYOKSHqgZ8ZWD+vJLEQROi03NThuHEPr3efr3kqXGDbx/Zbz8vhgQ0kz6te0VaDT6VDfS8vAXrtCWbLZVxsWpx0bNHd8YHX5TaqfZupxMjwrv3VJer+O6QolhZApaPEM5NGjVqcvGovEn4zk4z8p7a/0Z13oxONfZ5g+KKaMNj5tfz7n20ePwdX9/tOJ1XfCf98YH19COnc8Zddzpwv7V/x5m2lHT6Mf0+Qug065Eld81cvJjekZH708GDdXkZi9wjgw49Y9ySedfdQ24toYf++hi9aiwpn5S31lC+aM4cnLnnVTxizuz7X1xTcmYu/Y6+z2UF60sPQpz5sIzM0hDmFyHH3f8aTdfBTuUZfIk+5/4fbN+B29CLUm1dXyFSyGextxFitB7m+sIqyYJWS5v6IE9Dy6pgE4VZSDiQX1SU//o7Zvh5Rwiw30V/opfd+wVf3P7qeLIiLyd7FH3T7SwpNRjoTKFD9zcWf/S+VFtzxPKkmvPGQ2wdAz/oAFsFsIQn6ahFC1M/0714rPiInW6lM/D9OM9+pNiwu3zX++/vKt9tyE+4Ba/BJtiYrbklgb6TmUqvfPUlvZKayfQAskjVXBa27uk1k+MwBUzHawYdyCGIs9///LNa2htbcQzuO3XyhAmTK+k/4Voubqu/8+xnn36FIwwuE73ywkb6o8llUPlmejIBbr+mqGTXRvKCe7vQ2/2hUFyfBqn4JD0L9yZ1/WZzRsGcVt5zGmfQ5z3w7vsadSMZeIyAbsQb6IYtMJJhIf36Ua6gJXjCvT9Y7vzUfuTzj96On9j9mNDZlp7OdWTGq5iOMtJpw7fnKW3dBoezbQXnS5rUgix4PG6AYmA9xYw399vCYPCTmYw7PkcQ1XojSIVlMHWsvmt4gpbxMRUfCxGuewbzLT2XEV9pA8gaWPvO4Yh7FzbqUzrcWEOAu8KvysL/4hXuGcLfaY77LGfjMyHMPaT+gpDl3u5lB8nDO0xRdX81h2ldw61bADD+DAar5UYEx/4amHwQ3kl30nc+oe/QHVJt/SnSra6vmFp/gkRerWnibZLHbphvu9jkRfhrPBzyb3s+D4kI5qF67fyB2e5AY82lWYvVq8RTcMGTtF61ZcuqZ7dseRanYhtdRmvoHroU28XjtP7cN7Qei9+cwyJuR430MbqSGvHTeAqeip9WfZvHqYyCGFesjhFBz2GNbr5RGApJvS29QBv+hz6Px79uslpBUe6z37jddeJeOtliNFZqvNJazmtr2OUgKaKRwWC2oeC8h2q8D+oEq8ISbMcpOANX7n8Vt6av0oZVW15cC0J0wk/gSsYeraDL6umjE+hWnQiCXGxQ5UCemCznMdnZK7vo9Z58Aot6u6Aw2AxGCCsrJ0+uXE6fF/RY/nH+wow7B9fQJc/FlBWSoXeUl42lC+gP7nek2r998Ojefm3nLaBjsdOez+x1N+SUjSBPTxb1ns1Pu3ZaOdi9qZbqyfeakMtDxTzrsUmzp+dNWXGqBjLYd/Npw4IFF+13zsyfde+h3Vi8ZDsrradvJtySlXdbSvuwmHdrfvwuPg6nZWUX5qRndQ0b8M9tn12MBNrgI+LnPPc2xlIr8QOaQp+nw1h8X80Rt3GfAv8YAHB6DqeuR/owTfHgw5ZZsy0QL6foCbj+Bf614LVnn32NzKtfQA/Qv+N4PET1TU+u12m+jcNa4QkQ7KV4Ig2kpRAG28QcFpYAG4+Qz05PHATxX7YqhCXgrK++xDlffIVHAHL75e8oVGlXD4hD2A1Obbu6jM+PAFpmNa6xGqH8l6zE/6SP07/RQ/QxaCWBj/aAa6pQ59ZhSgWhTjhBu2F2eowbYPsvZXL7I6IFub4tngVxGINnU8Qj/JwQXL/a/ZBQBTRZTf2FJ/ZCWOzG4bAK0ezuL3xx1SIcdadKtV/XJ9CTX5NDnr3le7pgrUqBxBMStkro73a7j+qCT9WtPnXNXkmCzRI+Tnu+jT/eq1vxk4XLOYfvMTkOqEyghI8LE15yH3W7hf700imp+BTA7CIbpFxV7xiSCfwSJz5xnC6hSz/Gn4AQx9geUujH6dEupJqe4HuzkLA4Ul1fRk/s3ctpFYoXhHxdGR8LAjwFkMW368ro/XgGj5dc8OWR4iyI80jvSjguEhThqVzCYr13NKHCZFNZfub42awySl7reubAZSztnTmvYlt2xdEKrL+Er2SNSM1eZul9n3vB+rKJ76x9a2fnotzoaKzv3OVbRrMaaNaC/H4sRiO9ylrCNwf6NgLQ7RnCyrK/VFgsFeU2my1lrXnXxYu7zGtT6F487MsNq1dv2PjssxuF2pKJdAd1w7VjYslaQAoyg0zk7yBTB+/45GuZVwVADiXfk3HfypX3Zd0/eOTTObByvAupWp9bLQ6hn8QM2PLMM1tiBtIT3brhBCgzQ3BCN3UPxmp1INOG5xcWflxNgr4N5N9Q4i1MJts/p6yesuvbb3eZ1yxnkpRZLKRGGPvTubWlE3AmJnBlTqw/xKRht2aPB0R2etmJWzP0WsaRpNpCfMDd2m/x3LJau+OMA7LwUdwHSxdwF/rXqWMmVrUWYsvmzk1JpecGDIQauR1uixPpGyvK5lRZkSfWSCzIEcSoMIZD1GwCHs1qVRKzLTUeB9HzdM/q1Tv364K/SUjNaUD1q0kxRjk7tqi6oOniYnEmaKKXWl2xDUtbXueGqXsWTyokXlsskgk62fnddzunrE4BDR2mn0x6ZVzR2vxVj9YabZXlJrt9b8kEnFJ3FSdPKF1fr6eX6GklDLeLj6teR3TrVlY/u+6xleuYDNUQu8tABl59hcXpWV3Nlc8l4TW+uJTW+AcGJfc2OZnfZK4r3fxXYZN7tA0/ucLaMaLny0+6j+uC3RtKJl5Q1yrAiU8Czmb7oafwMDab7hWLr67WBdNPOGzDGzSdw/rBysazrE7b/2tz3njt2VvGp2nztp86/82E+3SIT1Z5Pw9zm9cjMAk78TQaLATRmXQ63asLrn8FPwXuvRYfo/3UeRp/nDvOmS74p3MMJ5S8um/Bd1gkqzscrgZP8IbpBzVqJ0TVTvKQDabNr9IajIdmjDcKtCZxxAQTfKbeWj2xYg1ZX2G5cNo9Whge0Lnj9KkbnnV/LAzfNXXjM+7jYvG6ycV2jw2AZos2CPk3NnhqmccGgI+ZQPX/dYBPq2u83MarrhHummq3T51is03BEu5OP4Gtdz09jnuSWS+uWfMiuzGiB+k5uA7iW3AwXLcw29LRYi3g5rEb2ciglnCCvA8rTjJOG3POCshA5RC/a93bdfI6r4xDElgS4qHL42E0jwee17x5D/JKESw2wgQXZIIywGjR8oMaCzVkuicVuPs2ZogMCIOfflB1Te4H/tn54rVKbkfu71vQd/a9jO/b19zZpkd30j805JXn3fVi8U6riUgwfzLkxUswv4W6hWX6a+sWhpZsLt9fNGFcythbrrx44Zyx1l78TsWk4mElgz98+ZPT496EXHlxwIDYuL7Rfq0iVr/4l+0REbjNoEGJgwf0D/Dtuvb5bS91Bbrtge8MaRXPNTygQC+xekgPcSzd6PFVnEW3JxSsp+deX716tbSKvtGAaGROQgN65X18AiN8u5qzloJv3CsWs7weBHIHs8JR3T3HNSbiHktxqhDQOjgJvI3FRuZzJZt34O3CS/bx9Hz0fdM7RfR46Umh99XVa5m/YZZlxccBp06rWXBYZzzsah1OoS66SiyuryO6q6sBLgliqxzgrq1tkvHgjZtwwKZNeDD9ga55cSNdDbPqiegWhfqrq4lQTznvjM4DMF+tbYJ4LQXTcTV9FBe9dxQXwXsuffH05/RFYYgQQbfjLPdn7v24hK7i80Mh1y6D+TzTYPjVackmISEUquXWX4CwY5M2PbTblD8ypJVY7PYVrlyN3/3YN4kj2R+6sDqHQJQ1r3PMxO4+LwTV3wXphojFde7qBlQnlDH4L+g2MRHyTHeoYUJYGdB0usk8I1ZdM9RDMubnYuKbb243zE9ImG/Y/uabt8/LHlVqzMueZzm1df++lVWfuVYe3r/l1JhH1j/zSIdOjzy9fskYoHGWdsE7dF0az/BffVfX5Qqr3JrTb6cRZX/i4OFCJdrIRUTEF0PnZo8yGkdlzx3qzYhlzJL1Tz/SqcMjz6x/ZMypLfsPr3R9VrVy3/6t/CwJb5PSST7b/+IQ5jw9e7CLJcs4FlTtQtkF1KW0yWvGFC/w99UFPFyYVz3OsHp08YJAnS7woaLsJ0j+5by0RB0huiHZBY3NkfzP1YUJvQ4dWWKf3Pq271EXX751O/byi334+y+tN1zJrt8ceEurMoD1RZ4fmOdjocBT4N1Xsn/6PDCh8U/oPT+lYj6awJegTXAvAMsOxwLZh7bofNEcSUBLfeLRrboMZBUGoS1kBFoF9wpyAHWH8QNCDSoR3KgU3jXCGrCugMbCfQTuZXDPgjsf7kfgdmjfdrgnC6fQe3C7GA7PLfZDc31i0UxpOWolzUYbpeFovFSHNorfqLf0FRqv06GNwsvsbnhSehT6V6KNPn3QRtav6wrw6dr7IYDvj+4WvwZcH0EbcPp8h+KlxShCim+4IN2C8pksjGd4zwH6uwj7OxRPQo17AuVKPVG1mMrf+eK/UC4Jg3nQlhRULUxnd8MB8ZDa9pmPnmT94iV1HoMji+E7E00md6L2MLZUfAV10q1HSeI61AnaoeIAjusLoH+WvbkuF3B7hGrXAJSK5qJ/4K7YimfjarwTf4qvCLLQXughDBKMwlxhk/AV8SWJZAZ5mLwvthV7iUniaHGKOFd8VYqSRkgLpa3Sp9IVXRtdqm6SrlJ3r26tbrvufd0Fn64+qT53+bzkc9aH+ob73uI71vcu3+W+m3xrfN/1/dT3ciuhVdtWWa1mt9rU6mM5UB4g2+Vqeav8nnxWvuIX43eH3/1+O/2+8vf1T/Wv8F/uv97/b/5fBOgCugbkBUwKWBqwL2Af97JSkoP6ohmQ5QRYJZOYV0rs3NWX/d0V1BESsccXn1A9lT9D4Atr5xIiellrE9QRvaK1RWi/o7UlwH5Ga+tgbfhOa/siPW6ltf1QF9xRawe0fQZ7YiAQDQp6X2u3QX7BvlpbjwKDg9jfeBHZn14PCA7T2hj1CRmltQXkGzJLaxM0KGSR1hahvUNrS6h9yDdaW4cGhBKt7YvCQ2O1th9KDC3S2gGRiaFLtHYgqrj1kNZug0JvG6i19ajzbRkpNvtMh7m8wqX0Ku2txMCaqZTMVIaZXU6Xw2SwRCmZ1tJoJbmyUslnUE4l3+Q0OaaZjNHydVPj2dRCwzTLFJu1XBlmqLjBxFTTFMPoKqW0wmAtNzkVg8OkmK2Kvaqk0lyqGG0Wg9nqgSkwWJ1Kis1qNFmdJuMwW6WxxQHl5iOjTQ6n2WZVYqJj41UoBtQI089rdpnNCry6QPQKl8ue2L+/EfqnVUU7bVWOUlOZzVFuiraaXOkcjHHOZG9Ul9LLaTIpJaZK2/Te0crPkDNayaicaa9wKmaL3eZwAb9lDptFSXaYpmmseGhwvVapevUmI8tN1EFOg6Ky1mgcud9Nf+TrzfizPUC5hrLZKRsUl8NgNFkMjqmKrexaLLKcZ3JYzE5uDLNTqTA5TECr3GGwguhRIDuIBdNAY6DnKMVlUwzWmYodzAcTbCUu0JgZVGBQSoFpGSBdFSaPnkpLbRY7gDMAVwVgBy0z8yq9wrlKwnsDMqNicDptpWYD0JONttIqi8nqMrgYP2XmSjBSL4aRT1AKbGWu6aD+8N6cE4fJ7rAZq0pNHI3RDIKZS6pcJsaD3GxCFJi5tLLKyDiZbnZV2KpcwIzFrBFiFByqKgFtlRPgmThRisXEpJa5gzgrorxoRDGa/W0OxWkCOwC0GVjVxL+GNGMO0NqZol2yqjpOaHoFONZ1E5gZyqocViBo4hONNsVpi1KcVSVTTKUu1sPkK7NVgrMxgUohasxMDmeiLBcCOkOJbZqJS6B6EWeg0QmsNheYwan2MqvYmzxAHVOcFYbKSrnEpGkN2IAoMTST02YFv3AoFpvD1KLYimum3VRmAELRKlPNRy2GmRAtMN1oLjMzRzNUusD1oAFIDUYjl1xVHQtQgwP4qqo0OGRGyGhymsutnI1yNVZhEvNQQykgcbIZHn6c11JiKGUgwBVmqGwZgTbHw0cTNmDPWjlTMXu5uczEcZjY3/PksKzhZIpkdvGEhwl8zuTgk6bbHEanEt4Yh+GMtmdADmdhG85VBpbJ0uKlxASRxLBWgQ2YTqbZzI2MmWa4IGIUg90O4WUoqTSxAVV2wMwacpNRKgwupcLgBIwmazOdMK9r8m6jUgWZWOWriVWZM6dKeDOrOiF5Q1RzszEjGZRKlj0gVjyAdkPpVEM5CAZxaLXJzFV/mVM1IwUJC1g0VZYxpoanKem5OYVKQW564Zjk/DQls0DJy88dnZmalqqEJxfAd3iUMiazcHhuUaECEPnJOYXjlNx0JTlnnDIyMyc1Skkbm5efVlAg5+Yrmdl5WZlp0JeZk5JVlJqZk6EMg3k5uYVKVmZ2ZiEgLczlUzVUmWkFDFl2Wn7KcPhMHpaZlVk4LkpOzyzMAZzAXL6SrOQl5xdmphRlJecreUX5ebkFaYAjFdDmZOak5wOVtOw0EAIQpeTmjcvPzBheGAWTCqEzSi7MT05Ny07OHxmlALJcEDlf4SDRwCXgUNJGs8kFw5OzspRhmYUFhflpydkMlmknIyc3O01Ozy3KSU0uzMzNUYalgSjJw7LSVN5AlJSs5MzsKCU1OTs5g4njIcLAVHGa1CGzCRlpOWn5yVlRSkFeWkoma4AeM/PTUgo5JOgeNJHF2U3JzSlIG1UEHQDnIREljxmexkmAAMnwm8I54+LngLgMT2FufmEjK2MyC9KilOT8zAJmkfT8XGCX2TM3nXtAEeiTGS9H45fZiPVd7x0AxWZrAqamJWcBwgLGBnTIzWDBu9JmlJrsLubbWnCrqZGnUTV3RnGvVZMAuHCGFQJX7eNNWJYgsviqo2a3pgWbLcdRaurl6QO8u8qppV7jNBNkQCdLJTaHbGPJZLrZySMdlkCLTV3zFKehEojBLBZFHApypaESpjkb2WwWULJnMbQ7zDBlusPsgmSiGKqg12G+S1uGHdoyxSVQmiRgVJqSg8q/w+S0wyplnmaqnBkNsA62lnFOzFao1Sya6Fx9pa5ET6ngUso5cqPNJUNFF63IMq+4fnPp9HMr4N+nDpLVOkj5NXWQ3FQHKb+yDpKvr4O0JF/KMTk9a0YLBWpTwSL/llpJ8dRK8n9HrSSrdvjDaiVZDdjfVCvJv2OtJDfVSsqvrJXkZnXBr6iV5BvVSsrPr5Vkr1rJO3yblUuwnkOS+L3KJVkrl5TfVC7Jzdjl+8bfu2SSrTblN5dM8u9aMslayaT8+pJJvrZkUn5NySS3WDIpv6RkkguTR2ePyGVsJw//VdWR3CT5b6mOZE91pPyW6kj2ro6UX1UdyS1WR8pvqY6YszYLlMbCR75h4aP8gsJHvnnho/yMwkfmhU/z2uHfFzQuD3wSLxrkaHhF3/Tkqv9081RzfzNkkBnR9gp7fy2NeR2eNT87QynIhuxoJnIgMypHFciFFNQLlaLe8I5BA+CKhVYJQChoGMC4kBNuBzIhA7KgKOjNRFaAj4ZWMqqES0H5jbic/MsEbxPMmQZPI0DKP4NqfCPVQqA0DWhNgTlWgGZ8GGDOL6OYCq0pMG80qgKIUoA1cGwmPsPAJVIAixWedoApAbxmgFNgvg2oG/jYtXgKOBaGIYVzZ4RRK6dtBC5tgMP4C2Yov2nOaC6xE/i0cSliQO5Y0KM3Lg+m6/H0uwHtMg6p6tWlWZ3p2QVaSkT94TJq8NMAPhrgbPB2gOZMfK6D6zgacJhgTroXNo/OPXa/3rvYGOPOxH3BBNzZ0HSAZZb/fezJMGXAyEyAqeAzzTBm53y7NP2WQdvGuUnmWKddo5Vr5Wjy16pm/nojaWS4WpJdtacBWt5auz5yZLDdr7/knxWNv38OaNneTTKbYUTmLRfvYV5m4bqeCn02sMC/44VJlsfxWTi2psgwc54q+JhJk6ucU7FqVo/S7K5aS6Wm+pjqz1GcLxu3vpXPt2vRp1KwAVaX5mNmzQsMHIeqaVnD6eJcXOtPpRyO+aGK3YOBQau8q77siV5mrXAvLwnnljPwCGdvJ+erFOYYNPlkHgWl4KEWjsXFRzz6KYNWpRZJvRp5bKLAMhLj3wX+q3o/o9ikE9Zj51FjBAqlfLaHGyOXwMV9rQRGXXxUpSHfhEKUFs2lwFkVx6LqZDr3gQqedVyaZiy8z1sijwyOZl6pclvFdRjlZR3WtnB7qraWvTKIE2ZH3UCOqEY5+/MMonDMajyouM2aVptb/+ZSezSncmtv9GgX56vJ65okms71YflZFDzRUMaztlWT0ORF0cifjEYUfzNNTAGIUo5PhfHYr4yvIWpm81ioVFtrzI32cMLKwaKzUOOO/Wf0Np4ZmmzgnYuaNHB9JrACvEuLBmczWE+sNGnMOwd4z1O4zAbOucxzc3NfU7WhriWGm9jTxlc5RbO9hb+b8sfPsYWLr0Rs5TRoEkU309TN5jKdzNTWFpU603kZ59GoeVIl91NHY4/KKdOp0cvm3l7nWUENfEU085xRyb/kRomMnFNmL6uXNsqbrasqJU8ONXDvUX3XQ+Na/Tj/rUweLmVNgiYPM3Ab/XwOmtO5Vh8t8Ral2buSzzPfIJvLjdZx8Dxr4HmlCa+nx9nokZ54uXb1MGl5zsSl8FCazqUy8vnhLayH4Y1yXztDhjHPahvu5WVqzGRds76U8Hi3efFapcWBx0+mwai5BY2Z0AyuZ6sWyXa41NXLwDOqqXGGt91Vnj09couRUsEzvMLfTo1HE/ekG/mJJ9e1lLuNfCVQa2JvfbWkVdlLc942/LWx6tQqb0WTxBNtnkhilUNlY+3h0GY0x2jnHj0VnuWaxdT1kHmV3JhV/8hMdWOpSrQYcWnrYVmjpoajNE4nF+XAF6OTC1+FaAzUkfl8LBP6FKjj8mFkNHyxf6wlldslmY+w8XAejWOgzTDmoiKOS8WRD0+Gexz0MNwK/2ZfIwE+B3CxuWloLKeRBtgKgLNcaDPc2dCbBe80DY7NSIGeIvhm7QzEqlCVHvsnYwp57LB5jBeV00Lob6LanKtMTtHDWTZ85QP+4doo++dpMjk+xn8Ur49YO0fjU9VcPsfOdMQwM5wpwFEW/2K9RfDOA7gCrs9kLrPKbQ6XIR3GVVnSOAeqJVSOUvg/gzOOQ7B/IKeQa4FRKtQgo7gdmTypfD6jOpJDqZzlalZm7SYs0ZouVT6Y/kc3Ui7g8mfBpXD5C/k/wcNskwz4PXg9vpPBMTC+Za6NIi5fMtdDLqcwjMMxLTJ9ZjV6XL6XVVK4vpjdGOepnFIy10hBi5J4sHlbpyXvkBspZHD50rimsjh0AegxDeAzG3tUf8zksqZoulZxqn6v+kSWl3ZTuIzMsqOAaprmU8lcd82lYHYaw/lvkkK1QLL2TPHSWZP1czTrevgp5JQLW9DKGB6LaRwqmdu6oDFG0nn8ZmucFzV6WFMOKNL8M7eRs+b69cSRB+7n5A4Vl4d2cwumcn/K0jgsaNSGCiHfBK+au9JgXSvl+xxXY95uvnJ7V41N1ah33RnllWu9KwE1C2dwWMs1cE296m5JXbOa9jretVtLO2zP7lit5T1Vb1P1oebuqsbTJU/Va+T1uVoDOhurEhuvA22Nlcl0Ptq0ptu1sxNbs30eo2zga39UIy3PWtSES60rDbxaYNScLWjzxiuUfN3O0M7Xe5XKdN52aZUJk69Kg2X9d12zG/ac/1xvA6VFG3hkaaly8Na/g9vbru2lzFzDrJ6M1vA6kGdf1qQTpgH1XM1yjdWbvI9hS0TXniowHZR7cW7kupaRekbHaMo8X3nOuP7zp06/9xnwf9N5kNzsPOjayuuPOw+SWzwPUv7k8yD5Z50HNa/kS714ajrr8ED+vBPUlk5Y5P/YuZJy3bmS/P/PlbzOlZpOGP7vPFeSm62w/7lzJbmF3dp/w7mS3OK5UpNEf865knyT84I/51xJRr/0XKnpT51+z3Olpnhrfq50o9X3xqdL6v5crST+206XZNT8dKnl040/53RJvol2FS8N/nefMsncx66vZv78Uyb5v/iUSb7mlKlpr/tnnjLJ//aUSfnTTpnkX3DKpPxhp0wy18FowDqCc6tqOxnG/7yzI7lFm/+nzo7k686OlP/Y2ZF8w7OjpjOgP/7sSP4FZ0c3w/vHnh15MuuNV5TrT3zkX3Hi431K83ue+Mi/6cTn+j3brzvxkb1OfG527vB7nNC4rsOfhJpOGmROh31F/4a/c9Wf62Uq3P05b0ZeNUXz+tUOfc2rsZb/5tnN/t4Zavy/ADTMZf8vgut/ku8W5uMeiCKCI5Eent1xGAgi4e6oDr4iUCg8w7W+cA7H2gQrfLwb2g3PrkCK4C58tDPqAM9OqCs8O/KeDvzZnj/b8Wcof4bgYBQIWEP4F2sTHMTbbfmzNQ5Ec2C8Nf9ibYIDsD96GPoCeF8A2odE7I/9IJFIfITAcz70+WEZ9YA+NkLgmQR9rIfgVnymL3/6IH/+ZDN02x6PlpKDsI7LJfGnyKEIl0jgPZg/UVLDHNJwO6GU1F+NkuopuRpF6ij56UqG9NMcciWD/FhHfqDke0ouU/K/u8klSr6j5CIl33YlFyg5f06WzlNyTibnksRvzsrSNzHkrEy+riNfLQuVvqLkyzryRR05Ax9nKPkfSk5T8i9KTlHyOSWfUfJpHTn5SXvppJF80p6cWN1VOmEkHx+PlD6uI8cjyUdHI6WP6siHHwRLH4aSD2rbSB8Ek9o25P1jftL7CjnmR/4JEP+sI0cB/9FI8o9H/aV/RJAj7wVLR3qQ9w63ld4LJofbkndh+N0u5J1g8vdDu6W/U3Lo4ETp0G5yaL54MKnh7Ujp4ERyMEl8O5L8jZK3jOTNpW2kNyk50Jm8Qcl+Sva9nijtqyOvv9xJej2R7N3TUdobQ/bU6KU9HUnN7tZSjZ7s3uUv7W5NdvmT14DYa5TspGRHCHm1LfkrJdsp+Qsl29qRVzqQraFkC+DZUkc2w2tzHXkZ4F/uRF6C10tzyIuUbOpBNlKygZIXKFlPyfMyWUfJc2sDpecoWRtI1iaJa0BRa+rIapiyuit5Fl7P1pFVIPyqzuQZSp5+arf0NCVPVU+UntpNnpovVi+JlKonkuok8UlKngDveIKSx6PJSpi4smtSA3kMpj6mkEf9yQroWjGSLIfXckqWgR6WhZKlbciSSPIIJYspeZiShyh5kJIHKLn/vkjpfkruiyT3UnIPJXfHkEUryUJKFlAyvwOZJ5O5lMyhZDYls+rIXXVkJiXTp62XplMybT2pcnWSquqIqxNx1hHHHHInJXZblGSLItY6YqkjlXVkKiVTKDFTUlHqL1XEkHJKymKIyShLJkqMMjEmiaUlslTqT0pkYigOkQwrSTHWS8UhZLJMJlEykZIJ8D2BkvF3dJLGU3IHfN3RiYyjZGwdGUPJaPhOahhNSRElhV1JQTDJH9VByq8jo2BgVAeSl9tByqsjuTl6KbcDydGT7K4ka2SwlBVCRo7QSyODyYjMQGmEnmQGkuF1JCM9WMoIIenBJK2OpKYESqmtSUogGZYcKQ2rI8mAMzmSJA1tLSVRMvT2QGloa3J7IBlyW4A0JJTcFkBuNZJESgYHk1soSQgi8XEdpfhIEjcoWIrrSOL2iYPkAGlQMBk0X4yN8Zdig0lskhjjTwYOWC8NpGQA4B+wnvT3J9FBpF9UotSvjkSFREpRiaSvkfQxkt6U9AohPdvppZ5dSQ+FRHYl3SNAAX27dyURehKOAqTwOhLWmoQliUow6SaTrl1Jl84dpC6RpHPrIKlzB9J5J+SMZWKnANKxw0ip4xzSAYh2GEnaU9JOT0KBWmgdCYG+kEgSbCRBetKWEj186ylpYyStA9tIrYNI631iYBsSOF8MgJGAOuIfQ/xANL9Q4jdflAOInCS2osSXEh9KdJIs6SiRZCIliWIdIUYiwCyBQvYKkLCeoACCd2LjPYtx3/83ftB/moE/8KcL+j+GSO8SCmVuZHN0cmVhbQplbmRvYmoKMjAgMCBvYmoKPDwvVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL1dpZHRoIDI1MQovSGVpZ2h0IDEwMAovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovQml0c1BlckNvbXBvbmVudCA4Ci9GaWx0ZXIgL0ZsYXRlRGVjb2RlCi9EZWNvZGVQYXJtcyA8PC9QcmVkaWN0b3IgMTUgL0NvbG9ycyAxIC9CaXRzUGVyQ29tcG9uZW50IDggL0NvbHVtbnMgMjUxPj4KL0xlbmd0aCAyNjQzPj4Kc3RyZWFtCnic7Zx/ROvfH8cPM2+TmWQmk8zMW3IlSZLk+riSK0mSSa5JkkwmmSRXkiRJkiTJvCXXleTKlcyVSZJkkmRmZmbmbZJkZt5e33PO+8dWW7f7+X76tO93vZ//vLdzzvuc1+N9fu68X2cIqVKlSpUqVapU/RNpDLYKptBGFEaWmdNY8EePrtB2FECf/ECUXCsrtCVvLusVSJrXFtqWt9akjA6JxkLb8sYynCjs4C60MW+s8kCGfbrQxryx3jO7/peCLvQX2pi31lBaZvdXFtqWt5Z+Sx7mu55P1D5tezuL3lCmlQRp8P4ezbNJmAM4rnpDk95O2ga3Z6O//DcpGC8UK/yLIuxwUl1oM15f+kbH3PraaFdV/h8zmtIPbYPXUIzwFvfRLR3q0rG99pKc6Mq+9XM+JY2GxQVf4rjOLG3g4UfT42jbVEDIii8qeONGEh4pZs8a7MvGQ/BEJ5bCGfu6MnJP2eDWocTWHQo50TBYQHNfUyWbuWzAd4uRmrbLPLHHbGFNfjUNpfLQwY3Yp+18PvRimePZQB46rCXS5ZsixYyOpvKjQ/QDnvnOswKEqDdaXOhlZwrb6USvYz2usLqQdiVDnjoaYEu9RYWOGu9k9E2ylNe0KFW9p/l0p6BHnaXimtZXPOjIIdP5TGJAmwx8Y/2moEc+kyjM7iuWEZ5oVMaTp2y9TwoIdyrtP/ZZjBvaLCZ0hV3olAK0P6SQ+11lUSPv4GmKa+v+efZkVEYPFlVtZ/Q8OyjVvlFQC/89/YZdUbFu3P4Be+pTQS389/R32DU97jEqd32hzH1V/Ql7qxRF9+uoRgtl7qvqT/r7sBT1ftjTDzLptjStvx/2lLKHF6sTo94Pe3JT2cXbEHdu3w97yqH8vE056d7l+2FPt2X8UG4HCPw7Ym81Xyjwd/OWd8Xehr5k9u2Fm8km69E7YtetZO/N34fu3hE7Mh5AXr0HdmT1vl92ZOHSedidBbT49fQSOzJM3uagF4nr5YvsSNvqfVr1m8Xhav4yO0JlDt9DFnmSMxfI2FfW+LPsys92LEPr7HE0Sao/Hfthz/XL+P9Ui1vUmOw5p+kaF0NGHrtcGWwtfThZR3VxtHdVqlSpUqVKlSpVqlSpUqVKlao3lNZUW1ks20l/T0z7t+vb0JH7n5xm1f/5n11oSl/7jzF0+vzhxkVuvhR1c5vt5JtpybPOIjvnwVodrqPvxvWLsZ1R18is/1raXq1c9SzR/dSPHu4LuWpHOM+CCelnPLI4O2LGOM+yuO3KtK/7DteGq1DpvGejhoR0cpyLvnt24zuz/wbD8Nfst/OfG6O46IZNMa/NBtTHrdfSaNsSN6XTTXEbdUrhVCWTnGeaEtrWPAu01DYP10sDXJsHx9xMhymX3RKBkBnNAsQImS0GyRa0IO2nxhexWfqVgP3L7sXpwqe1YDO9pfYWYN2APwwCrJGA6iiAMIyMV5nd5gVUFQFID9DHPifuQ5+Xm0OQps7DUwD7xNWmLgaQ6slYU7UrbVmHa5FdzmsYrYDoZl3uBbgwGC6IU6JcOFXzHUCSHjVuwhmskP45hq/44bqC0r44l+uxq7DDRW0W+7n36CoJ4DEgZ7RrKcYNT/uvOvd+lcrswjKTYae70V59yeL+6TEmDpyenvSSsgF+kbrofIDb7ZWDWKQyh32GJDo0yMawmEqInJ5FUw9NhP32+6/Tk/1Wwg6HRmRYg2fYl0g+1HGJsKcXdJR9HmknMMTt+enNPRzm7gJn2OHMlmEf1ZYYB3gQBvQnK2NB+9jWaufOqT1ml9nhYUQrF192Cg9xSHYjLaPXfwP4otOXINM5JBPwQHqSG2dtQrqq/rKn7OZLuOchKb+q0GK2yKhNb7C1LdcQ9isTo9NpNZRd2ChfEp5ht95A8gESzRI7JIc0Ijtx2d9tLNWbGyemf8OeBvhutMrsLhI3jS1sjvReTn2NcCf+rpD757rMnob7Abl4exqOcNJvNPNtANqGe3AgzmcHBw7j5N4BG+7fFSEQRusbG+s3RPa+NBziRFuSWbjvpCVne42GsIfaGhoaaxnKjvOgL+nzsY8A7OHntiyyC2lyCo+yrwMcSy8D8hxAV9i3zwG42kfsuHT/SOBzyBEcR5Yb175n/YAR2e/2UhD5a4AWr+EAxtkI3LZksWu/A4xZw8A34LGRnBkQwvNmwg7Je6wUZdft4c6MgWM1oi2dAoTwZGIecjpHaL0LyYeHZLSWsHvJcYO7IyEfu8EH6Z76WwhVUfbEjzREmkYB5gxn4kvtzyPOEXvu7KGwj7Vibo5/yu4MdIT6g5PIGhg52Fz/KbHfNi8IEPDQ4uvjEK1GG3RoUdjreHIcCtfFKv7CbtEa2yvFbV4RZm9IQIRFm6SCqLoEuDHRCzGAsCd4PhGuI+xDwylIz43krffWB7iuYPCD/ErZ4w2rAlxhQ+aMNyB049rBURDJnaIVdjey3xHH9ix23GxOWsK9F/Nfw7sX/p7w8O6q1Obvasv2cVJaPG7u12NOToBwdYYdBwbHnFu42ZI3UkzTQgA32r5y3Obnuu327h3KjksN4DvxqC4eE2q5xw8VoY54UmIPNlVVVbE6yq6dSy0zjnzsGtyyz1xObNCFmbKzZQfUuDkCvUha5j1mzz1pq7CPI+0kcQDKsFdf4hrR+zbc4b7+neWO7bP2YKfCLrZkXLwpc7RzSmEvv8gK/ODAc44NN7/p8uyxTmPOzImit4XhCAdXIT3rkdivjKKRhB3pv5SjvOxsWM5GGBLZkY0aN48GcNcfZJCpP/UCO2KWFPa9EdfsDcDNB9Qf65uOf5+Y8/s/73oNGXbUeE3Ze3H2xz7f0YUAfrPMjoe/hI8EpsFv/Hj3c7i5NwiC/RE7GeloIr94J+mXeLoPbbnXbiT2+KTL5RqtEdmJHrOfuEh0C+4IUZIPvstbIrKjpiBlJ1499wdTk17hJXZk3IbstQ1cf8Qrk/XY4NDO0cEkXtmKp9cldvQJDz+rzC7Akg7LekkdRig7g9v0HAm0XOFpkgy95C2zz/RojjPgBrmA0zBVuCdIS7QuuQrv7U/WNvnYRW3hOp7AcyFTH8XTZaPIjlrjdBix7cveDYe5/6JWecGflqMJnqeOLuU/+VAj+srHYrH45cEEfXtsnOd/zYxOrAbOpYP7H274K5K9xhFKzFkv+aAYPs3zHINWE7EOVH0VD9FRH7nj/FbZ4H4kHg8u2JDpmA/TtaGT59dqrvmA6GEyw/Pb0k+lmsULUrSnnUFt0XiMKO5As4m4+HA6o/yhXn/IxzpRnxgbi58E45f0eLFmhefn6wL8ORnVNEORxBy+ltLCowfOPF4NWgtr0SIjy9IlGzKxNh3+ZsMyyosBpnXDHwscKnczVraSzpbaStaks9msYsJSlgSXszY9KlECS2xsBW47Vnwht1SwYrjexhpxlEVcZ+qVT9jmMhuOI2nxTVTYMmyVuPbTkaI1laxVhww2SSwrmYPKWLaCscjGWVhxCa+zYGv++1+h2CCz6higSpUqVapUqfrf1X8AYdsfFAplbmRzdHJlYW0KZW5kb2JqCjIxIDAgb2JqCjw8L1R5cGUgL1hPYmplY3QKL1N1YnR5cGUgL0ltYWdlCi9XaWR0aCAyNTEKL0hlaWdodCAxMDAKL1NNYXNrIDIwIDAgUgovQ29sb3JTcGFjZSAvRGV2aWNlUkdCCi9CaXRzUGVyQ29tcG9uZW50IDgKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0RlY29kZVBhcm1zIDw8L1ByZWRpY3RvciAxNSAvQ29sb3JzIDMgL0JpdHNQZXJDb21wb25lbnQgOCAvQ29sdW1ucyAyNTE+PgovTGVuZ3RoIDg5NjI+PgpzdHJlYW0KeJztnXl4FFX298+pqt7TnU4n6ewLBMIqyjoIQYEB9FEQGMZdHpcHdRh95YfjMjqD6CA6uKLgGnUAHR1BQAVEDMMeAYEEQkhCSEhn607S6XRn6b2qzvvH1TZCQgAdM471efgjVN9769a93zr3nHOrqwEUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFP7TYG934H8B0R2iKOS+CmIHAQeylaMr9AiigKre7prCmSiK/1FIrUGMLqOn0tAhgVtGAiAgFUA0R4MEKBL5d+J7u48KP0BR/MUjNQQxsZj+lonHwuiRQfzuAwRQI1k5eawG7RL/TGxv9lLhhyiKv3ikx1ygQTwRQqfcxccCUgovvxSDeX7+JtPP3juFruF6uwO/VORX2rhnXVgaxuau5A4AYQKXxL3ShodDP2/XFM6FoviLRU3ymwkQJOhG8ICAfoJ6CfYGftaOKZwTobc78IulOIwSYIDOVUYGsEuoWJX/JpTZuFisHMRjzxZDgxBSgqX/IhQbf7GM11AMBzYRO6Ruy/AAiTwFz7kOKPy8KDb+IqEkFY6spwQe1AjdSJoMHPXhaZz65+2awrlQFH+R8EPUtDwWBguUyIMKzhY96ZCsPH4ZIqG72PYCkF71EByQ/+CU73KKr7jF3zdJoeCPb/ZXiOJiXjwyETzjISPi8TDUiRikb3XPA+k5SuelG6O4/IDw55if4Fxzm8jEYWUYw0RpgpyjxuNhetYk6DU/vvFfFYoff/FwiAAQXuGmy1V8GQfNEnAAKgQOZD3yzzfR5dqfRO4AAB5Cr4xhAAmwRsR8ohwNPt4W9gZVBkX0F4Bi438ZyNc1ghrRLYFEAAAcyOk8TdBgUZieNQp6bW938BeDoviLR9zhxcn7aNkY9BK4JBQQZQABoIMoGimJpwla+Dog/N9Fmnnpaz9pEQ8GsF4GJPARHg9D6LuIgQM5g6cJGigOw+JoIVqJj88LRfEXg1Toly7TCn9zQ60IThlCBCKgREDfjagKSYsQjaTjYIAANSK//HwfohR3eCFbhZ944ZSILTL4CEUCEYAAwvSDEJkDyhBoghqOh+FJIx+tWPqeURR/wUh/dMIgFRwOooewQwYJvhd6ZwgAgVQIsUj9VOKSGH67V5hqPEfLYl6HPEXLP+xBuwR+wg4C8bunGLqbKCb6KzRQHIK/RvMxiqXvAUXxF4b0RydoERslbJIhfH5bSwhkRIrh6C8x8HVAuKPr5yilJ11g4bBEhHoRvQTSeU8OE/1oNfhl/k+W872SXytKruYCkO5ygorDU2H0yt0+QHY2BNhKEJThSXfzP63iMZfw8g+emJeedXOtMgHCwRA2St8+Z3/+tkgCrJPALEKt2HPhXz3KDtT5Ii13w5VqdIkXJncGAgYJm+W4Bc38yw5xd8f3zeZ30J/NskhwKIh2CS5CtByQFkmPFK3MZs8oY3ReSGVh7v8KcV8QGy9c7t+BIcJ6SX4+BfO+3S4Vj/tC4wz4RAtWiuil7p5WOGejQAaOkngsleASZTZ7RvHjzwtxqRv0yO0JYIv8o8aMA0oRwmvihQ/bwYC4op36qbBBwtaebqMug2MEMiAlC9Akw2/V/P/7iXa7/qfhe7sDvwDEHV7+dhtk67l66VxmGAGEnmyIDCATVxKCBhlOhaG/Ck+L6OnpLlIjRXMQw2EAvl9hEEiPlCSAU8IrNPzFZv1/bSjrYM+gQ5TXZWKjDOFuSvBAJo6SeMoUKE2gJJ4M3UgYAf2E9SLu8NOtRjglYuu55E4qICtHgwWYpqVxGiCKtEN6pAQenRJNUHEPKnI/X5RcTc9gcRgQ0Nu140E8QJpAl6iwSJJvVWMd0JUafKUdSsPo62pFkAA6SD6Ygv/nQo8McrfLAhmQUngapeaWeOEyGfH7JzS/lbtLgrEaQclIXgiKjT8PisPQShCkrqUZw8tj1CQCtzpOmGTi55rgtQDYREoXSN1VBQIIA/duGzol9HfTJgDFczRazb3dAZLM1aaAGSmFJzV+a93jOWyWpPFq7jFF7heGovieQR9BUzcpGg5AC9zrHfCALnJMWBaNv9XBOA0au5IzAvpkOh6G7qJVAtIh9VVxT34lOZKF+RYAkK/Viq/H0iAVpfCylQcvSqN51SPKm3AuGMWrOQ8kQI/UteJVCAk8XMEJuh8+03KJCnJ0sC8IrrOqEYCWQxWgv5soWAWQIXDL1kqv3CwkfduscJNJvl6E580gEycgxwmw7kde1a8URfHnA4LclToJgAPQcXj2p1EcQPejywF09xwBAZk4ulRFL90kLDD/oBKvTNZPgOLV/Gc4914SQbcuDQdg5KQ/RONo5Xse/xEUxfcGQUJ394rXoDDVwU3Q/7x9+rWgLJQ/OwjfPnTZZZaGR+AB+iiv4f5PoSi+N+jO5yEAHsDCnf0cMi1qARMPJ0PU5VvQVADD1HBS5N6M+4m7+j+HovjegMfvLX1nEEACaJZBPEvxTglCMjrErjM8aqR0GRqVp4V7RvHjf3YIQA0U1c17nSQCIigXJfrBxygDyggynPOf8vKznlEU3xuoECzdjLwE0EbS9gT4oO3C2lSegj0/FK+mN+AALDxgV04IAbTJ+IQHzv3WYoWLRbHxvQEBxHFdP3WDgH7iqkWYqZMu1MwrnAeK4nsDArTwoO/uITJAt4wfeeVbjYrof3IUxfcGCKBFSuZJ6Eb0AcI6iZ/nlPQgPeD8eTv3P46i+F4iTJQhQPdfHAE/QY0kbPRDphAgonheiU1/EhTF9xIEXKNEGTzpuhUyhgntEuYH1Xc5CUjOFJTp+vEoQ9hrYLaKbjGAlTvXJEiAzTJWi1xhCP/t7/ZrhwrnjZKd7D0kwO0BGq4BCGGteI7fDAQZwEvo7fYLUwA9Pa2p8B2Kje9N6C8xUBmmEWpK4Xt4rQQqe0w/DYriexMBUX41DhtFytFQHA/CjzDVCMAr90TPKIrvZVSI3DNx0CDRGLWcJlDUhauWgDgArwzxytuHekZR/H8F/OJYSuNoQRQNUlEcB11ux3YJAQgAVo7GaqifEpX1jKL4H8dPFy8KN0TTIB4aiKbp5H4CWTnQILB31HR5FgJgvyiYLsAoNRYE6SFzV+UUfoBiFXpAcgThiiYaJKD37B+wBOABEKg7u8FhF/Eo+z54V0JWaTQAEH5bXHG3acG7HiiXoF6CIIGfUARgv5RMBIjAAaiRTEjpPF2qRockPxOrQsWP7xlF8efBqQDMiaHmLt/DgWTlzn7OkbREAJTMQeCsESYALUKYvv0Ns7NQ6QUAeBBAcktg5mh9B54IQ6MEagQewEsQhRAGiOflgSrx90dUdAWPCA/+JJeqoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoPDfxn/F98Skgx2g53BfGKIRmmXSIXd3tHTUL1ym67mygsKF0MuKl5a4IFWAoyF0yRAk0CH4CQjAwNEAwfdYjLYtpIr+BfyyKREBgCiKsiwDgEbTC30Oh8MAIMsyIsqyrNVqe6zyP0AgEEBERBQEQZZlQejhi6xdK37u3LlEZLPZfD6fyWQaMmRIcXExx3FtbW2ImJCQsHnz5q+++uqqq67Ky8sTRfGtt96y2WwWi2XKlCn79+8XBKGurk6S2FeRgeM4rVaLiEOHDh0xYoTH43n44YcBQLqxCXjAMEGQQIsQx0GyAF4ZqkTokCmKoxSe/6BDXBqjmhfNmlqxYoUsy1u2bGlqarJYLEOHDuU4bvny5QAwffp0i8VSXl4eDocNBsOePXtYldzc3LS0tJdeesnlchGRyWSaOHFiamrq5MmTFy1aVFJSgl19IRoReZ6/9NJL16xZs3v37qVLl9rtdlmW4+Pjp02bRkTsEgDg/vvvR8RTp045nU6O4/x+PyKmpqZmZ2cvX75869atubm5NpstJiZmxowZPp/vL3/5CwD06dPnhhtu2L17dzgcvuSSS/7xj38cPHhw7NixK1asSE5Ofu+99xwOBwDExsaOHz8eERcvXtzdFD7xxBOjRo1atWqV3W5vaWmJi4tzu90qlcpqtaakpFgsFp7n4+Pjv/jii/b29jPqarXaiRMn7t+/PxwO+/1+tVqdkJAQDocHDRr0wgsvAMCIESOysrKqq6vD4fDAgQOXLVtWUlLy1ltvVVdX63S6SZMmVVRU6PX60tLSUCik0+kOHjzIbrwzWL58OcdxGzdubG1tZdc1depUSZIee+wxAHj11Vc5jvvss8+am5tjY2OvvPLKYDD49NNPA8DEiRP79etXXFwcDodNJtPOnTs9Hs+uXbtmzpw5b948u91eX18viqJOp2PXnpiYOGHChEceeaSmpiY9Pf3snnR9Q1RWVsqyXFpa2t7ebrFY9Ho9U7zb7ZZlOSkp6fbbb29qarrxxhs9Ho8oiqWlpSdPnrRarYMGDSorK1OpVKdOnYoonqFSqcrKygoKCi677LJbbrllzdjXKJHHD31AQMk8EoFLxqYgCQh6hDQVnhKhUpRvNHBfB6WnWvjFFgBwOBxEdOLECbvdrlarq6urb7311nvuueftt98uKyuLj48vKioKBoMmkyly3gMHDhQUFBw/fryxsREAoqKiYmNjd+7cOX78+JMnTx49epTpu4uhEQSj0RgMBt9+++1vvvnG6XQCgF6vV6vVmzdvJiJEXLhwYXZ29ltvvVVZWenz+WRZ5jgOAE6dOlVfX//WW2+ZTKbS0tKysjKr1Tpy5MiOjg7WeFVVVXV19bFjx0KhEDPGbW1tAJCXl5eQkHDw4MGWlhZ2OlEU9+zZY7fbk5OTz+hhU1OT1WptbGx88sknbTZbW1ubJEnl5eXsitRqtdFojI2NTU1NHTJkyPHjx91ud+RK2aJkNptzcnJsNltLSwvrW1JS0uzZs3ft2uVwOJ5++mmNRrNt27aTJ09KkoSIgUCgtbW1rKysrKzMYrFcccUV9fX1AHDs2LFgMGgwGLqUOwB88cUXWq322LFjHo+HnTc1NbWgoIB92tDQwHEcm1adTldbWzt79uw77rhj1apVxcXFrP1QKJSUlAQAzzzzDACsWLHixIkTHo8nFAqxdRURa2pqDAaD3+8noq1bt3bZk64VL8syEcnfQd/B/mu327dt25aTk7Np06Y5c+aw413WslgsAwcODIfDLperoaHB6XS2tbW1t7ffe++9p94+PuC3gwGA0nlskCFMYESQEVQIHQQekQarsEYEuySP1Xb+IchIZ0KhkN1u37lz5z333GMymTZs2ND57Kzw+++/P3fu3JycnJaWFjbH7e3tx44dW7BggV6vT0lJ8fv9iYmJkiSVlJQ4nU5EHDx4cGJiImsqJiYGANjIsuper7eiomLFihWPPvooAOzatctut1dVVbW3t0dFRSUnJ+t0umAw2NjYGAgEOjo6TCZTZHA6d6zzkcjBTz/9dNasWZMnT25vb4+czul0bt++fceOHWdP0z333DNv3rw9e/aUl5dLkqTX6w0GA8/z7G5sa2trbW0VRTE6OppdDhGZzWaz2ZyYmGgwGERRDAaDZrNZp9NFpqyhoWHPnj05OTmPPfZYnz591qxZU1NTI4oiG3lE7CwG6ORESZJE1PXbGTZu3Dh79uzLLrusra2NlWltbS0oKMjPz//www/vueeezg36/f6ampr9+/cvW7Zs3Lhxf/7zn9lHkiS1tLSoVKrf/e539957b0VFhdfr5XneaDQKgqBWqzmOCwQC7e3tzFgwC3K+ij83ROR0OktKSubPn19XV5eYmNhlMY7j9Hr9vn37RFFcs2bN6tWri4qKPB5PTU3Nps2bHlj5AL3WSGYOfAQ+mfoJECKok8DI0TAVnBSxLEwj1VgQwqow1EuSPcgnn+kc+/3+I0eOvPTSS3l5eV988cXZfaisrFy2bNkbb7zBDH9UVJTdbm9ubj5y5MjLL79cWVkZKZmdnc3cElmW8/LyIsfffffdZ599NhgMRkdHG43Gurq6+vr67du3V1RUEFFUVJTRaAwEAgCQkJBw9913r1+//uWXX37ttddMJlNCQkJniTN3s/N/z+htSUnJyy+/vHLlStZbg8HgcDgcDsebb77JtNK5ynPPPTd//vxRo0ZVVFTIsmwymUaNGjV8+HCfz6dSqTQazaFDh1pbWzmOs1gskVqJiYkvvfTS1Vdf3fm8jz/+uNFoZH+LonjixAlJkh599NHc3Fy73R4Oh5nQu5zl8yEvL+/RRx9dvXq1KIqJiYlardZms9nt9kcffbS2tvbs8n6/v7Cw8I9//OORI0fOcOdMJtNzzz1XXV3t9Xq1Wm3//v379etnNpsNBkN6enpxcXFFRYXVarXZbN2FUheseDaCbrf71KlTGzdunDFjxtmrbQRRFA0Gg8/ne+ONN3bv3j148OCOjg6v11tdWxPc1aQWAZJ4rAhDPA/NMrYTxHIQADwaosvUUBjCVgIthx4ZP62hq8+Mw/R6vSAIra2t1dXVd911l81mGzZsWOcCLpdrypQpl1xyCVtJExMT+/Tp09zc3NbWVlBQsGfPnjfffHP+/Pnz5s3Lzc2NSJO5kmxNyMrKGjNmTHNzMwAkJSUNGTLE6XR6vd7a2tq8vLzXX389ISEhMTFREIRgMFhXV7dy5cr09PQdO3asWbPm6NGjdXV1zDrCdwYyOzv7+eefBwCDwbB3794zZPTOO+9ceumlzPtKSUnJysrKy8tzu93l5eUFBQXr1q3rXLioqOjBBx90uVySJOl0uiFDhuzYsSMrKys3N5cVqKmp0Wg0ra2tixYtitQKh8Pr1q177bXXImM4fvz4zpNrMBg6OjpOnz79t7/9rb6+3u/3GwyGQCBwho96/uTn548fP/6aa65hQWBaWprRaKytrfV4PIWFhfn5+W+88UZn3UdFRbGI0Waz3XDDDQ6Ho3///p3ntKKior29HRH79u3LjD0L5ACABT9RUVF6vT4zM7PL/lyw4nmez87OrqqqamhoqK+vP3jw4IgRI3qsNX/+/C+//DInJ8fhcHg8Hre7xdnuTIV48BIEgIYKWBaGFB7f9MovmDE/gLUiqBHbZDIithLRSLBL8IcftGm1WkePHr158+aWlpa9e/eOHz/e5/N1LvD2228XFBQMHjy4ra1Nq9X27dt3/PjxxcXF9fX1drt96dKlNpvtHH1es2bNyZMnf/Ob37S3t2u12qSkpKysLIvF4nA4qqqq/vSnPzkcjqlTp1osFovFEggEgsFgTU1NY2OjzWbbs2fPhAkT4uLirFYra621tfWDDz5gXj4AIGIwGOzs9ba3t+fl5c2aNYtZr4SEhMGDB3/zzTdNTU01NTW///3vIzEAo6mpyWg0hkIhAIiJibn88svHjRuXkJCwYsUKm80miuKLL74IABqNRq1WR2rV1NRs3LiR3WlElJaWdvXVV8+cOZN5TSqVatSoUYWFhR6Phy1iRqMxJydn7969Z5z9/CkpKdmwYcNf//pXn89nMBgsFkt8fLzZbHa5XFVVVY888khHR4fZ/P37AxMSEkaMGLFly5aWlpZdu3ZNmTKFWQ3mO/n9/hEjRkiSpNFoUlJS7r///rVr186ePXvbtm1er/fvf/87EWm12qFDh/J81++dvRivxu/3T58+fdOmTQ0NDYWFhR999FF3rXcmJiYmLi6OlQwGg6FQCPDbt9gBsB9oJ9ASdEhAzAOgb1+tKANAF79wTURTpkwpKiqqqqpyu90nT54cNGhQZ6tZVla2cOFCl8sly7LZbM7Kylq0aFFOTk5jY2Nra2thYeH+/ftXrFhRVFTUZYf37t17+PBhlqIxm80jR4587rnnxo4d63Q629vba2pq9u3b99FHH7344osDBgxQqVQul8vr9QYCgdra2qamppaWllmzZkVcPkmSmpqaOrslZxj4tWvXfvHFFw0NDUQUGxs7fPjwZcuW7dixw+Vytba21tbWHjx4sLCwMGJffD4f88EAQK/XZ2dn8zyfn58fFxf38ccfezweROQ4zmg0JiYmXn755ayWLMter5edWpblUCjE87xKpWKfIqIoiiNHjiwoKHC73Xq9fty4cUOGDNm3b1+P89slRHTVVVfFx8cz6xsfHz9x4sS77767qKjI7Xa73e7i4uKtW7c+/vjjkSqyLF911VXHjx+vrKxsbm4uLy8fOHBg5NPGxkYmIUR0uVzp6emrV6+eMWPGhx9+2NzczLyvzMzMrKys7tKUF/mm1dzc3IEDB8bGxgaDwf3799vt9nOXdzgcq1atKiws9Hq9AGCJsWij9CATqAE0iLUi6BDqJbpBjyUiaZAyBQoSaRGaJBAA8Shs8XfZ8u233z548GCWDz106FDEiwAAFjmwdJgkSZs2bbJarS6XSxCEcDhss9kWLlx47NixLpvdunXrO++8c/z4cRYGCYKwbt26+Pj49vZ2lUolimJFRcWdd975+eefHz58ePv27bfddtvEiROHDRuWnJzMnBybzVZSUhIbG8saNBqNY8eOTU5OTk1NTU1NzcjIGDlyZOdZ+fjjj0tKSpgDxvP8xo0bExISWltbBUEQRbGpqempp55av359pHxqampaWhqz321tbcXFxTt27DAYDAaDwWQy8Tzf/h1sHWCkp6fn5OSYTKbY2NjY2Njo6GiNRvPZZ59FCoiiuH379gEDBphMpuTk5Iceesjlcp17chld5rvy8/O3bdtWXl7OXJpQKLRy5cqhQ4eyxHkgEDhx4sTSpUvPiHZkWZ44ceKgQYN4nq+vrz98+HDEzGdkZLBYWRRFjuOqq6unTZvGslImk4llC1iuprvA4yIVn5SUtHPnzjFjxhgMhtbW1u7iYkZVVVVSUtLNN99cUlISCAQEQTCbzMlzBpAA4JQhgYNGGdRICTwgggFpkAoPBEFAEgDCAEaO5iTTrVFnt0xEHR0dl156aUZGBiK2tLRE3M2EhIRly5ZVV1cHg0EAaG5urqurc7lcJ0+eZIGm2+0+ceLEu+++e4b3zygoKHjxxRerq6uZXOx2O6teWlrKqns8nrq6un/9618PP/zw/PnztVrt6NGjjxw5cuedd7LAJhgMMv+ENahWq8eNG3fttdfabDabzVZZWdm3b9+IkwMAixYtYmEiANTW1tbU1LhcrsrKStb/hoaGf//730uXLo0k3fr37z9w4MCYmBh24du2bbNarStXrrzmmmsee+yxrKyszo1H0Gg0DzzwwLvvvtvQ0NDU1HTo0KGbbrqps3OFiAMGDEhMTLzuuuvmzJmzcuXKc8xsj6xevfrBBx+sr69nmZyGhgaHw9HU1FRSUsIGtqWlpaCg4NChQ51rEZFerx8zZgxbISPTykJ/i8Wi1WpFUaytrb3tttsSExPvvPPOBQsWzJ07t7N31B0X+aZVj8ezYMGCOXPmtLW1FRYWnuFAR/ptsVgmTZq0cOHC4cOH7969u6GhAQDi4+OHDRu2YeGq2ZdOhzqJzAKqAOwS6gHUCH7gCkMgAGUIeFoEA1J/AZJ5PqprxykzM3Pz5s3XXXfdunXr6urqIsfdbjdzggEgMTGxb9++EUMSDAaZcMvKypYsWVJTU3N2s8uXL2cODABYrdasrCxmMxDR7/eXlpYGg8Hy8vLFixfv3LmT7cKkpaVt2LDBarWy6REEIRQKMb1GBqSz4els2ADg4MGD7HTJyckZGRmR0wUCAWYpGhsbc3NzT506xco/9NBD0dHRV199Ncv5VlVVrV+/fv/+/W63OzY2tqqq6ozcDiMQCKxfvz4YDN5www0AoFKp+vXr9/7777OBYoRCoU8//ZT9PXny5M6pnrOnuPPfJpPp2muvjVwXz/P9+vX78ssv3W43fLcodR7GkpISr9d77Nixp556qqSkpHPLgwcP3rJly4wZMzZv3tw5rk1JSZk6darH4ykrK2tsbNy0adOhQ4eefPJJSZJ4nmeWl114l2sOdKf4SM61899nHHn11VcXLVp07733Pv/886WlpZGUbaQRWZbLy8srKyuJiG2/s2gjMzPz/fffz132EiVqsd2PNSKl8GjhoFqCVhl4IBMHmQKWhiAIMEiFGwL0igme6LZja9euHT169OTJkwGAiZ6IgsHgsGHDOjo6BEHIzs7evXt3pGNvvvnmM888U1tb63K5Dh069Pnnnz///POd29TpdJs2bbrtttu8Xq8gCJmZmV9//XWk+quvvvrCCy/U1tY6nc4DBw5YrdaysrLTp09XV1erVCp2L6lUKhYoR1yC7hZZdkaXy3X69GmWWBwyZMhXX30VKfDaa6+9+OKLVVVVdrt97dq1R44c2bZt21VXXRUdHT137tw1a9aMHDnS4XA4nU6WOeU4jm0g8jxvMpn69OljMBgiZ6+urmYuNTui1Wp/85vfsN3ozmPbuXvnEMYZBRoaGj799NPIEZ7nfT5fbW2t3+/XarUjRozo7D4tWbLE4XA0NjY2NTV98MEHo0ePPqPB5557rn///tOmTQuFQix/RUT19fVbt2698sorEZFtg7S1tbHVjHk7Go0mOjraZDJ1tjWd6VrxqampRMS8IrPZnJyc3KdPH5aX5Hk+LS0tFAqVlJQsWbJk0qRJQ4cO1Wg0brfbYrHExcWlp6erVKrIxgq74QKBgNlsTk9PHzNmzOLFi10ulzYuTnq5ha7V4JYg1kvgkiGeg3Q1SIQ1EhSFQIOQwUFpEG7T86MMrGOxsbGyLGdmZrLkCc/zHMcNGDCgqKjokksumTVrll6vl2XZYDA8/fTTBoOBzXdGRsb06dO3bNnCpnz+/PkzZ87U6/WSJAUCgcLCQgDIyMjgeR4RzWZzWlra9u3bo6Oj+/TpYzQahw8ffuONN65duxYADhw4sGDBggkTJrB0LyLOmTNHrVazXSq2qZmQkBAVFTVp0iQWNCcmJrJtII1GE3G6rFar0Wjs06ePKIqxsbFr1qyxWCx9+/aNiorKzs6eM2fOhg0bACA/P/++++77/PPPBUFgmZOGhga2qwoA77///rx5844cOXLHHXcwm+f1en0+n06n0+l08fHx2dnZkydPPnDggE6nS09PjwQVETQaDZsyRPT5fGq1OjU1Va1WR7JYMTExOp0uIyPD5/OlpqaqVKqoqKj09PRwOGw0GllOSaVSud3uYDB4Rtqe47jk5OT6+vq+ffuazea+ffved999r7/+OgC88847ixYt2rVrF9sdFwQhOjo6LS1Nr9cnJydzHMdxXP/+/fft2zd+/Pjrr7/++PHjkiQlJCTk5+dv3bp15syZN91009dff11eXs62gQGAjXB8fPzYsWNvvvnm7hISXVv+TZs2EVFbW1s4HFar1Var1el0EhHbjIiOjm5tbb399tsjgv78889dLpdGo0lPT3c4HBzHRXYN4bt7fejQoSz123m5ET/zgpnDbT6sFSEEoEYIEwBBFEfD1NwTbrElXcV9788cPnyYiNiWPs/z0dHRiDhz5kwAYFFdR0cHEfE8zwIjIjIYDP369UPESJbjk08+MZlMjY2Noigyi37TTTd98sknLKpWq9WCILB7mKW6UlJSEHHChAms+urVq9VqdTAYZAPi9/tfeOGFtWvX7t27d/PmzRzHzZgxY8KECSyrIEkSa4eINBoNEd1yyy0A8N5776nVatYBRGQjLMsyG20AmDp1Kjtdbm6uxWJpb2+XZTkqKmrUqFEqlSotLa3zfFVVVcXGxq5aterIkSNFRUXZ2dmjR4+ePn366dOnr7nmmn/+8588z7PwozNEJAgC23xgFooZNZ/Px3weAFi1ahXLCDEfdcKECW63++jRo8yyGo1GtuXCXO0z/ChETE5OdjgcLJmYnZ0NAKNGjWKnXrx4MduFZRbKaDQ2NTWxFkwmE8dxs2bNAoAtW7awtAzbVL7++uuZbWUlP/vss6NHj27fvp2Ipk2bNnLkyGuvvbZLd64Hxf/MyAUdYOLgmAxBoLBMFoTpBgBJQFVvd01BQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUHhQvn/gJtDAQplbmRzdHJlYW0KZW5kb2JqCjIgMCBvYmoKPDwvUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KL0ZvbnQgPDwKL0YxIDYgMCBSCi9GMiAxMyAwIFIKPj4KL0V4dEdTdGF0ZSA8PAovR1MxIDUgMCBSCj4+Ci9YT2JqZWN0IDw8Ci9JMSAyMCAwIFIKL0kyIDIxIDAgUgo+Pgo+PgplbmRvYmoKMjIgMCBvYmoKPDwKL1Byb2R1Y2VyICj+/wBtAFAARABGACAANgAuADEpCi9DcmVhdGlvbkRhdGUgKDIwMTgxMjA2MDYzODA2KzAwJzAwJykKL01vZERhdGUgKDIwMTgxMjA2MDYzODA2KzAwJzAwJykKPj4KZW5kb2JqCjIzIDAgb2JqCjw8Ci9UeXBlIC9DYXRhbG9nCi9QYWdlcyAxIDAgUgovT3BlbkFjdGlvbiBbMyAwIFIgL1hZWiBudWxsIG51bGwgMV0KL1BhZ2VMYXlvdXQgL09uZUNvbHVtbgo+PgplbmRvYmoKeHJlZgowIDI0CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMjEyOSAwMDAwMCBuIAowMDAwMDQxODYwIDAwMDAwIG4gCjAwMDAwMDAwMTUgMDAwMDAgbiAKMDAwMDAwMDIyMyAwMDAwMCBuIAowMDAwMDAyMjE4IDAwMDAwIG4gCjAwMDAwMDIyNzkgMDAwMDAgbiAKMDAwMDAwMjQyOSAwMDAwMCBuIAowMDAwMDAyOTc5IDAwMDAwIG4gCjAwMDAwMDMzNzQgMDAwMDAgbiAKMDAwMDAwMzQ0MiAwMDAwMCBuIAowMDAwMDAzNzQ5IDAwMDAwIG4gCjAwMDAwMDQxMzMgMDAwMDAgbiAKMDAwMDAxNTc0MiAwMDAwMCBuIAowMDAwMDE1OTAwIDAwMDAwIG4gCjAwMDAwMTY0NTcgMDAwMDAgbiAKMDAwMDAxNjg1MyAwMDAwMCBuIAowMDAwMDE2OTIyIDAwMDAwIG4gCjAwMDAwMTcyNDAgMDAwMDAgbiAKMDAwMDAxNzYyNCAwMDAwMCBuIAowMDAwMDI5NzU0IDAwMDAwIG4gCjAwMDAwMzI2NDEgMDAwMDAgbiAKMDAwMDA0MjAyNCAwMDAwMCBuIAowMDAwMDQyMTQ4IDAwMDAwIG4gCnRyYWlsZXIKPDwKL1NpemUgMjQKL1Jvb3QgMjMgMCBSCi9JbmZvIDIyIDAgUgovSUQgWzw5NjIyNzIyY2Y2ODA5YzI3MDI1MWUwMjFhMTg2Nzc3Mz4gPDk2MjI3MjJjZjY4MDljMjcwMjUxZTAyMWExODY3NzczPl0KPj4Kc3RhcnR4cmVmCjQyMjU4CiUlRU9G";
//			$strEncodeFile = base64_encode( $strFileBody );
			$strRawMessage = "MIME-Version: 1.0" . $nl;
			$strRawMessage .= $strFrom;
			$strRawMessage .= $strTo;
			$strRawMessage .= "Subject: =?utf-8?B?" . base64_encode( $strSubject ) . "?=\r\n";
			$strRawMessage .= <<<EOD
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="MixedBoundaryString"

--MixedBoundaryString
Content-Type: multipart/related; boundary="RelatedBoundaryString"

--RelatedBoundaryString
Content-Type: multipart/alternative; boundary="AlternativeBoundaryString"

--AlternativeBoundaryString
Content-Type: text/plain;charset="utf-8"
Content-Transfer-Encoding: quoted-printable

This is the plain text part of the email.

--AlternativeBoundaryString
Content-Type: text/html;charset="utf-8"
Content-Transfer-Encoding: 8bit

$strBodyHtml

--AlternativeBoundaryString--

EOD;
			foreach ( $attachments as $strFileName => $strEncodeFile ) {
				$strRawMessage .= <<<EOD
--MixedBoundaryString
Content-Type: application/pdf;name="$strFileName"
Content-Transfer-Encoding: base64
Content-Disposition: attachment;filename="$strFileName"

$strEncodeFile

EOD;
			}
			if ( sizeof( $attachments ) > 0 ) {
				$strRawMessage .= "--MixedBoundaryString--";
			}
			$mime = rtrim( strtr( base64_encode( $strRawMessage ), '+/', '-_' ), '=' );
			$msg  = new Google_Service_Gmail_Message();
			$msg->setRaw( $mime );
			$service->users_messages->send( "me", $msg );
			return true;
		} catch ( Exception $e ) {
			return "An error occurred: " . $e->getMessage();
		}
	}
	/**
	 * Returns an authorized API client.
	 * @return Google_Client the authorized client object
	 * @throws Google_Exception
	 */
	private function getClient( $credentials, $token ) {
//		require __DIR__ . '/../extensions/vendor/autoload.php';
		$client = new Google_Client();
		$client->setApplicationName( 'Payroll Gmail API PHP Quickstart' );
		$client->setScopes( Google_Service_Gmail::GMAIL_COMPOSE );
//		$client->setAuthConfig( YiiBase::getPathOfAlias( 'application' ) . '/credentials.json' );
		$client->setAuthConfig( $credentials );
		$client->setAccessType( 'offline' );
		$client->setPrompt( 'select_account consent' );
		// Load previously authorized token from a file, if it exists.
		// The file token.json stores the user's access and refresh tokens, and is
		// created automatically when the authorization flow completes for the first
		// time.
//		$tokenPath = YiiBase::getPathOfAlias( 'application' ) . '/token.json';
//		if ( file_exists( $tokenPath ) ) {
//			$accessToken = json_decode( file_get_contents( $tokenPath ), true );//
//		}
		$client->setAccessToken( $token );
		// If there is no previous token or it's expired.
		if ( $client->isAccessTokenExpired() ) {
			echo "Google Client : token expired, please fetch a new one \n";
			return null;
			// Refresh the token if possible, else fetch a new one.
//			if ( $client->getRefreshToken() ) {
//				$client->fetchAccessTokenWithRefreshToken( $client->getRefreshToken() );
//			} else {
//				// Request authorization from the user.
//				$authUrl = $client->createAuthUrl();
//				printf( "Open the following link in your browser:\n%s\n", $authUrl );
//				print 'Enter verification code: ';
//				$authCode = trim( fgets( STDIN ) );
//				// Exchange authorization code for an access token.
//				$accessToken = $client->fetchAccessTokenWithAuthCode( $authCode );
//				$client->setAccessToken( $accessToken );
//				// Check to see if there was an error.
//				if ( array_key_exists( 'error', $accessToken ) ) {
//					throw new Exception( join( ', ', $accessToken ) );
//				}
//			}
//			// Save the token to a file.
//			if ( ! file_exists( dirname( $tokenPath ) ) ) {
//				mkdir( dirname( $tokenPath ), 0700, true );
//			}
//			file_put_contents( $tokenPath, json_encode( $client->getAccessToken() ) );
		}
		return $client;
	}
}