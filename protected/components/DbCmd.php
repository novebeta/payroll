<?php
class DbCmd {
    private  $cmd;

    public  $select;
    private $arrSelect;
    public  $from;
    private  $arrFrom;
    private  $join;
    private  $arrWhere;
    public  $params;
    public  $group;
    private  $arrGroup;
    public  $order;
    private  $arrOrder;
    private  $arrHaving;
    public  $limit;
    public  $offset;
    public  $as;
    public  $isUnion;

    private $inCounter = 0;

    private $tablePattern = "/\S+((\s+(AS|as|As|sA)\s+)|(\s+))\S+/";

    static public function instance(){
        return new self();
    }

    public function __construct($table=''){
        $this->reset();
        $table && $this->addFrom($table);
    }

    public function reset(){
        $this->select = '';
        $this->arrSelect = array();

        $this->from = '';
        $this->arrFrom = array();

        $this->join = array();

        $this->arrWhere = array();
        $this->params = array();

        $this->group = '';
        $this->arrGroup = array();

        $this->order = '';
        $this->arrOrder = array();

        $this->arrHaving = array();

        $this->limit = false;
        $this->offset = false;

        $this->as = false;

        $this->isUnion = false;
    }

    private function addArrFrom($table){
        if($table instanceof self){
            $this->arrFrom[] = '('.$table->getText().')'.($this->isUnion?'':' AS '.($table->as? $table->as : 't'.count($this->arrFrom)));
            $this->params = array_merge($this->params, $table->params);
        } else
            $this->arrFrom[] = $table;
    }

    public function addFrom($table){
        if(is_array($table)){
            foreach ($table as $t){
                $this->addArrFrom($t);
            }
        } else
            $this->addArrFrom($table);

        return $this;
    }

    public function clearFrom(){
        $this->arrFrom = [];

        return $this;
    }

    public function addSelect($fields){
        if(is_array($fields)) $this->arrSelect = array_merge($this->arrSelect, $fields);
        else $this->arrSelect[] = $fields;

        return $this;
    }

    public function updateSelect($index = -1, $fields){
        if(is_array($fields))
            foreach ($fields as $key => $value) $this->arrSelect[$key] = $value;
        else
            $this->arrSelect[$index] = $fields;

        return $this;
    }

    public function clearSelect(){
        $this->arrSelect = [];

        return $this;
    }

    public function addCondition($condition, $operand = 'AND'){
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $this->arrWhere[] = $condition;

        return $this;
    }

    public function addIsNullCondition($field, $operand = 'AND'){
        $this->addCondition($field.' IS NULL', $operand);

        return $this;
    }

    public function addIsNotNullCondition($field, $operand = 'AND'){
        $this->addCondition($field.' IS NOT NULL', $operand);

        return $this;
    }

    public function addInCondition($field, array $value, $operand = 'AND'){
        $this->addInOrNotINCOndition('IN', $field, $value, $operand);

        return $this;
    }

    public function addNotInCondition($field, array $value, $operand = 'AND'){
        $this->addInOrNotINCOndition('NOT IN', $field, $value, $operand);

        return $this;
    }

    private function addInOrNotINCOndition($in, $field, array $value, $operand = 'AND'){
        $this->inCounter++;
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $condition = $field." $in ( ";
        for($i = 0; $i < count($value); $i++){
            if($i > 0 && $i < count($value)) $condition .= ' , ';
            $key = ":inOrNotin".$this->inCounter.$i;
            $condition .= $key;
            $this->params[$key] = $value[$i];
        }
        $this->arrWhere[] = $condition.' )';
    }

    public function addParam($key, $value){
        $this->params[$key] = $value;

        return $this;
    }

    public function addParams($params=array()){
        if(is_array($params))
            $this->params = array_merge($this->params, $params);

        return $this;
    }

    public function addJoin($table, $condition, $join = 'LEFT JOIN'){
        if($table instanceof self){
            $joinTable = '('.$table->getText().') AS '.($table->as? $table->as : 'jt'.count($this->join));
            $this->params = array_merge($this->params, $table->params);
        }else{
            $joinTable = $table;
        }
        $this->join[] = ' ' . $join . ' ' . $joinTable . ' ON ' . $condition . ' ';

        return $this;
    }

    public function addLeftJoin($table, $condition){
        $this->addJoin($table, $condition, 'LEFT JOIN');

        return $this;
    }

    public function addRightJoin($table, $condition){
        $this->addJoin($table, $condition, 'RIGHT JOIN');

        return $this;
    }

    public function addInnerJoin($table, $condition){
        $this->addJoin($table, $condition, 'INNER JOIN');

        return $this;
    }

    public function addOrder($fields){
        if(is_array($fields)) $this->arrOrder = array_merge($this->arrOrder, $fields);
        else $this->arrOrder[] = $fields;

        return $this;
    }

    public function clearOrder(){
        $this->arrOrder = [];

        return $this;
    }

    public function addGroup($fields){
        if(is_array($fields)) $this->arrGroup = array_merge($this->arrGroup, $fields);
        else $this->arrGroup[] = $fields;

        return $this;
    }

    public function clearGroup(){
        $this->arrGroup = [];

        return $this;
    }

    public function addHaving($condition, $operand = 'AND'){
        if(count($this->arrHaving)) $this->arrHaving[] = $operand;
        $this->arrHaving[] = $condition;

        return $this;
    }

    public function clearHaving(){
        $this->arrHaving = [];

        return $this;
    }

    public function setLimit($limit, $offset = false){
        $this->limit = (int)$limit;
        if($offset!==false) $this->offset = (int)$offset;

        return $this;
    }

    public function union(array $tables){
        $this->reset();
        $this->isUnion = true;
        foreach ($tables as $table)
            $this->addArrFrom($table);

        return $this;
    }

    private function prepare(){

        if($this->isUnion){
            $this->cmd = Yii::app()->db->createCommand(implode(" UNION ", $this->arrFrom));
        }else{
            $this->cmd = Yii::app()->db->createCommand();
            $this->cmd->select = ($this->select? $this->select.(count($this->arrSelect)?', ':''):'').implode(", ", $this->arrSelect);
            $this->cmd->from = ($this->from? $this->from.(count($this->arrFrom)?', ':''):'').implode(", ", $this->arrFrom);
            $this->cmd->join = implode(" ", $this->join);
            $this->cmd->where = implode(" ", $this->arrWhere);
            $this->cmd->order = ($this->order? $this->order.(count($this->arrOrder)?', ':''):'').implode(", ", $this->arrOrder);
            $this->cmd->group = ($this->group? $this->group.(count($this->arrGroup)?', ':''):'').implode(", ", $this->arrGroup);
            $this->cmd->having = implode(" ", $this->arrHaving);
            if($this->limit !== false) $this->cmd->limit($this->limit);
            if( $this->offset !== false) $this->cmd->offset($this->offset);
        }
    }

    public function query($params = array()){
        $this->prepare();
        return $this->cmd->query(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryAll($fetchAssociative=true, $params = array()){
        $this->prepare();
        return $this->cmd->queryAll($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryRow($fetchAssociative=true, $params = array()){
        $this->prepare();
        return $this->cmd->queryRow($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryScalar($params = array()){
        $this->prepare();
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryColumn($params = array()){
        $this->prepare();
        return $this->cmd->queryColumn(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryCount($params = array()){
        $this->prepare();
        $this->cmd->select = 'COUNT(*)';
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function getText(){
        $this->prepare();
        return $this->cmd->getText();
    }

    /**
     * The alias name,
     * Will be used when applied as subquery
     * @param $alias
     * @return $this
     */
    public function setAs($alias){
        $this->as = $alias;

        return $this;
    }
}