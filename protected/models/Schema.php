<?php
Yii::import( 'application.models._base.BaseSchema' );
class Schema extends BaseSchema {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public static function getByPaycode( $paycode, $bu_id ) {
		$skema = Schema::model()->findByAttributes( [
			'paycode' => $paycode,
			'bu_id'   => $bu_id
		] );
		return $skema;
	}
	public function beforeValidate() {
		if ( $this->schema_id == null ) {
			$command         = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid            = $command->queryScalar();
			$this->schema_id = $uuid;
		}
		if ( $this->seq == null ) {
			$criteria         = new CDbCriteria;
			$criteria->select = 'max(seq) AS seq';
			/** @var Schema $row */
			$row = $this::model()->find( $criteria );
			if ( $row->seq == null ) {
				$row->seq = 0;
			} else {
				$row->seq ++;
			}
			$this->seq = $row->seq;
		}
		return parent::beforeValidate();
	}
}