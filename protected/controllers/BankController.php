<?php
class BankController extends GxController {
	public function actionCreate() {
		$model = new Bank;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Bank'][ $k ] = $v;
			}
			$model->attributes = $_POST['Bank'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bank_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'Bank' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['Bank'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['Bank'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->bank_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->bank_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Bank' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		$client = new \GuzzleHttp\Client();
		$response = $client->request( 'POST',
			'http://hrd.ena/api/GetAllBank', [
				'headers'     => [
					'Authorization' => 'B7076B5F-269E-11E8-8341-6DB33A098066'
				]
			] );
		echo $response->getBody();
//		if ( isset( $_POST['limit'] ) ) {
//			$limit = $_POST['limit'];
//		} else {
//			$limit = 20;
//		}
//		if ( isset( $_POST['start'] ) ) {
//			$start = $_POST['start'];
//		} else {
//			$start = 0;
//		}
//		$criteria = new CDbCriteria();
//		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
//		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
//			$criteria->limit  = $limit;
//			$criteria->offset = $start;
//		}
//		$model = Bank::model()->findAll( $criteria );
//		$total = Bank::model()->count( $criteria );
//		$this->renderJson( $model, $total );
	}
}