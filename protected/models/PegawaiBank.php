<?php
Yii::import( 'application.models._base.BasePegawaiBank' );
class PegawaiBank extends BasePegawaiBank {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->pegawai_bank_id == null ) {
			$command               = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                  = $command->queryScalar();
			$this->pegawai_bank_id = $uuid;
		}
		return parent::beforeValidate();
	}
}