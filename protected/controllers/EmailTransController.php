<?php
class EmailTransController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$pegawais  = CJSON::decode( $_POST['pegawai'] );
			$fail_name = [];
			$msg       = 'Berhasil disimpan';
			foreach ( $pegawais as $peg ) {
				$model             = new EmailTrans;
				$model->pegawai_id = $peg['pegawai_id'];
				$model->email      = trim($peg['email']);
				$model->nama_      = $peg['nama_lengkap'];
				$model->grup_      = 0;
				$model->bu_id      = $_POST['bu_id'];
				if ( ! $model->save() ) {
					$fail_name[] = $model->nama_ . '-' . CHtml::errorSummary( $model );
				}
			}
			$status = true;
			if ( count( $fail_name ) > 0 ) {
				$status = false;
				$msg    = implode( '; ', $fail_name );
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionCreateGrup() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$model        = new EmailTrans;
				$model->nama_ = $_POST['nama_'];
				$model->email = $_POST['email'];
				$model->grup_ = 1;
				$model->bu_id = $_POST['bu_id'];
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'EmailTrans' ) ) . CHtml::errorSummary( $model ) );
				}
				$pegawais  = CJSON::decode( $_POST['pegawai'] );
				$fail_name = [];
				$msg       = 'Berhasil disimpan';
				foreach ( $pegawais as $peg ) {
					$modelD                 = new EmailTransDetail;
					$modelD->email_trans_id = $model->email_trans_id;
					$modelD->pegawai_id     = $peg['pegawai_id'];
					$modelD->nama_          = $peg['nama_'];
					if ( ! $modelD->save() ) {
						$fail_name[] = $modelD->nama_ . CHtml::errorSummary( $modelD ) . '</br>';
					}
				}
				if ( count( $fail_name ) > 0 ) {
					$msg = implode( ', ', $fail_name ) . ' gagal disimpan';
					throw new Exception( $msg );
				}
				$status = true;
				$transaction->commit();
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'EmailTrans' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['EmailTrans'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['EmailTrans'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->email_trans_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->email_trans_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				/** @var EmailTrans $model */
				$model = $this->loadModel( $id, 'EmailTrans' );
				if ( $model->grup_ == 1 ) {
					EmailTransDetail::model()->deleteAll( 'email_trans_id = :email_trans_id', [
						':email_trans_id' => $id
					] );
				}
				$model->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = EmailTrans::model()->findAll( $criteria );
		$total = EmailTrans::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionPreview() {
		$bu_id = $_POST['bu_id'];
		$arr   = U::previewEmail( $bu_id );
		$this->renderJsonArr( $arr );
	}
}