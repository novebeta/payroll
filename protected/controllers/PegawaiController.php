<?php
class PegawaiController extends GxController {
	public function actionCreate() {
		$model = new Pegawai;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils      = CJSON::decode( $_POST['detil'] );
			$transaction = Yii::app()->db->beginTransaction();
			try {
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST['Pegawai'][ $k ] = $v;
				}
				$model->attributes = $_POST['Pegawai'];
				if ( $model->nik == null ) {
					$model->nik = $this->getNumber( Pegawai::GROUP, Pegawai::DIGIT, Pegawai::VALUE );
				}
				//save store
//            $store = Cabang::model()->findByAttributes(array('cabang_id' => $model->cabang_id));
//            $model->store = $store->kode_cabang;
				$msg = "Data gagal disimpan.";
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				foreach ( $detils as $detil ) {
					$pBank              = new PegawaiBank;
					$pBank->pegawai_id  = $model->pegawai_id;
					$pBank->bank_id     = $detil['bank_id'];
					$pBank->tipe_bank   = $detil['tipe_bank'];
					$pBank->no_rekening = $detil['no_rekening'];
					$pBank->fotocopy    = $detil['fotocopy'];
					if ( ! $pBank->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Detail Cash' ) ) . CHtml::errorSummary( $pBank ) );
					}
				}
				$msg = "Data berhasil di simpan.";
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		/** @var Pegawai $model */
		$model = $this->loadModel( $id, 'Pegawai' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$detils      = CJSON::decode( $_POST['detil'] );
			$transaction = Yii::app()->db->beginTransaction();
			try {
				PegawaiBank::model()->deleteAll( 'pegawai_id = :pegawai_id',
					[ ':pegawai_id' => $model->pegawai_id ] );
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST['Pegawai'][ $k ] = $v;
				}
				$_POST['Pegawai']['last_update_id'] = Yii::app()->user->id;
//            $_POST['Pegawai']['last_update'] = Now();
				$msg               = "Data gagal disimpan";
				$model->attributes = $_POST['Pegawai'];
				//save store
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Cash' ) ) . CHtml::errorSummary( $model ) );
				}
				foreach ( $detils as $detil ) {
					$pBank              = new PegawaiBank;
					$pBank->pegawai_id  = $model->pegawai_id;
					$pBank->bank_id     = $detil['bank_id'];
					$pBank->tipe_bank   = $detil['tipe_bank'];
					$pBank->fotocopy    = $detil['fotocopy'];
					$pBank->no_rekening = $detil['no_rekening'];
					if ( ! $pBank->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Detail Cash' ) ) . CHtml::errorSummary( $pBank ) );
					}
				}
				$msg = "Data berhasil di simpan.";
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg//,
				//'print' => $print
			) );
			Yii::app()->end();
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'Pegawai' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400, Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$params   = [];
		$id       = Yii::app()->user->getId();
		$sri      = Users::model()
		                 ->findByAttributes( array( 'id' => $id ) )
			->security_roles_id;
//        $store = Pegawai::model()->findByAttributes(array('nik' => $nik))->store;
		$criteria->select             = "DISTINCT pp.pegawai_id,pp.nik,pp.golongan_id,pp.status_id,pp.cabang_id,pp.leveling_id,pp.nama_lengkap,pp.email,pp.tgl_masuk,pp.npwp,pp.bank_nama,
            pp.bank_kota,pp.rekening,pp.last_update_id,pp.status_pegawai_id,pp.jenis_periode_id,
            CONCAT(TO_CHAR(pp.tdate,'DD Mon YYYY HH12:MI:SS'),' by ',pu2.user_id,' (',pu2.name_,')') AS tdate,
            CONCAT(TO_CHAR(pp.last_update,'DD Mon YYYY HH12:MI:SS'),' by ',pu1.user_id,' (',pu1.name_,')') AS last_update,
            pp.jabatan_id,pp.tuser,pp.birth_date";
		$criteria->alias              = 'pp';
		$criteria->join               = 'LEFT JOIN pbu_users AS pu1 ON pp.last_update_id = pu1.id
            LEFT JOIN pbu_users AS pu2 ON pp.tuser = pu2.id 
            INNER JOIN pbu_sr_cbg_area_bu AS sr ON pp.cabang_id = sr.cabang_id
            INNER JOIN pbu_sr_level_bu as sl ON (pp.leveling_id = sl.leveling_id OR pp.leveling_id IS NULL)';
		$criteria->condition          = 'sr.security_roles_id = :security_roles_id AND sl.security_roles_id = :security_roles_id';
		$params[':security_roles_id'] = $sri;
		$criteria->order              = "pp.nik";
		//        if($store != 'ALL'){
//            $criteria->addCondition("store = '$store'");
//        }
//        $criteria
//        $cabang = Cabang::model()->checkCabang();
//        if ($cabang != 'ALL'){
//            $criteria->addCondition("store = '$cabang'");
//        }
		if ( isset( $_POST['bu_id'] ) && $_POST['bu_id'] != null ) {
			$criteria->addCondition( 'sr.bu_id = :bu_id' );
			$params[':bu_id'] = $_POST['bu_id'];
		}
		if ( isset( $_POST['nik'] ) && $_POST['nik'] != null ) {
			$criteria->addCondition( 'pp.nik ilike :nik' );
			$params[':nik'] = '%' . $_POST['nik'] . '%';
		}
		if ( isset( $_POST['nama_lengkap'] ) && $_POST['nama_lengkap'] != null ) {
			$criteria->addCondition( 'pp.nama_lengkap ilike :nama_lengkap' );
			$params[':nama_lengkap'] = '%' . $_POST['nama_lengkap'] . '%';
		}
		if ( isset( $_POST['cabang'] ) && $_POST['cabang'] != null ) {
			$criteria->addCondition( 'sr.kode_cabang ilike :kode_cabang' );
			$params[':kode_cabang'] = '%' . $_POST['cabang'] . '%';
		}
		$criteria->params = $params;
		$total            = count( Pegawai::model()->findAll( $criteria ) );
		if ( ( isset( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) )
		) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$criteria->order  = "pp.nik";
		$criteria->params = $params;
		$model            = Pegawai::model()->findAll( $criteria );
		$this->renderJson( $model, $total );
	}

	public function actionEmail() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		$params   = [];
		$id       = Yii::app()->user->getId();
		$sri      = Users::model()
		                 ->findByAttributes( array( 'id' => $id ) )
			->security_roles_id;
//        $store = Pegawai::model()->findByAttributes(array('nik' => $nik))->store;
		$criteria->select             = "DISTINCT pp.pegawai_id,pp.nama_lengkap,pp.email,pp.nik";
		$criteria->alias              = 'pp';
		$criteria->join               = 'LEFT JOIN pbu_users AS pu1 ON pp.last_update_id = pu1.id
            LEFT JOIN pbu_users AS pu2 ON pp.tuser = pu2.id 
            INNER JOIN pbu_sr_cbg_area_bu AS sr ON pp.cabang_id = sr.cabang_id
            INNER JOIN pbu_sr_level_bu as sl ON (pp.leveling_id = sl.leveling_id OR pp.leveling_id IS NULL)
            LEFT JOIN pbu_email_trans AS pet ON pp.pegawai_id = pet.pegawai_id
            LEFT JOIN pbu_email_trans_detail AS petd ON pp.pegawai_id = petd.pegawai_id';
		$criteria->condition          = '(pet.pegawai_id IS NULL AND petd.pegawai_id IS NULL) 
			AND sr.security_roles_id = :security_roles_id AND sl.security_roles_id = :security_roles_id';
		$params[':security_roles_id'] = $sri;
		$criteria->order              = "pp.nik";
		//        if($store != 'ALL'){
//            $criteria->addCondition("store = '$store'");
//        }
//        $criteria
//        $cabang = Cabang::model()->checkCabang();
//        if ($cabang != 'ALL'){
//            $criteria->addCondition("store = '$cabang'");
//        }
		if ( isset( $_POST['bu_id'] ) && $_POST['bu_id'] != null ) {
			$criteria->addCondition( 'sr.bu_id = :bu_id' );
			$params[':bu_id'] = $_POST['bu_id'];
		}
		$criteria->params = $params;
		$total            = count( Pegawai::model()->findAll( $criteria ) );
		if ( ( isset( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) )
		) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$criteria->order  = "pp.nik";
		$criteria->params = $params;
		$model            = Pegawai::model()->findAll( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionSinkron() {
		$html   = "";
		$output = Yii::app()->curl->post( Yii::app()->curl->base_url . 'GetAllPegawai',
			http_build_query( [ 'bu_id' => $_POST['bu_id'] ] ) );
		$decode = json_decode( $output, true );
		foreach ( $decode['results'] as $result ) {
			unset( $result['status_id'] );
			/** @var Pegawai $model */
			$model = Pegawai::model()->findByPk( $result['pegawai_id'] );
			if ( $model == null ) {
				$model = new Pegawai;
				$model->setAttributes( $result );
				$model->leveling_id = $result['lvljabatan_id'];
				$model->golongan_id = $result['lvl2jabatan_id'];
			} else {
				$model->nama_lengkap = $result['nama_lengkap'];
				$model->cabang_id    = $result['cabang_id'];
				$model->nik          = $result['nik'];
			}
//			unset( $result['cabang_id'] );
			if ( ! $model->save() ) {
				$html .= CHtml::errorSummary( $model );
			}
		}
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => $html
		) );
		Yii::app()->end();
	}
}
