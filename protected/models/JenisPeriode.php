<?php
Yii::import('application.models._base.BaseJenisPeriode');
class JenisPeriode extends BaseJenisPeriode
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->jenis_periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->jenis_periode_id = $uuid;
        }
        return parent::beforeValidate();
    }
}