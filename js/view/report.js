jun.ReportAbsen = Ext.extend(Ext.Window, {
    title: "Report Absen",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAbsen",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 100,
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAbsen.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportAbsen").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsen").getForm().url = "Report/Absen";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAbsen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportAbsen").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAbsen").getForm().url = "Report/Absen";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportAbsen').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportMandiri = Ext.extend(Ext.Window, {
    title: "Report Mandiri",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 250,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        jun.rztPeriodeCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportMandiri",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        width: 100,
                        anchor: '100%'
                    },
                    {
                        xtype: "xdatefield",
                        fieldLabel: 'Tgl Proses',
                        ref: "../tgl_proses",
                        name: "tgl_proses",
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        // format: "d M Y",
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Rek. Induk',
                        hideLabel: false,
                        //hidden:true,
                        name: 'rek_induk',
                        ref: '../rek_induk',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Ket Trans 1',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket1',
                        ref: '../ket1',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Ket Trans 2',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket2',
                        ref: '../ket2',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportMandiri.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportMandiri").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMandiri").getForm().url = "Report/Mandiri";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportMandiri').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportFp = Ext.extend(Ext.Window, {
    title: "Report Finger Print",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
//    height: 140,
    height: 180,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFp",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
//                    {
//                        xtype: "label",
//                        text: "Periode:",
//                        x: 5,
//                        y: 5
//                    },
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeLib, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        width: 175,
                        anchor: "100%",
//                        x: 70,
//                        y: 2,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Pegawai',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        allowBlank: true,
                        emptyText: 'ALL',
                        store: jun.rztPegawaiLib,
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        ref: '../pegawai',
                        displayField: 'nik',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFp.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportFp").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFp").getForm().url = "Report/Fp";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportFp").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFp").getForm().url = "Report/Fp";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportFp').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPegawai = Ext.extend(Ext.Window, {
    title: "Report Rekap Pegawai",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 110,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPegawai",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPegawai.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapPegawai").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPegawai").getForm().url = "Report/PrintRekapPegawai";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapPegawai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapPegawai").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPegawai").getForm().url = "Report/PrintRekapPegawai";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapPegawai').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportMasterGaji = Ext.extend(Ext.Window, {
    title: "Master Gaji",
    iconCls: "silk13-report",
    modez: 1,
    width: 300,
    height: 110,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportMasterGaji",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'absolute',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportMasterGaji.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportMasterGaji").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMasterGaji").getForm().url = "Report/PrintMasterGaji";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportMasterGaji').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportMasterGaji").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportMasterGaji").getForm().url = "Report/PrintMasterGaji";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportMasterGaji').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPayroll = Ext.extend(Ext.Window, {
    title: "Rekap Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        jun.rztPeriodeCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPayroll",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Hide HK Null',
                        // boxLabel: "Shift",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "hide_hk"
                    },
                    // {
                    //     xtype: 'combo',
                    //     style: 'margin-bottom:2px',
                    //     typeAhead: true,
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     fieldLabel: 'Periode',
                    //     forceSelection: true,
                    //     store: jun.rztPeriodeCmp, //RUMUS deklarasi store langsung di combo
                    //     hiddenName: 'periode_id',
                    //     valueField: 'periode_id',
                    //     emptyText: 'Pilih Periode',
                    //     ref: '../periode_id',
                    //     displayField: 'kode_periode',
                    //     // width: 175,
                    //     itemSelector: "div.search-item",
                    //     tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                    //         '<div style="width:100%;display:inline-block;">\n\
                    //             <span style="font-weight: bold">{kode_periode}</span>\n\
                    //             <br>\n\{periode_start} - {periode_end}\n\
                    //             </div>',
                    //         "</div></tpl>")
                    // },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPayroll").getForm().url = "Report/PrintRekapPayroll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapPayroll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPayroll").getForm().url = "Report/PrintRekapPayroll";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapPayroll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.LockPayroll = Ext.extend(Ext.Window, {
    title: "Lock Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        jun.rztPeriodeCmp.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-LockPayroll",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    // iconCls: "silk13-page_white_excel",
                    text: "Lock Payroll",
                    ref: "../btnSave"
                }
            ]
        };
        jun.LockPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        // this.btnDisabled(true);
        var urlz = 'Payroll/lock';
        Ext.getCmp('form-LockPayroll').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                // this.btnDisabled(false);
            }
        });
    }
});
jun.ReportRekapAbsensi = Ext.extend(Ext.Window, {
    title: "Rekap Presensi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 150,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCabangUser.getTotalCount() === 0) {
            jun.rztCabangUser.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapAbsensi",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        allowBlank: false,
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
//                        readOnly: true,
                        fieldLabel: 'Store',
                        store: jun.rztCabangUser,
//                        value: 'cabang_id',
                        hiddenName: 'cabang_id',
                        valueField: 'cabang_id',
                        displayField: 'kode_cabang',
                        anchor: '100%',
                        width: 100
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapAbsensi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapAbsensi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapAbsensi").getForm().url = "Report/RekapAbsensi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapAbsensi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportRekapAbsensi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapAbsensi").getForm().url = "Report/RekapAbsensi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-ReportRekapAbsensi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportLaporanBank = Ext.extend(Ext.Window, {
    title: "Report Laporan Bank",
    iconCls: "silk13-report",
    modez: 1,
    width: 450,
    height: 350,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        jun.rztPeriodeCmp.load();
        jun.rztBank.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLaporanBank",
                labelWidth: 100,
                labelAlign: "left",
                layout: 'form',
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bank',
                        store: jun.rztBank,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    new jun.comboTipeBank({
                        id: 'comboJenisBank',
                        fieldLabel: 'Jenis Bank',
                        emptyText: '-  Pilih Jenis Bank  -',
                        ref: '../status',
                        anchor: '100%'
                    }),
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Periode',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztPeriodeCmp,
                        id: 'periodeCmpReportBank',
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        // width: 100,
                        anchor: '100%'
                    },
                    new jun.ReportLaporanBankGrid({
                        x: 5,
                        y: 185,
                        height: 175,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                    }),
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "hidden",
                        name: "detil",
                        ref: "../detil"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportLaporanBank.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.periode_id.on("select", function () {
            jun.rztPayrollDetailsReport.load();
        }, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLaporanBank").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaporanBank").getForm().url = "Report/PrintLaporanBank";
        this.format.setValue("excel");
        this.detil.setValue(Ext.encode(Ext.pluck(
            jun.colLapBank.data.items, "data")));
        var form = Ext.getCmp('form-ReportLaporanBank').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.colLapBank = new Ext.data.ArrayStore({
    fields: ["nama_col"]
});
jun.ReportLaporanBankGrid = Ext.extend(Ext.grid.GridPanel, {
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Schema',
            dataIndex: 'nama_col',
        }
    ],
    initComponent: function () {
        jun.rztPayrollDetailsReport.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var periode = Ext.getCmp('periodeCmpReportBank');
                    b.params.periode_id = periode.getValue();
                }
            }
        });
        this.store = jun.colLapBank;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Schema :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztPayrollDetailsReport,
                    ref: '../bank',
                    hiddenName: 'nama_skema',
                    valueField: 'nama_skema',
                    displayField: 'nama_skema',
                    width: 250
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    // height: 44,
                    ref: '../btnAdd'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    // height: 44,
                    ref: '../btnDelete'
                }
            ]
        };
        jun.ReportLaporanBankGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bank = this.bank.getValue();
        var c = this.store.recordType,
            d = new c({
                nama_col: bank,
            });
        this.store.add(d);
        this.bank.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});