<?php

//Yii::import('application.vendors.*');
//require 'tad/TADFactory.php';
//require 'tad/TAD.php';
//require 'tad/TADResponse.php';
//require 'tad/Providers/TADSoap.php';
//require 'tad/Providers/TADZKLib.php';
//require 'tad/Exceptions/ConnectionError.php';
//require 'tad/Exceptions/FilterArgumentError.php';
//require 'tad/Exceptions/UnrecognizedArgument.php';
//require 'tad/Exceptions/UnrecognizedCommand.php';
//Yii::import('application.components.U');
//Yii::import('application.components.TarikAbsenAll');

class TarikAbsenAll {

    public function AbsensiAll() {
        $fp_id = 1;
        $count = 0;
        $tgl = date('Y-m-d');
        $hariini = date('Y-m-d', strtotime($tgl));
        $ipz = Yii::app()->db->createCommand(
                        "SELECT kode_ip FROM pbu_ip order by cabang")
                ->queryAll();
        foreach ($ipz as $k) {
            $ip = $k['kode_ip'];
            $cabang = Yii::app()->db->createCommand(
                            "SELECT cabang FROM pbu_ip where kode_ip = '$ip'")
                    ->queryScalar();
            $comkey = Ip::model()->findByAttributes(array('kode_ip' => $ip))->com_key;
//        $tgl = get_date_today('yyyy-MM-dd%');
            $options = [
                'ip' => "$ip", // '169.254.0.1' by default (totally useless!!!).
                'internal_id' => $fp_id, // 1 by default.
                'com_key' => $comkey, // 0 by default.
                'description' => 'TAD1', // 'N/A' by default.
                'soap_port' => 80, // 80 by default,
                'udp_port' => 4370, // 4370 by default.
                'encoding' => 'utf-8'    // iso8859-1 by default.
            ];
            $tad_factory = new TADPHP\TADFactory($options);
            $tad = $tad_factory->get_instance();

            try {
                $att_logs = $tad->get_att_log();
            } catch (Exception $ex) {
                echo "Gagal! " . $ex->getMessage() . " cabang $cabang" . PHP_EOL;
                continue;
            }

            $kemarin = date('Y-m-d', strtotime($tgl . "-1 days"));
            $filtered_att_logs = $att_logs->filter_by_date(
                    ['start' => "$kemarin", 'end' => "$kemarin"]
            );
            $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
            if (!isset($result->Row)) {
                echo "Data absen cabang $cabang pada tgl $kemarin tidak ada." . PHP_EOL;
                continue;
            }
            Fp::model()->deleteAll("DateTime_ like '$kemarin%' AND cabang = '$cabang'");
            $multi = new CDbMultiInsertCommand(new Fp());
            $ROW = $result->Row;
            foreach ($ROW as $row) {
                $length = strlen($row->PIN);
                if($length == 5){
                    $pinold = substr($row->PIN, 1,4);
                    $pin = "10$pinold";
                } else {
                    $pin = substr($row->PIN, -6);
                }

                $fp = new Fp();
                $fp->PIN = $pin;
                $fp->PIN_real = $row->PIN;
                $fp->DateTime_ = $row->DateTime;
                $fp->Verified = $row->Verified;
                $fp->Status = $row->Status;
                $fp->WorkCode = $row->WorkCode;
                $fp->terminal_id = $fp_id;
//                $fp->tdate = NOW();
                $fp->tdate = $hariini . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
                $fp->cabang = $cabang;
                $multi->add($fp);
                echo "Menyimpan $pin pada tanggal $row->DateTime dari $cabang" . PHP_EOL;
                $count++;
            }
            if ($multi->getCountModel() > 0) {
                $multi->execute();           
            }
        }
        echo "Total absen yang disimpan $count" . PHP_EOL;
    }
    public function AbsensiPerTgl($cabang,$tglin,$tglout) {
        $fp_id = 1;
        $count = 0;
        $tgl = date('Y-m-d');
        $hariini = date('Y-m-d', strtotime($tgl));
        $ipz = Yii::app()->db->createCommand(
                        "SELECT kode_ip FROM pbu_ip where cabang = '$cabang'")
                ->queryAll();
        
        foreach ($ipz as $k) {
            $ip = $k['kode_ip'];
            $cabang = Yii::app()->db->createCommand(
                            "SELECT cabang FROM pbu_ip where kode_ip = '$ip'")
                    ->queryScalar();
            $comkey = Ip::model()->findByAttributes(array('kode_ip' => $ip))->com_key;
//        $tgl = get_date_today('yyyy-MM-dd%');
            $options = [
                'ip' => "$ip", // '169.254.0.1' by default (totally useless!!!).
                'internal_id' => $fp_id, // 1 by default.
                'com_key' => $comkey, // 0 by default.
                'description' => 'TAD1', // 'N/A' by default.
                'soap_port' => 80, // 80 by default,
                'udp_port' => 4370, // 4370 by default.
                'encoding' => 'utf-8'    // iso8859-1 by default.
            ];
            $tad_factory = new TADPHP\TADFactory($options);
            $tad = $tad_factory->get_instance();

            try {
                $att_logs = $tad->get_att_log();
            } catch (Exception $ex) {
                echo "Gagal! " . $ex->getMessage() . " cabang $cabang" . PHP_EOL;
                continue;
            }

//            $kemarin = date('Y-m-d', strtotime($tgl . "-1 days"));
            $filtered_att_logs = $att_logs->filter_by_date(
                    ['start' => "$tglin", 'end' => "$tglout"]
            );
            $result = json_decode($filtered_att_logs->get_response(['format' => 'json']));
            if (!isset($result->Row)) {
                echo "Data absen cabang $cabang pada tgl $tglin sampai tgl $tglout tidak ada." . PHP_EOL;
                continue;
            }            
            Fp::model()->deleteAll("DateTime_ between '$tglin 00:00:00' AND '$tglout 23:59:59' AND cabang = '$cabang'");
            $multi = new CDbMultiInsertCommand(new Fp());
            $ROW = $result->Row;
            foreach ($ROW as $row) {
                $length = strlen($row->PIN);
                if($length == 5){
                    $pinold = substr($row->PIN, 1,4);
                    $pin = "10$pinold";
                } else {
                    $pin = substr($row->PIN, -6);
                }

                $fp = new Fp();
                $fp->PIN = $pin;
                $fp->PIN_real = $row->PIN;
                $fp->DateTime_ = $row->DateTime;
                $fp->Verified = $row->Verified;
                $fp->Status = $row->Status;
                $fp->WorkCode = $row->WorkCode;
                $fp->terminal_id = $fp_id;
//                $fp->tdate = NOW();
                $fp->tdate = $hariini . ' ' . Yii::app()->dateFormatter->format('HH:mm:ss', time());
                $fp->cabang = $cabang;
                $multi->add($fp);
                echo "Menyimpan $pin pada tanggal $row->DateTime dari $cabang" . PHP_EOL;
                $count++;
            }
            if ($multi->getCountModel() > 0) {
                $multi->execute();           
            }
        }
        echo "Total absen yang disimpan $count" . PHP_EOL;
    }

}
