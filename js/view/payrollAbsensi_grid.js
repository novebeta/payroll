jun.PayrollAbsensiGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "Absensi Payroll",
    id: 'docs-jun.PayrollAbsensiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: false
    },
    // autoExpandColumn: 'pegawai_id',
    // sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    clicksToEdit: 1,
    columns: [
        // {
        //     header: 'Periode',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'periode_id',
        //     width: 100,
        //     renderer: jun.renderPeriode
        // },
        {
            header: 'NIK',
            sortable: true,
            resizable: true,
            dataIndex: 'nik'
        },
        {
            header: 'Nama',
            sortable: true,
            resizable: true,
            dataIndex: 'pegawai_id',
            renderer: jun.renderPegawai
        },
        {
            header: 'HK',
            sortable: true,
            resizable: true,
            dataIndex: 'total_hari_kerja',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Sakit',
            sortable: true,
            resizable: true,
            dataIndex: 'total_sakit',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Jatah Off',
            sortable: true,
            resizable: true,
            dataIndex: 'jatah_off',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Total Off',
            sortable: true,
            resizable: true,
            dataIndex: 'total_off',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Tahunan',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_tahunan',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'LK',
            sortable: true,
            resizable: true,
            dataIndex: 'total_lk',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Nikah',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_menikah',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        // {
        //     header: 'Bersalin',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'total_cuti_bersalin',
        //     align: 'right',
        //     editor: new Ext.form.NumberField({
        //         allowBlank: false,
        //         allowNegative: true,
        //         maxValue: 31
        //     })
        // },
        {
            header: 'Istimewa',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_istimewa',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },
        {
            header: 'Non Aktif',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_non_aktif',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true,
                maxValue: 31
            })
        },

        {
            header: 'Cuti Menikah',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_menikah',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: false,
                maxValue: 31
            })
        },
        {
            header: 'Cuti Bersalin',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_bersalin',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: false,
                maxValue: 31
            })
        },
        {
            header: 'Cuti Istimewa',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_istimewa',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: false,
                maxValue: 31
            })
        },
        {
            header: 'Cuti Non Aktif',
            sortable: true,
            resizable: true,
            dataIndex: 'total_cuti_non_aktif',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: false,
                maxValue: 31
            })
        },
        {
            header: 'Presentasi',
            sortable: true,
            resizable: true,
            dataIndex: 'total_presentasi',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: false,
                maxValue: 31
            })
        },
        {
            header: 'L1',
            sortable: true,
            resizable: true,
            dataIndex: 'total_lembur_1',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true
            })
        },
        {
            header: 'L2',
            sortable: true,
            resizable: true,
            dataIndex: 'total_lembur_next',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true
            })
        },
        {
            header: 'LT',
            sortable: true,
            resizable: true,
            dataIndex: 'less_time',
            align: 'right',
            editor: new Ext.form.NumberField({
                allowBlank: false,
                allowNegative: true
            })
        }
    ],
    initComponent: function () {
        jun.rztPegawaiLib.load();
        jun.rztPayrollAbsensi.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var id = Ext.getCmp('periodeidpayrollabsensigrid').getValue();
                    if (id == '' || id == null || id == undefined) {
                        return false;
                    }
                    b.params.periode_id = id;
                }
            }
        });
        // if (jun.rztPeriodeCmp.getTotalCount() === 0)
        jun.rztPeriodeCmp.load();
        // if (jun.rztPegawaiCmp.getTotalCount() === 0)
        jun.rztPegawaiCmp.load();
        this.store = jun.rztPayrollAbsensi;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Buat',
                //     ref: '../btnAdd',
                //     iconCls: 'silk13-add'
                // },
                {
                    xtype: 'button',
                    text: 'Save Absensi',
                    iconCls: 'silk13-pencil',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Hapus Absensi',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Periode :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    id: 'periodeidpayrollabsensigrid',
                    forceSelection: true,
                    fieldLabel: 'Periode',
                    store: jun.rztPeriodeCmp,
                    ref: '../periode',
                    hiddenName: 'periode_id',
                    valueField: 'periode_id',
                    displayField: 'kode_periode'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tarik Laporan Absen',
                    iconCls: 'silk13-shape_square_go',
                    ref: '../btnSimpanValid'
                },
                {
                    xtype: 'tbseparator'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.PayrollAbsensiGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.periode.on('select', this.onPeriodeLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnSimpanValid.on('click', this.simpanValidOnClick, this);
    },
    //----------------tarik data result---------
    simpanValidOnClick: function () {
        if (!this.periode.getValue()) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Periode!");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin Tarik dan menyimpan semua data result absen?', this.saveValidationConfirm, this);
    },
    saveValidationConfirm: function (btn) {
        var a = jun.rztPayrollAbsensiLib.findBy(
            function (t) {
                return t.get('periode_id') === this.periode.getValue();
            }, this);
        if (btn == 'no') {
            return;
        } else {
            if (a == -1) {
                Ext.MessageBox.confirm('Pertanyaan', 'Sudah terdapat data yang tersimpan. Jika Anda pilih Yes maka akan menimpa <b> SEMUA </b> data yang ada?', this.saveValidation, this);
            } else {
                this.saveValidation('yes');
            }
        }
    },
    saveValidation: function (btn) {
        if (btn == 'no') {
            return;
        } else {
            Ext.Ajax.request({
                url: 'PayrollAbsensi/saveLaporan',
                method: 'POST',
                params: {
                    periode_id: this.periode.getValue()
                },
                success: function (f, a) {
                    jun.rztPayrollAbsensi.reload();
//                    jun.rztValidasi.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    },
    //---------------------------
    onPeriodeLoad: function () {
        this.store.load();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PayrollAbsensiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        // var selectedz = this.sm.getSelected();
        // //var dodol = this.store.getAt(0);
        // if (selectedz == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
        //     return;
        // }
        // var idz = selectedz.json.payroll_absensi_id;
        // var form = new jun.PayrollAbsensiWin({modez: 1, id: idz});
        // form.show(this);
        // form.formz.getForm().loadRecord(this.record);
        this.store.commitChanges();
        Ext.Ajax.request({
            url: 'PayrollAbsensi/create',
            method: 'POST',
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPayrollAbsensi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PayrollAbsensi/delete/id/' + record.json.payroll_absensi_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPayrollAbsensi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
