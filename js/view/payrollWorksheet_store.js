jun.PayrollWorksheetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PayrollWorksheetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PayrollWorksheetStoreId',
            url: 'PayrollWorksheet',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'payroll_worksheet_id'},
                {name: 'periode_id'},
                {name: 'schema_id'},
                {name: 'amount'},
                {name: 'pegawai_id'},
                {name: 'trans_type'},
                {name: 'trans_id'},
                {name: 'tdate'},
                {name: 'tgl'},
                {name: 'user_id'},
            ]
        }, cfg));
    }
});
jun.rztPayrollWorksheet = new jun.PayrollWorksheetstore();
//jun.rztPayrollWorksheet.load();
