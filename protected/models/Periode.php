<?php
Yii::import('application.models._base.BasePeriode');
class Periode extends BasePeriode
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->periode_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->periode_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        return parent::beforeValidate();
    }
    public function getCount()
    {
        $command = $this->dbConnection->createCommand("SELECT DATE_PART('day', periode_end - periode_start) 
        FROM pbu_periode WHERE periode_id = :periode_id;");
        return $command->queryScalar([':periode_id' => $this->periode_id]);
    }
}
