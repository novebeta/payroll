jun.Payrollstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Payrollstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PayrollStoreId',
            url: 'Payroll',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'payroll_id'},
                {name: 'periode_id'},
                {name: 'total_hari_kerja'},
                {name: 'total_lk'},
                {name: 'total_sakit'},
                {name: 'total_cuti_tahunan'},
                {name: 'total_cuti_menikah'},
                {name: 'total_cuti_bersalin'},
                {name: 'total_cuti_istimewa'},
                {name: 'total_cuti_non_aktif'},
                {name: 'total_off'},
                {name: 'total_lembur_1'},
                {name: 'total_lembur_next'},
                {name: 'less_time'},
                {name: 'pegawai_id'},
                {name: 'nik'},
                {name: 'golongan_id'},
                {name: 'leveling_id'},
                {name: 'area_id'},
                {name: 'nama_lengkap'},
                {name: 'email'},
                {name: 'total_income'},
                {name: 'total_deduction'},
                {name: 'take_home_pay'},
                {name: 'transfer'},
                {name: 'nama_jabatan'},
                {name: 'jatah_off'},
                {name: 'tgl_masuk'},
                {name: 'npwp'},
                {name: 'kode_gol'},
                {name: 'kode_level'},
                {name: 'lock'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztPayroll = new jun.Payrollstore();
//jun.rztPayroll.load();
