<h1>Rekap Pegawai</h1>
<?
$this->pageTitle = 'Rekap Pegawai';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Tgl Masuk',
            'name' => 'tgl_masuk'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Nama Lengkap',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'Golongan',
            'name' => 'kode_golongan'
        ),
        array(
            'header' => 'Jabatan',
            'name' => 'nama_jabatan'
        ),
        array(
            'header' => 'Email',
            'name' => 'email'
        ),
        array(
            'header' => 'NPWP',
            'name' => 'npwp'
        ),
        array(
            'header' => 'Status',
            'name' => 'kode_status'
        ),
        array(
            'header' => 'Nama Bank',
            'name' => 'bank_nama'
        ),
        array(
            'header' => 'Kota Bank',
            'name' => 'bank_kota'
        ),
        array(
            'header' => 'Rekening',
            'name' => 'rekening'
        ),
        array(
            'header' => 'Gaji Pokok',
            'name' => 'gp',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Tunjangan',
            'name' => 'tj',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Uang Makan',
            'name' => 'um',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Uang Transport',
            'name' => 'ut',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));
?>