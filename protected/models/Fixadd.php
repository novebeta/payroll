<?php
Yii::import( 'application.models._base.BaseFixadd' );
class Fixadd extends BaseFixadd {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->add_id == null ) {
			$command      = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid         = $command->queryScalar();
			$this->add_id = $uuid;
		}
		return parent::beforeValidate();
	}
}