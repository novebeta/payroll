<?php

Yii::import('application.models._base.BaseBank');

class Bank extends BaseBank
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function beforeValidate() {
		if ($this->bank_id == null) {
			$command = $this->dbConnection->createCommand("SELECT UUID();");
			$uuid = $command->queryScalar();
			$this->bank_id = $uuid;
		}
		return parent::beforeValidate();
	}
}