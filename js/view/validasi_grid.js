jun.ValidasiGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Validasi",
    id: 'docs-jun.ValidasiGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'validasi_id',
            sortable: true,
            resizable: true,
            dataIndex: 'validasi_id',
            width: 100
        },
        {
            header: 'pegawai_id',
            sortable: true,
            resizable: true,
            dataIndex: 'pegawai_id',
            width: 100
        },
        {
            header: 'pin_id',
            sortable: true,
            resizable: true,
            dataIndex: 'pin_id',
            width: 100
        },
        {
            header: 'PIN',
            sortable: true,
            resizable: true,
            dataIndex: 'PIN',
            width: 100
        },
        {
            header: 'in_time',
            sortable: true,
            resizable: true,
            dataIndex: 'in_time',
            width: 100
        },
        {
            header: 'out_time',
            sortable: true,
            resizable: true,
            dataIndex: 'out_time',
            width: 100
        },
        /*
{
header:'min_early_time',
sortable:true,
resizable:true,
dataIndex:'min_early_time',
width:100
},
                {
header:'min_late_time',
sortable:true,
resizable:true,
dataIndex:'min_late_time',
width:100
},
                {
header:'min_least_time',
sortable:true,
resizable:true,
dataIndex:'min_least_time',
width:100
},
                {
header:'min_over_time_awal',
sortable:true,
resizable:true,
dataIndex:'min_over_time_awal',
width:100
},
                {
header:'min_over_time',
sortable:true,
resizable:true,
dataIndex:'min_over_time',
width:100
},
                {
header:'tdate',
sortable:true,
resizable:true,
dataIndex:'tdate',
width:100
},
                {
header:'kode_ket',
sortable:true,
resizable:true,
dataIndex:'kode_ket',
width:100
},
                {
header:'no_surat_tugas',
sortable:true,
resizable:true,
dataIndex:'no_surat_tugas',
width:100
},
                {
header:'status_int',
sortable:true,
resizable:true,
dataIndex:'status_int',
width:100
},
                {
header:'result_id',
sortable:true,
resizable:true,
dataIndex:'result_id',
width:100
},
                {
header:'user_id',
sortable:true,
resizable:true,
dataIndex:'user_id',
width:100
},
                {
header:'status_pegawai_id',
sortable:true,
resizable:true,
dataIndex:'status_pegawai_id',
width:100
},
                {
header:'shift_id',
sortable:true,
resizable:true,
dataIndex:'shift_id',
width:100
},
                {
header:'approval_lembur',
sortable:true,
resizable:true,
dataIndex:'approval_lembur',
width:100
},
        */
    ],
    initComponent: function () {
        this.store = jun.rztValidasi;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ValidasiGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ValidasiWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.validasi_id;
        var form = new jun.ValidasiWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Validasi/delete/id/' + record.json.validasi_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
