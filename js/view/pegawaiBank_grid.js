jun.PegawaiBankGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PegawaiBank",
    id: 'docs-jun.PegawaiBankGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
        {
            header: 'Tipe Bank',
            sortable: true,
            resizable: true,
            dataIndex: 'tipe_bank',
            width: 100
        },
        {
            header: 'No Rekening',
            sortable: true,
            resizable: true,
            dataIndex: 'no_rekening',
            width: 100
        },
        {
            header: 'Fotocopy',
            sortable: true,
            resizable: true,
            dataIndex: 'fotocopy',
            width: 100,
            renderer: function (v, m, r) {
                if(r.get('fotocopy') == 0){
                    return 'Tidak Ada';
                } else {
                    return 'Ada';
                }
            }
        }
    ],
    initComponent: function () {
        this.store = jun.rztPegawaiBank;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Bank :'
                        },
                        {
                            xtype: 'combo',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'Bank',
                            store: jun.rztBank,
                            ref: '../../bank',
                            hiddenName: 'bank_id',
                            valueField: 'bank_id',
                            displayField: 'nama_bank',
                            width: 150
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Rekening :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'no_rekening_id',
                            ref: '../../no_rekening',
                            width: 150,
                            maxLength: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Tipe Bank:'
                        },
                        new jun.comboTipeBank({
                            id: 'comboJenisBank',
                            emptyText: '-  Pilih Jenis Bank  -',
                            ref: '../../status',
                            width: 150
                        }),
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Fotocopy :'
                        },
                        new jun.comboStatusFotocopy({
                            id: 'comboStatusFotocopy',
//                            emptyText: '-  Pilih Jenis Bank  -',
                            ref: '../../statusfotocopy',
                            width: 150
                        })
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PegawaiBankGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var bank = this.bank.getValue();
        var no_rekening = this.no_rekening.getValue();
        var status = this.status.getValue();
        var statusfotocopy = this.statusfotocopy.getValue();

        if (bank == '') {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Bank");
            return;
        }
        if (no_rekening == '') {
            Ext.MessageBox.alert("Warning", "Anda belum mengisi Rekening");
            return;
        }
        if (status == '') {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Tipe Bank");
            return;
        }
        if (statusfotocopy == '') {
            statusfotocopy = 0;
        }

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('bank_id', bank);
            record.set('no_rekening', no_rekening);
            record.set('tipe_bank', status);
            record.set('fotocopy', statusfotocopy);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    bank_id: bank,
                    no_rekening: no_rekening,
                    tipe_bank: status,
                    fotocopy: statusfotocopy
                });
            this.store.add(d);
        }
//        clearText();
        this.bank.reset();
        this.no_rekening.reset();
        this.status.reset();
        this.statusfotocopy.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.bank.setValue(record.data.bank_id);
            this.no_rekening.setValue(record.data.no_rekening);
            this.status.setValue(record.data.tipe_bank);
            this.statusfotocopy.setValue(record.data.fotocopy);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
