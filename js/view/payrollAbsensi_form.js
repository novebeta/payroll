jun.PayrollAbsensiWin = Ext.extend(Ext.Window, {
    title: 'Absensi Payroll',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PayrollAbsensi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Pegawai',
                        store: jun.rztPegawaiCmp,
                        hiddenName: 'pegawai_id',
                        valueField: 'pegawai_id',
                        displayField: 'nik',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Hari Kerja',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_hari_kerja',
                        id: 'total_hari_kerjaid',
                        ref: '../total_hari_kerja',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Luar Kota',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_lk',
                        id: 'total_lkid',
                        ref: '../total_lk',
                        maxLength: 4,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Sakit',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_sakit',
                        id: 'total_sakitid',
                        ref: '../total_sakit',
                        maxLength: 4,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Cuti Tahunan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_cuti_tahunan',
                        id: 'total_cuti_tahunanid',
                        ref: '../total_cuti_tahunan',
                        maxLength: 4,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Off',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total_off',
                        id: 'total_offid',
                        ref: '../total_off',
                        maxLength: 4,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PayrollAbsensiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'PayrollAbsensi/update/id/' + this.id;
        } else {
            urlz = 'PayrollAbsensi/create/';
        }
        Ext.getCmp('form-PayrollAbsensi').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPayrollAbsensi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PayrollAbsensi').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});