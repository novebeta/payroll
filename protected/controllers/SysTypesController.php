<?php
Yii::import('application.components.Reference');
class SysTypesController extends GxController
{
    public function actionReset()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $ref = new Reference();
            $docref = $ref->get_next_reference($_POST['SysTypes']);
            $ref->save_reset($_POST['SysTypes'], $docref);
            echo CJSON::encode(array(
                'success' => true,
                'msg' => t('save.success', 'app')));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SysTypes');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SysTypes'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SysTypes'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sys_types_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sys_types_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SysTypes')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = SysTypes::model()->findAll($criteria);
        $total = SysTypes::model()->count($criteria);
        $argh = array();
        foreach ($model AS $dodol) {
            global $systypes_array;
            $arr = $dodol->getAttributes();
            $arr['ket'] = $systypes_array[$arr['type_id']];
            $argh[] = $arr;
        };
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
        Yii::app()->end($jsonresult);
    }
}