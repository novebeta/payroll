jun.AddValuestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AddValuestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AddValueStoreId',
            url: 'AddValue',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'add_value_id'},
                {name: 'add_id'},
                {name: 'pegawai_id'},
                {name: 'amount'},
                {name: 'schema_id'}
            ]
        }, cfg));
    }
});
jun.rztAddValue = new jun.AddValuestore();
//jun.rztAddValue.load();
