<h1>Laporan Absen</h1>
<h3>PERIODE : <?=$kode_periode?></h3>
<?
$this->pageTitle = 'Laporan Absen';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType' => 'nested',
    'columns' => array(
        array(
            'header' => 'Nama',
            'name' => 'nama_lengkap'
        ),
        array(
            'header' => 'NIK',
            'name' => 'nik'
        ),
        array(
            'header' => 'Jabatan',
            'name' => 'kode',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Hari Kerja',
            'name' => 'HK',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total LK',
            'name' => 'LK',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Sakit',
            'name' => 'S',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),        
        array(
            'header' => 'Total Off',
            'name' => 'OFF',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Tahunan',
            'name' => 'CT',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Menikah',
            'name' => 'CM',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Bersalin',
            'name' => 'CB',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Istimewa',
            'name' => 'CI',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Total Cuti Non Aktif',
            'name' => 'CNA',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),        
        array(
            'header' => 'Lembur Awal',
            'name' => 'real_lembur_awal',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Lembur 1 Jam Pertama',
            'name' => 'real_lembur_pertama',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        ),
        array(
            'header' => 'Lembur Selanjutnya',
            'name' => 'real_lembur_akhir',
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )        
    )
));
?>