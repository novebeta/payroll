<?php
class HistorySinkronController extends GxController {
	public function actionCreate() {
		$model = new HistorySinkron;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['HistorySinkron'][ $k ] = $v;
			}
			$model->attributes = $_POST['HistorySinkron'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->history_sinkron_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'HistorySinkron' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['HistorySinkron'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['HistorySinkron'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->history_sinkron_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->history_sinkron_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'HistorySinkron' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['model'] ) ) {
			$criteria->addCondition( 'model_ = :model' );
			$criteria->params[':model'] = $_POST['model'];
		}
		$model = HistorySinkron::model()->findAll( $criteria );
		$total = HistorySinkron::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
	public function actionSinkron() {
		$html          = "";
		$_POST['host'] = 'http://' . $_POST['host'];
//		Yii::app()->curl->addHeader( [
//			'Authorization' => $_POST['auth']
//		] );
		$client = new \GuzzleHttp\Client();
//		try {
//			$response = $client->request( 'POST',
//				'http://localhost:81/ullenmember/web/index.php',  [
//					'query'       => [
//						'r' => 'api/member/login'
//					],
//					'form_params' => [
//						'email'    => $this->email,
//						'password' => $this->password
//					]
//				] );
//		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
//			Yii::log( $e->getMessage(), 'error', 'system.web.rest' );
//			return false;
//		}
//		$jtoken = $response->getBody();
//		$token  = CJSON::decode( $jtoken );
//		$this->access_token =  $token['access_token'];
//		if(!$this->save()){
//			Yii::log( CHtml::errorSummary($this), 'error', 'system.web.user' );
//			return false;
//		}
//		return true;
		try {
			switch ( $_POST['model'] ) {
				case 'bu':
//					$output = Yii::app()->curl->get( $_POST['host'] . '/GetAllBu' );
//					$decode = json_decode( $output, true );
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllBu', [
							'headers' => [
								'Authorization' => $_POST['auth']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ( $decode['results'] as $result ) {
						$bu = Bu::model()->findByPk( $result['bu_id'] );
						if ( $bu == null ) {
							$bu = new Bu;
							$insert ++;
						} else {
							$update ++;
						}
						if ( $result['tgl_berdiri'] == '0000-00-00' ) {
							unset( $result['tgl_berdiri'] );
						}
						$bu->setAttributes( $result );
						if ( ! $bu->save() ) {
							$html .= CHtml::errorSummary( $bu );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'bu';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'validasi':
					$periode = Periode::model()->findByPk( $_POST['periode_id'] );
					if ( $periode == null ) {
						$success = false;
						$msg     = 'Periode tidak ditemukan';
					}
					$dStart = new DateTime( $periode->periode_start );
					$dEnd   = new DateTime( $periode->periode_end );
//					$output = Yii::app()->curl->post( $_POST['host'] . '/api/getallvalidasi', http_build_query( [
//						'bu_id'  => $_POST['bu_id'],
//						'tglin'  => $dStart->format( 'Y-m-d' ),
//						'tglout' => $dEnd->format( 'Y-m-d' )
//					] ) );
//					$decode = json_decode( $output, true );
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/getallvalidasi', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id'  => $_POST['bu_id'],
								'tglin'  => $dStart->format( 'Y-m-d' ),
								'tglout' => $dEnd->format( 'Y-m-d' )
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					if ( ! isset( $decode['results'] ) ) {
						$html = "decode not set";
						break;
					}
					if ( ! is_array( $decode['results'] ) ) {
						$html = "decode not arrays";
						break;
					}
					foreach ( $decode['results'] as $result ) {
						$bu = Validasi::model()->findByPk( $result['validasi_id'] );
						if ( $bu == null ) {
							$bu = new Validasi;
							$insert ++;
						} else {
							$update ++;
						}
						$bu->setAttributes( $result );
						$bu->real_lembur_pertama = $result['L1'];
						$bu->real_lembur_akhir   = $result['L2'];
						$bu->real_lembur_hari    = $result['LH'];
						$bu->real_less_time      = $result['LT'];
						if ( ! $bu->save() ) {
							$html .= CHtml::errorSummary( $bu );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'validasi';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'pegawai':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllPegawai', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					if ( ! isset( $decode['results'] ) ) {
						$html = "decode not set";
						break;
					}
					if ( ! is_array( $decode['results'] ) ) {
						$html = "decode not arrays";
						break;
					}
					foreach ( $decode['results'] as $result ) {
//						unset( $result['status_id'] );
						if ( $result['birth_date'] == '0000-00-00' ) {
							unset( $result['birth_date'] );
						}
						/** @var Pegawai $model */
						$model = Pegawai::model()->findByPk( $result['pegawai_id'] );
						if ( $model == null ) {
							$model = new Pegawai;
							$model->setAttributes( $result );
							$model->leveling_id = $result['lvljabatan_id'];
							$model->golongan_id = $result['lvl2jabatan_id'];
							$insert ++;
						} else {
//							unset( $result['lvljabatan_id'] );
//							unset( $result['lvl2jabatan_id'] );
							$model->setAttributes( $result );
							$update ++;
						}
						$model->status_pegawai_id = $result['ptkp_id'];
						$model->status_id = $result['status_pegawai_id'];
						if ( ! $model->save() ) {
							$html .= CHtml::errorSummary( $model );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'pegawai';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'pegawaiNon':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllPegawai', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					if ( ! isset( $decode['results'] ) ) {
						$html = "decode not set";
						break;
					}
					if ( ! is_array( $decode['results'] ) ) {
						$html = "decode not arrays";
						break;
					}
					foreach ( $decode['results'] as $result ) {
//						unset( $result['status_id'] );
						if ( $result['birth_date'] == '0000-00-00' ) {
							unset( $result['birth_date'] );
						}
						/** @var Pegawai $model */
						$model = Pegawai::model()->findByPk( $result['pegawai_id'] );
						if ( $model == null ) {
							$model = new Pegawai;
							$model->setAttributes( $result );
							$model->leveling_id = $result['lvljabatan_id'];
							$model->golongan_id = $result['lvl2jabatan_id'];
							$insert ++;
						} else {
							unset( $result['lvljabatan_id'] );
							unset( $result['lvl2jabatan_id'] );
							$model->setAttributes( $result );
							$update ++;
						}
						$model->status_pegawai_id = $result['ptkp_id'];
//						$model->status_id = $result['status_pegawai_id'];
						if ( ! $model->save() ) {
							$html .= CHtml::errorSummary( $model );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'pegawai';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'cabang':
//					$output = Yii::app()->curl->post( $_POST['host'] . '/api/GetAllCabang', [
//						'bu_id' => $_POST['bu_id']
//					] );
//					$decode = json_decode( $output, true );
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllCabang', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ( $decode['results'] as $result ) {
						$model = Cabang::model()->findByPk( $result['cabang_id'] );
						if ( $model == null ) {
							$model = new Cabang;
							$insert ++;
						} else {
							$update ++;
						}
						$model->setAttributes( $result );
						if ( ! $model->save() ) {
							$html .= CHtml::errorSummary( $model );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'cabang';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'area':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllArea', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ( $decode['results'] as $result ) {
						$model = Area::model()->findByPk( $result['area_id'] );
						if ( $model == null ) {
							$model = new Area;
							$insert ++;
						} else {
							$update ++;
						}
						$model->setAttributes( $result );
						if ( ! $model->save() ) {
							$html .= CHtml::errorSummary( $model );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'area';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'leveling':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllLeveling', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ( $decode['results'] as $result ) {
						$model = Leveling::model()->findByPk($result['lvljabatan_id']);
						if ($model == null) {
							$model = new Leveling;
							$model->leveling_id = $result['lvljabatan_id'];
							$insert ++;
						} else {
							$update ++;
						}
						$model->bu_id = $result['bu_id'];
						$model->kode  = $result['urutan_romawi'];
						$model->nama  = $result['nama_level'];
//            $model->setAttributes($result);
						if (!$model->save()) {
							$html .= CHtml::errorSummary($model);
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'leveling';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'golongan':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllGolongan', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ( $decode['results'] as $result ) {
						$model = Golongan::model()->findByPk( $result['lvl2jabatan_id'] );
						if ( $model == null ) {
							$model              = new Golongan;
							$model->golongan_id = $result['lvl2jabatan_id'];
							$insert ++;
						} else {
							$update ++;
						}
						$model->bu_id = $result['bu_id'];
						$model->kode  = $result['urutan_alps'];
						$model->nama  = $result['nama'];
//            $model->setAttributes($result);
						if ( ! $model->save() ) {
							$html .= CHtml::errorSummary( $model );
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'golongan';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'jabatan':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllJabatan', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							],
							'form_params' => [
								'bu_id' => $_POST['bu_id']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ($decode['results'] as $result) {
						if ($result['nama_jabatan'] == null) {
							continue;
						}
						$model = Jabatan::model()->findByPk($result['jabatan_id']);
						if ($model == null) {
							$model = new Jabatan;
							$insert ++;
						} else {
							$update ++;
						}
						$model->setAttributes($result);
						if (!$model->save()) {
							$html .= CHtml::errorSummary($model);
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'jabatan';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'statusPegawai':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllPTKP', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ($decode['results'] as $result) {
						$model = StatusPegawai::model()->findByPk($result['ptkp_id']);
						if ($model == null) {
							$model = new StatusPegawai;
							$insert ++;
						} else {
							$update ++;
						}
						$model->setAttributes($result);
						$model->status_pegawai_id = $result['ptkp_id'];
						$model->kode = $result['ptkp_kode'];
						$model->nama_status = $result['ptkp_nama'];
						if (!$model->save()) {
							$html .= CHtml::errorSummary($model);
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'statusPegawai';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
				case 'status':
					$response = $client->request( 'POST',
						$_POST['host'] . '/api/GetAllStatusPegawai', [
							'headers'     => [
								'Authorization' => $_POST['auth']
							]
						] );
					$output   = $response->getBody();
					$decode   = CJSON::decode( $output, true );
					$insert   = $update = 0;
					foreach ($decode['results'] as $result) {
						$model = Status::model()->findByPk($result['status_pegawai_id']);
						if ($model == null) {
							$model = new Status;
							$insert ++;
						} else {
							$update ++;
						}
						$model->setAttributes($result);
						$model->status_id = $result['status_pegawai_id'];
						$model->kode = $result['status_pegawai_kode'];
						$model->nama = $result['status_pegawai_nama'];
						if (!$model->save()) {
							$html .= CHtml::errorSummary($model);
						}
					}
					$hS          = new HistorySinkron;
					$hS->update_ = $update;
					$hS->insert_ = $insert;
					$hS->model_  = 'status';
					$hS->note_   = $html;
					$hS->status  = $html == '' ? 'S' : 'E';
					$hS->save();
					break;
			}
		} catch ( Exception $e ) {
			$html .= $e->getMessage();
		} catch ( \GuzzleHttp\Exception\GuzzleException $e ) {
			$html .= $e->getMessage();
		}
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => $html
		) );
		Yii::app()->end();
	}
}