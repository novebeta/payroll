<?php
class EmailTransHistoryController extends GxController {
	public function actionCreate() {
		$model = new EmailTransHistory;
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['EmailTransHistory'][ $k ] = $v;
			}
			$model->attributes = $_POST['EmailTransHistory'];
			$msg               = "Data gagal disimpan.";
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->email_trans_history_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionSendAll() {
//		$details                 = CJSON::decode( $_POST['details'], true );
		$uuid                    = $this->generate_uuid();
		$model                   = new EmailSession;
		$model->email_session_id = $uuid;
		$model->param            = $_POST['details'];
		if ( $model->save() ) {
			$status = true;
			$msg    = "Slip gaji mulai dikirim.";
			/** @var Users $user */
			$user_id = Yii::app()->user->id;
			$user    = Users::model()->findByPk( $user_id );
			/** @var Email $email */
			$email = Email::model()->findByPk( $user->email_id );
			if ( $email == null ) {
				$msg    = "Email sender tidak ditemukan.";
				$status = false;
			} else {
				console( 'email',
					( $email->mode_ == 0 ) ? 'SendAll' : 'SendAllGoogle',
					' --periode_id="' . $_POST['periode_id'] .
					'" --email_id="' . $user->email_id .
					'" --email_session_id="' . $uuid . '"'
					, $_POST['periode_id'] );
			}
		} else {
			$msg    = "Error email session " . implode( ", ", $model->getErrors() );
			$status = false;
		}
//		$dArr = array_column($details,'email_trans_id');
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
		Yii::app()->end();
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'EmailTransHistory' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['EmailTransHistory'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['EmailTransHistory'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->email_trans_history_id;
			} else {
				$msg    .= " " . implode( ", ", $model->getErrors() );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->email_trans_history_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'EmailTransHistory' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) ) ) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		$model = EmailTransHistory::model()->findAll( $criteria );
		$total = EmailTransHistory::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}