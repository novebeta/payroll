<?php

/**
 * This is the model base class for the table "{{payroll_absensi}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PayrollAbsensi".
 *
 * Columns in table "{{payroll_absensi}}" available as properties of the model,
 * followed by relations of table "{{payroll_absensi}}" available as properties of the model.
 *
 * @property string $payroll_absensi_id
 * @property string $periode_id
 * @property string $pegawai_id
 * @property double $total_hari_kerja
 * @property integer $locked
 * @property string $tdate
 * @property integer $total_lk
 * @property integer $total_sakit
 * @property integer $total_cuti_tahunan
 * @property integer $total_off
 * @property integer $total_cuti_menikah
 * @property integer $total_cuti_bersalin
 * @property integer $total_cuti_istimewa
 * @property integer $total_cuti_non_aktif
 * @property integer $total_lembur_1
 * @property integer $total_lembur_next
 * @property integer $jatah_off
 * @property integer $less_time
 * @property integer $total_presentasi
 *
 * @property Periode $periode
 * @property Pegawai $pegawai
 */
abstract class BasePayrollAbsensi extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{payroll_absensi}}';
	}

	public static function representingColumn() {
		return 'tdate';
	}

	public function rules() {
		return array(
			array('payroll_absensi_id, periode_id, pegawai_id, tdate', 'required'),
			array('locked, total_lk, total_sakit, total_cuti_tahunan, total_off, total_cuti_menikah, total_cuti_bersalin, total_cuti_istimewa, total_cuti_non_aktif, total_lembur_1, total_lembur_next, jatah_off, less_time, total_presentasi', 'numerical', 'integerOnly'=>true),
			array('total_hari_kerja', 'numerical'),
			array('payroll_absensi_id, periode_id, pegawai_id', 'length', 'max'=>36),
			array('total_hari_kerja, locked, total_lk, total_sakit, total_cuti_tahunan, total_off, total_cuti_menikah, total_cuti_bersalin, total_cuti_istimewa, total_cuti_non_aktif, total_lembur_1, total_lembur_next, jatah_off, less_time, total_presentasi', 'default', 'setOnEmpty' => true, 'value' => null),
			array('payroll_absensi_id, periode_id, pegawai_id, total_hari_kerja, locked, tdate, total_lk, total_sakit, total_cuti_tahunan, total_off, total_cuti_menikah, total_cuti_bersalin, total_cuti_istimewa, total_cuti_non_aktif, total_lembur_1, total_lembur_next, jatah_off, less_time, total_presentasi', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'periode' => array(self::BELONGS_TO, 'Periode', 'periode_id'),
			'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'pegawai_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'payroll_absensi_id' => Yii::t('app', 'Payroll Absensi'),
			'periode_id' => Yii::t('app', 'Periode'),
			'pegawai_id' => Yii::t('app', 'Pegawai'),
			'total_hari_kerja' => Yii::t('app', 'Total Hari Kerja'),
			'locked' => Yii::t('app', 'Locked'),
			'tdate' => Yii::t('app', 'Tdate'),
			'total_lk' => Yii::t('app', 'Total Lk'),
			'total_sakit' => Yii::t('app', 'Total Sakit'),
			'total_cuti_tahunan' => Yii::t('app', 'Total Cuti Tahunan'),
			'total_off' => Yii::t('app', 'Total Off'),
			'total_cuti_menikah' => Yii::t('app', 'Total Cuti Menikah'),
			'total_cuti_bersalin' => Yii::t('app', 'Total Cuti Bersalin'),
			'total_cuti_istimewa' => Yii::t('app', 'Total Cuti Istimewa'),
			'total_cuti_non_aktif' => Yii::t('app', 'Total Cuti Non Aktif'),
			'total_lembur_1' => Yii::t('app', 'Total Lembur 1'),
			'total_lembur_next' => Yii::t('app', 'Total Lembur Next'),
			'jatah_off' => Yii::t('app', 'Jatah Off'),
			'less_time' => Yii::t('app', 'Less Time'),
			'total_presentasi' => Yii::t('app', 'Total Presentasi'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('payroll_absensi_id', $this->payroll_absensi_id, true);
		$criteria->compare('periode_id', $this->periode_id);
		$criteria->compare('pegawai_id', $this->pegawai_id);
		$criteria->compare('total_hari_kerja', $this->total_hari_kerja);
		$criteria->compare('locked', $this->locked);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('total_lk', $this->total_lk);
		$criteria->compare('total_sakit', $this->total_sakit);
		$criteria->compare('total_cuti_tahunan', $this->total_cuti_tahunan);
		$criteria->compare('total_off', $this->total_off);
		$criteria->compare('total_cuti_menikah', $this->total_cuti_menikah);
		$criteria->compare('total_cuti_bersalin', $this->total_cuti_bersalin);
		$criteria->compare('total_cuti_istimewa', $this->total_cuti_istimewa);
		$criteria->compare('total_cuti_non_aktif', $this->total_cuti_non_aktif);
		$criteria->compare('total_lembur_1', $this->total_lembur_1);
		$criteria->compare('total_lembur_next', $this->total_lembur_next);
		$criteria->compare('jatah_off', $this->jatah_off);
		$criteria->compare('less_time', $this->less_time);
		$criteria->compare('total_presentasi', $this->total_presentasi);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}