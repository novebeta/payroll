<?php
Yii::import( 'application.models._base.BaseHistorySinkron' );
class HistorySinkron extends BaseHistorySinkron {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->history_sinkron_id == null ) {
			$command                  = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid                     = $command->queryScalar();
			$this->history_sinkron_id = $uuid;
			if ( $this->tdate == null ) {
				$this->tdate = new CDbExpression( 'NOW()' );
			}
			if ( $this->user_id == null ) {
				$this->user_id = Yii::app()->user->getId();
			}
		}
		return parent::beforeValidate();
	}
}