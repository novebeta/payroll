jun.JenisPeriodestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.JenisPeriodestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JenisPeriodeStoreId',
            url: 'JenisPeriode',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jenis_periode_id'},
                {name: 'bu_id'},
                {name: 'nama_jenis'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztJenisPeriode = new jun.JenisPeriodestore();
jun.rztJenisPeriodeCmp = new jun.JenisPeriodestore();
jun.rztJenisPeriodeLib = new jun.JenisPeriodestore();
//jun.rztJenisPeriode.load();
