jun.SchemaGajistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SchemaGajistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SchemaGajiStoreId',
            url: 'SchemaGaji',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'nama_skema'},
                {name: 'type_'},
                {name: 'seq'},
                {name: 'kode'},
                {name: 'nama'},
                {name: 'formula'},
                {name: 'schema_gaji_id'},
                {name: 'jenis_periode_id'},
                {name: 'status_id'},
                {name: 'schema_id'},
                {name: 'rotation'},
                {name: 'paycode'},
                {name: 'bu_id'}
            ]
        }, cfg));
        this.on('beforeload', this.onloadData, this);
    },
    onloadData: function (a, b) {
        if (jun.bu_id == '') {
            return false;
        }
        b.params.bu_id = jun.bu_id;
    }
});
jun.rztSchemaGaji = new jun.SchemaGajistore();
//jun.rztSchemaGaji.load();
