<h1>Laporan Presensi Karyawan</h1>
<h3>Periode : <?= $kode_periode ?></h3>
<h3>From : <?= $from ?></h3>
<h3>To : <?= $to ?></h3>
<h3>Cabang : <?= $kode_cabang ?></h3>
<?php
$this->pageTitle = 'Rekap Absensi';
$gridColums = array();
$rec = $dp->rawData;
array_push($gridColums,array(
    'header' => 'NIK',
    'name' => 'nik')
);        
array_push($gridColums,array(
    'header' => 'Nama',
    'name' => 'nama_lengkap')
);        
foreach($tgl as $s=>$t){
    array_push($gridColums,
        array(
            'header' => $t['tgl'],
            'name' => $t['tgl']
        )
    );
}
array_push($gridColums,array(
    'header' => 'HK',
    'name' => 'cmasuk')
);
array_push($gridColums,array(
    'header' => 'Off',
    'name' => 'coff')
);
array_push($gridColums,array(
    'header' => 'LK',
    'name' => 'clk')
);
array_push($gridColums,array(
    'header' => 'Sakit',
    'name' => 'csakit')
);
array_push($gridColums,array(
    'header' => 'Cuti',
    'name' => 'ccuti')
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));