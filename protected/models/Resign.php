<?php
Yii::import( 'application.models._base.BaseResign' );
class Resign extends BaseResign {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->resign_id == null ) {
			$command         = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid            = $command->queryScalar();
			$this->resign_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->id;
		}
		return parent::beforeValidate();
	}
}