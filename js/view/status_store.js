jun.Statusstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Statusstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StatusStoreId',
            url: 'Status',
            root: 'results',
            totalProperty: 'total',
            autoLoad: true,
            fields: [
                {name: 'status_id'},
                {name: 'kode'},
                {name: 'nama'}
            ]
        }, cfg));
        // this.on('beforeload', this.onloadData, this);
    },
    // onloadData: function (a, b) {
    //     if (jun.bu_id == '') {
    //         return false;
    //     }
    //     b.params.bu_id = jun.bu_id;
    // }
});
jun.rztStatus = new jun.Statusstore();
jun.rztStatusLib = new jun.Statusstore();
jun.rztStatusCmp = new jun.Statusstore();
jun.rztStatusLib.load();
