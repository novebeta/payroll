jun.Emailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Emailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmailStoreId',
            url: 'Email',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'email_id'},
                {name: 'name_'},
                {name: 'host_'},
                {name: 'smtp'},
                {name: 'smtp_auth'},
                {name: 'username_'},
                {name: 'passw_'},
                {name: 'smtp_secure'},
                {name: 'port_'},
                {name: 'user_id'},
                {name: 'credentials_'},
                {name: 'token_'},
                {name: 'mode_'},
            ]
        }, cfg));
    }
});
jun.rztEmail = new jun.Emailstore();
//jun.rztEmail.load();
