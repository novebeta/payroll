jun.ValidasiWin = Ext.extend(Ext.Window, {
    title: 'Validasi',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Validasi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'pegawai_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pegawai_id',
                        id: 'pegawai_idid',
                        ref: '../pegawai_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'pin_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pin_id',
                        id: 'pin_idid',
                        ref: '../pin_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'PIN',
                        hideLabel: false,
                        //hidden:true,
                        name: 'PIN',
                        id: 'PINid',
                        ref: '../PIN',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../in_time',
                        fieldLabel: 'in_time',
                        name: 'in_time',
                        id: 'in_timeid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../out_time',
                        fieldLabel: 'out_time',
                        name: 'out_time',
                        id: 'out_timeid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'min_early_time',
                        hideLabel: false,
                        //hidden:true,
                        name: 'min_early_time',
                        id: 'min_early_timeid',
                        ref: '../min_early_time',
                        maxLength: 4,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'min_late_time',
                        hideLabel: false,
                        //hidden:true,
                        name: 'min_late_time',
                        id: 'min_late_timeid',
                        ref: '../min_late_time',
                        maxLength: 4,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'min_least_time',
                        hideLabel: false,
                        //hidden:true,
                        name: 'min_least_time',
                        id: 'min_least_timeid',
                        ref: '../min_least_time',
                        maxLength: 4,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'min_over_time_awal',
                        hideLabel: false,
                        //hidden:true,
                        name: 'min_over_time_awal',
                        id: 'min_over_time_awalid',
                        ref: '../min_over_time_awal',
                        maxLength: 4,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'min_over_time',
                        hideLabel: false,
                        //hidden:true,
                        name: 'min_over_time',
                        id: 'min_over_timeid',
                        ref: '../min_over_time',
                        maxLength: 4,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tdate',
                        fieldLabel: 'tdate',
                        name: 'tdate',
                        id: 'tdateid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'kode_ket',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_ket',
                        id: 'kode_ketid',
                        ref: '../kode_ket',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'no_surat_tugas',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_surat_tugas',
                        id: 'no_surat_tugasid',
                        ref: '../no_surat_tugas',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'status_int',
                        hideLabel: false,
                        //hidden:true,
                        name: 'status_int',
                        id: 'status_intid',
                        ref: '../status_int',
                        maxLength: 3,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'result_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'result_id',
                        id: 'result_idid',
                        ref: '../result_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'user_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'user_id',
                        id: 'user_idid',
                        ref: '../user_id',
                        maxLength: 36,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'status_pegawai_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'status_pegawai_id',
                        id: 'status_pegawai_idid',
                        ref: '../status_pegawai_id',
                        maxLength: 36,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'shift_id',
                        hideLabel: false,
                        //hidden:true,
                        name: 'shift_id',
                        id: 'shift_idid',
                        ref: '../shift_id',
                        maxLength: 36,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'approval_lembur',
                        hideLabel: false,
                        //hidden:true,
                        name: 'approval_lembur',
                        id: 'approval_lemburid',
                        ref: '../approval_lembur',
                        maxLength: 3,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ValidasiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Validasi/update/id/' + this.id;
        } else {
            urlz = 'Validasi/create/';
        }
        Ext.getCmp('form-Validasi').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztValidasi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Validasi').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});