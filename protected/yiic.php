<?php
require_once(dirname(__FILE__) . '/extensions/vendor/autoload.php');

// change the following paths if necessary
$yiic=dirname(__FILE__).'/../../yii/yiic.php';
$config=dirname(__FILE__).'/config/console.php';
$yiiG = dirname(__FILE__) . '/globals.php';
require_once($yiiG);
require_once($yiic);