jun.PayrollWorksheetGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Payroll Worksheet",
    id: 'docs-jun.PayrollWorksheetGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Paycode',
            sortable: true,
            resizable: true,
            dataIndex: 'schema_id',
            renderer: jun.renderSchemaPaycode
            // width: 100
        },
        {
            header: 'Trans',
            sortable: true,
            resizable: true,
            dataIndex: 'trans_type',
            // width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            // width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Creator',
            sortable: true,
            resizable: true,
            dataIndex: 'user_id',
            renderer: jun.renderUser
        },
        {
            header: 'Timestamp',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            // width: 100
        },
    ],
    initComponent: function () {
        jun.rztPegawaiCmp.load();
        jun.rztSchemaCmp.load();
        jun.rztSchemaLib.load();
        jun.rztPeriodeCmp.load();
        jun.rztPeriodeLib.load();
        jun.rztPayrollWorksheet.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var periode = Ext.getCmp('payroll_worksheet_periode_id');
                    var pegawai = Ext.getCmp('payroll_worksheet_pegawai_id');
                    b.params.pegawai_id = pegawai.getValue();
                    b.params.periode_id = periode.getValue();
                    if (b.params.pegawai_id == '' || b.params.periode_id == '') {
                        return false;
                    }
                }
            }
        });
        this.store = jun.rztPayrollWorksheet;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.tbar = {
            xtype: 'toolbar',
            items: [{
                xtype: 'panel',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                height: 50,
                width: 700,
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {flex: 1},
                border: false,
                // tbar: [],
                items: [
                    {
                        xtype: 'toolbar',
                        items: [
                            'Pegawai :',
                            {
                                xtype: 'mfcombobox',
                                searchFields: [
                                    'nik',
                                    'nama_lengkap'
                                ],
                                itemSelector: 'tr.search-item',
                                tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox"><thead><tr style="background:#eeeeee;">',
                                    '<th>NIK</th><th>Nama Lengkap</th></tr></thead>',
                                    '<tbody><tpl for="."><tr class="search-item">',
                                    '<td>{nik}</td><td>{nama_lengkap}</td>',
                                    '</tr></tpl></tbody></table>'),
                                listWidth: 400,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztPegawaiCmp,
                                hiddenName: 'pegawai_id',
                                valueField: 'pegawai_id',
                                displayField: 'nik',
                                anchor: '100%',
                                id: 'payroll_worksheet_pegawai_id',
                                ref: '../../../pegawai'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            'Periode :',
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztPeriodeCmp,
                                hiddenName: 'periode_id',
                                valueField: 'periode_id',
                                displayField: 'kode_periode',
                                anchor: '100%',
                                id: 'payroll_worksheet_periode_id',
                                ref: '../../../periode'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                text: 'Reload',
                                ref: '../../../btnReload'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                text: 'ReGenerate Worksheet',
                                ref: '../../../btnReGenerate'
                            }
                        ]
                    },
                    {
                        xtype: 'toolbar',
                        items: [
                            {
                                xtype: 'button',
                                text: 'Generate Worksheet',
                                ref: '../../../btnGenerate'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                text: 'Add Record',
                                ref: '../../../btnAdd'
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            // {
                            //     xtype: 'button',
                            //     text: 'Edit Record',
                            //     ref: '../../../btnEdit'
                            // },
                            // {
                            //     xtype: 'tbseparator'
                            // },
                            // {
                            //     xtype: 'button',
                            //     text: 'Delete Record',
                            //     ref: '../../../btnDelete'
                            // },
                            // {
                            //     xtype: 'tbseparator'
                            // },
                            {
                                xtype: 'button',
                                text: 'Download Template Gaji',
                                ref: '../../../btnDownloadImport',
                                hidden: false
                            },
                            {
                                xtype: 'tbseparator'
                            },
                            {
                                xtype: 'button',
                                text: 'Import Template Gaji',
                                ref: '../../../btnImport',
                                hidden: false
                            }
                        ]
                    }
                ]
            }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.PayrollWorksheetGrid.superclass.initComponent.call(this);
        this.btnGenerate.on('Click', this.loadGenerateForm, this);
        this.btnReGenerate.on('Click', this.regenerateRec, this);
        this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.btnDownloadImport.on('Click', this.downloadTemplate, this);
        this.btnImport.on('Click', this.importData, this);
        this.btnReload.on('Click', this.reloadGrid, this);
        this.pegawai.on('Change', this.changePeriodePegawai, this);
        this.periode.on('Change', this.changePeriodePegawai, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    changePeriodePegawai: function () {
        let peg = this.pegawai.getValue();
        let peri = this.periode.getValue();
        if (peg !== '' && peri !== '') {
            this.reloadGrid();
        }
    },
    reloadGrid: function () {
        jun.rztPayrollWorksheet.load();
    },
    downloadTemplate: function () {
        window.open(TEMPLATE_UPLOAD);
    },
    importData: function () {
        var form = new jun.ImportKomponenPayroll();
        form.show();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadGenerateForm: function () {
        var form = new jun.WorksheetGenerateWin({modez: 0});
        form.show();
    },
    regenerateRec: function () {
        var periode = Ext.getCmp('payroll_worksheet_periode_id');
        var pegawai = Ext.getCmp('payroll_worksheet_pegawai_id');
        var pegawai_id = pegawai.getValue();
        var periode_id = periode.getValue();
        if (pegawai_id == '' || periode_id == '') {
            if (record == undefined) {
                Ext.MessageBox.alert("Warning", "Anda Belum Memilih Pegawai dan Periode");
                return;
            }
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin generate data ini?', this.regenerateRecYes, this);
    },
    regenerateRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var periode = Ext.getCmp('payroll_worksheet_periode_id');
        var pegawai = Ext.getCmp('payroll_worksheet_pegawai_id');
        var pegawai_id = pegawai.getValue();
        var periode_id = periode.getValue();
        Ext.Ajax.request({
            url: 'PayrollWorksheet/WorksheetGenerate/',
            method: 'POST',
            params: {
                'pegawai_id': Ext.getCmp('payroll_worksheet_pegawai_id').getValue(),
                'periode_id': Ext.getCmp('payroll_worksheet_periode_id').getValue()
            },
            success: function (f, a) {
                jun.rztPayrollWorksheet.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadForm: function () {
        var pegawai_id = this.pegawai.getValue();
        var periode_id = this.periode.getValue();
        if (pegawai_id === '' || periode_id === '') {
            Ext.MessageBox.alert("Warning", "Select pegawai and Periode!!!");
        }
        var form = new jun.PayrollWorksheetWin({modez: 0});
        form.pegawai_id.setValue(pegawai_id);
        form.periode_id.setValue(periode_id);
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.payroll_worksheet_id;
        var form = new jun.PayrollWorksheetWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PayrollWorksheet/delete/id/' + record.json.payroll_worksheet_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPayrollWorksheet.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.WorksheetGenerateWin = Ext.extend(Ext.Window, {
    title: 'Generate Worksheet',
    modez: 1,
    width: 400,
    height: 115,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-WorksheetGenerate',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp,
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.WorksheetGenerateWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        urlz = 'PayrollWorksheet/WorksheetGenerate/';
        Ext.getCmp('form-WorksheetGenerate').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztPayroll.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-WorksheetGenerate').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ImportKomponenPayroll = Ext.extend(Ext.Window, {
    title: "Import Komponen Payroll",
    iconCls: "silk13-report",
    modez: 1,
    width: 500,
    height: 445,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    headerCounter: 0,
    dataSubmit: null,
    arrData: null,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportKomponenPayroll",
                bodyStyle: 'background-color: #E4E4E4; padding: 10px;margin-bottom: 5px;',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: true,
                items: [
                    {
                        xtype: 'combo',
                        style: 'margin-bottom:2px',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Periode',
                        store: jun.rztPeriodeCmp, //RUMUS deklarasi store langsung di combo
                        hiddenName: 'periode_id',
                        valueField: 'periode_id',
                        emptyText: 'Pilih Periode',
                        ref: '../periode_id',
                        displayField: 'kode_periode',
                        anchor: '100%',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<div style="width:100%;display:inline-block;">\n\
                                <span style="font-weight: bold">{kode_periode}</span>\n\
                                <br>\n\{periode_start} - {periode_end}\n\
                                </div>',
                            "</div></tpl>")
                    },
                    {
                        xtype: 'fileuploadfield',
                        id: 'inputFileid',
                        ref: '../inputFile',
                        emptyText: 'Format file : Excel (.xlsx)',
                        fieldLabel: 'File',
                        name: 'xlfile',
                        buttonText: '',
                        buttonCfg: {
                            iconCls: "silk13-page_white_excel",
                        },
                        anchor: '100%',
                        listeners: {
                            render: function (c) {
                                new Ext.ToolTip({
                                    target: c.getEl(),
                                    html: 'Format file : Excel (.xlsx)'
                                });
                            },
                            fileselected: function (t, n, o) {
                                var f = t.fileInput.dom.files[0];
                                if (!f) {
                                    alert("Failed to load file");
                                    t.reset();
                                }  else {
                                    var r = new FileReader();
                                    r.onload = function (e) {
                                        var data = e.target.result;
                                        var wb;
                                        var arr = fixdata(data);
                                        wb = XLS.read(btoa(arr), {type: 'base64'});
                                        jun.dataStockOpname = to_json(wb);
                                        jun.namasheet = wb.SheetNames[0];
                                        jun.dataSales = jun.dataStockOpname[jun.namasheet];
                                    };
                                    r.readAsArrayBuffer(f);
                                }
                            }
                            // afterrender: function () {
                            //     itemFile = document.getElementById("inputFileid");
                            //     itemFile.addEventListener('change', function (evt) {
                            //             //Retrieve the first (and only!) File from the FileList object
                            //             var f = evt.target.files[0];
                            //             if (!f) {
                            //                 alert("Failed to load file");
                            //             } else if (!file.type.match('*.xlsx')) {
                            //                 alert(f.name + " is not a valid xlsx file.");
                            //             } else {
                            //                 var r = new FileReader();
                            //                 r.onload = function (e) {
                            //                     var data = e.target.result;
                            //                     var wb;
                            //                     var arr = fixdata(data);
                            //                     wb = XLS.read(btoa(arr), {type: 'base64'});
                            //                     jun.dataStockOpname = to_json(wb);
                            //                     jun.namasheet = wb.SheetNames[0];
                            //                     jun.dataSales = jun.dataStockOpname[jun.namasheet];
                            //                 };
                            //                 r.readAsArrayBuffer(f);
                            //             }
                            //         }
                            //         , false);
                            // }
                        }
                    }//,
                    // {
                    //     html: '<input type="file" name="xlfile" id="inputFile" />',
                    //     name: 'file',
                    //     xtype: "panel",
                    //     listeners: {
                    //         render: function (c) {
                    //             new Ext.ToolTip({
                    //                 target: c.getEl(),
                    //                 html: 'Format file : Excel (.xls)'
                    //             });
                    //         },
                    //         afterrender: function () {
                    //             itemFile = document.getElementById("inputFile");
                    //             itemFile.addEventListener('change', readFileExcel, false);
                    //         }
                    //     }
                    // }
                ]
            },
            // {
            //     xtype: "panel",
            //     bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
            //     html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            // },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 250,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg',
                    style: {
                        'fontFamily': 'courier new',
                        'fontSize': '12px'
                    }
                }
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportKomponenPayroll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportKomponenPayrollWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFileid").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT KOMPONEN GAJI -----');
        this.rowLoopCounter = 0;
        this.headerCounter = 0;
        this.dataSubmit = null;
        this.arrData = jun.dataSales;
        jun.dataSales = null;
        this.loopPost();
    },
    loopPost: function () {
        if (this.headerCounter >= this.arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT KOMPONEN GAJI SELESAI -----'
                + '\nJumlah data : ' + this.headerCounter
                + '\n-------------------------------------------\n');
            // document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            this.arrData = null;
            jun.dataStockOpname = null;
            return;
        }
        var d = this.arrData[this.headerCounter];
        // this.headerCounter++;
        this.dataSubmit = {};
        this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
        this.loopLog('Data Header nomor : ' + this.headerCounter);
        this.dataSubmit['NIK'] = d.NIK;
        this.dataSubmit['AMOUNT'] = d.AMOUNT;
        this.dataSubmit['PAYCODE'] = d.PAYCODE;
        this.writeLog('NIK : ' + this.dataSubmit['NIK']
            + '\nPAYCODE : ' + this.dataSubmit['PAYCODE']
            + '\nAMOUNT : ' + this.dataSubmit['AMOUNT'])
        ;
        this.writeLog('>>> Submit : Data nomor : ' + this.headerCounter);
        // this.nextRow();
        Ext.getCmp('form-ImportKomponenPayroll').getForm().submit({
            url: 'PayrollWorksheet/Upload',
            timeOut: 1000,
            scope: this,
            params: this.dataSubmit,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                var msg = '';
                if (response.success) {
                    msg = response.msg;
                } else {
                    msg = '***** GAGAL ****\n' + response.msg;
                    this.btnSave.setDisabled(false);
                }
                this.writeLog('<<< Result : dari nomor : ' + this.headerCounter + '\n' + msg);
                if (response.success) {
                    this.nextRow();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        // this.nextRow();
    },
    nextRow: function () {
        this.headerCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('Baris ke- ' + (this.headerCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});